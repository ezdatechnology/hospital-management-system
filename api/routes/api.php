<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\PatientAuthenticationMiddleware;
use App\Http\Middleware\SampleCollectorAuthenticationMiddleware;
use App\Http\Controllers\AddressController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PatientAuthController;
use App\Http\Controllers\SampleCollectorAuthController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\SlotController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

#==============================================#
#----- PATIENT AUTHENTICATION API ROUTES  -----#
#==============================================#  
    
Route::post("/new_register", [PatientAuthController::class, "register"]);
Route::post("/login", [PatientAuthController::class, "login"]);
Route::post("/send-otp", [PatientAuthController::class, "sendOTP"]);
Route::post("/verify-otp", [PatientAuthController::class, "verifyOTP"]);

Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/logout", [PatientAuthController::class, "logout"]);
    Route::post("/profile/edit", [PatientAuthController::class, "editProfile"]);
});

#==============================================#
#---------- PATIENT TEST API ROUTES  ----------#
#==============================================#  
    
Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/test/get-test-by-id", [PatientController::class, "getTestById"]);
    Route::post("/test/get-all-tests", [PatientController::class, "getAllTests"]);
    Route::post("/test/get-all-tests-by-search", [PatientController::class, "getAllTestsBySearchKeyword"]);
});

#==============================================#
#---------- PATIENT SLOT API ROUTES  ----------#
#==============================================#  
    
Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/slot/book-slot", [SlotController::class, "bookSlot"]);
    Route::post("/slot/search-slot-by-date", [SlotController::class, "searchSlotByDate"]);
});

#==============================================#
#---------- PATIENT CART API ROUTES  ----------#
#==============================================#  
    
Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/cart/add-test-in-cart", [CartController::class, "addTestInCart"]);
    Route::post("/cart/get-cart-details", [CartController::class, "getCartDetails"]);
    Route::post("/cart/review-my-order", [CartController::class, "reviewMyOrder"]);
    Route::post("/cart/remove-test-from-cart", [CartController::class, "removeTestFromCart"]);
    Route::post("/cart/clear-cart", [CartController::class, "clearCart"]);
});

#==============================================#
#-------------- ORDER API ROUTES  -------------#
#==============================================#
Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/order/create", [OrderController::class, "createOrder"]);
    Route::post("/order/track-my-order", [OrderController::class, "trackMyOrder"]);
    Route::post("/order/cancel-my-order", [OrderController::class, "cancelMyOrder"]);
    Route::post("/order/get-all-orders", [OrderController::class, "getAllOrders"]);
    Route::post("/order/get-all-open-orders", [OrderController::class, "getAllOpenOrders"]);
    Route::post("/order/get-all-past-orders", [OrderController::class, "getAllPastOrders"]);
    Route::post("/order/get-all-cancelled-orders", [OrderController::class, "getAllCancelledOrders"]);
    Route::post("/order/get-all-refunded-orders", [OrderController::class, "getAllRefundedOrders"]);
    Route::post("/order/update-order-status-by-id", [OrderController::class, "updateOrderStatusById"]);
    Route::post("/order/get-all-order-status", [OrderController::class, "getAllOrderStatus"]);
    Route::post("/order/get-all-payment-options", [OrderController::class, "getAllPaymentOptions"]);
});

#==============================================#
#-------------- MEMBER API ROUTES  ------------#
#==============================================#  

Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/member/add", [MemberController::class, "addMember"]);
    Route::post("/member/edit-member-by-id", [MemberController::class, "editMember"]);
    Route::post("/member/send-otp", [MemberController::class, "sendOTP"]);
    Route::post("/member/verify-otp", [MemberController::class, "verifyOTP"]);
    Route::post("/member/get-member-by-id", [MemberController::class, "getMemberById"]);
    Route::post("/member/get-all-members", [MemberController::class, "getAllMembers"]);
    Route::post("/member/get-all-family-relations", [MemberController::class, "getAllFamilyRealtions"]);
});

#==============================================#
#------------- ADDRESS API ROUTES  ------------#
#==============================================#  

Route::middleware([PatientAuthenticationMiddleware::class])->group(function(){
    Route::post("/address/add", [AddressController::class, "addAddress"]);
    Route::post("/address/edit-address-by-id", [AddressController::class, "editAddress"]);
    Route::post("/address/get-address-by-id", [AddressController::class, "getAddressById"]);
    Route::post("/address/get-all-addresses", [AddressController::class, "getAllAddresses"]);
    Route::post("/address/make-primary-address-by-id", [AddressController::class, "makeAddressPrimaryById"]);
});

#===============================================#
#-- SAMPLE COLLECTOR AUTHENTICATION API ROUTES -#
#===============================================#  
    
Route::post("/sample-collector/login", [SampleCollectorAuthController::class, "login"]);
Route::post("/sample-collector/send-otp", [SampleCollectorAuthController::class, "sendOTP"]);
Route::post("/sample-collector/verify-otp", [SampleCollectorAuthController::class, "verifyOTP"]);

Route::middleware([SampleCollectorAuthenticationMiddleware::class])->group(function(){
    Route::post("/sample-collector/logout", [SampleCollectorAuthController::class, "logout"]);
    Route::post("/sample-collector/profile/edit", [SampleCollectorAuthController::class, "editProfile"]);
});

#==============================================#
#--- SAMPLE COLLECTOR DASHBOARD API ROUTES  ---#
#==============================================#  

Route::middleware([SampleCollectorAuthenticationMiddleware::class])->group(function(){
    Route::post("/dashboard/get-today-slots", [DashboardController::class, "getTodaySlots"]);
    Route::post("/dashboard/slot-reschedule", [DashboardController::class, "slotReschedule"]);
    Route::post("/dashboard/order-denied", [DashboardController::class, "orderCancel"]);
    Route::post("/dashboard/sample-collect", [DashboardController::class, "sampleCollect"]);
    Route::post("/dashboard/sample-deposit", [DashboardController::class, "sampleDeposit"]);
    Route::post("/dashboard/get-today-amounts", [DashboardController::class, "getTodayAmounts"]);
    Route::post("/dashboard/get-amount-history", [DashboardController::class, "getAmountHistory"]);
    Route::post("/dashboard/track-my-order", [DashboardController::class, "trackMyOrder"]);
    Route::post("/dashboard/search-slot-by-date", [DashboardController::class, "searchSlotByDate"]);
    Route::post("/dashboard/assign-sample-collector", [DashboardController::class, "assignSampleCollector"]);
});