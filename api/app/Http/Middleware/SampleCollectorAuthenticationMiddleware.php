<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Crypt;
use App\Models\SampleCollectorSession;

class SampleCollectorAuthenticationMiddleware extends BaseController{
    public function handle(Request $request, Closure $next){
        // Validating If Auth Token Provided.
        if(trim($request->header("AUTHTOKEN"))){
            if(trim($request->hasHeader("AUTHTOKEN")) === ""){
                return $this->fail(null, "Auth Token Required.");
            }
        } else{
            return $this->fail(null, "Auth Token Required.");
        }
        
        try{
            // Decrypting Auth Token.
            $authToken =  Crypt::decryptString(trim($request->header("AUTHTOKEN")));

            // After Decrypting, Converting Auth Token Into Array.
            $authToken = json_decode($authToken, true);
        } catch(DecryptException $e){
            return $this->fail(null, "Invalid PayLoad.");
        }
        
        // Validating Auth Token.
        $sampleCollector = SampleCollectorSession::firstWhere($authToken);
        
        // On Sample Collector's Session Not Found.
        if(!$sampleCollector){
            return $this->fail(null, "Invalid Auth Token.");
        }

        // Sending Back Decrypted Auth Token The Requested Route.
        $request["auth_token"] = $authToken;

        // Proceed Request.
        return $next($request);
    }
}
