<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Crypt;
use App\Models\PatientSession;

class PatientAuthenticationMiddleware extends BaseController{
    public function handle(Request $request, Closure $next){
        // Validating If Auth Token Provided.
        if(trim($request->header("AUTHTOKEN"))){
            if(trim($request->hasHeader("AUTHTOKEN")) === ""){
                return $this->fail([], "Auth Token Required.");
            }
        } else{
            return $this->fail([], "Auth Token Required.");
        }
        
        try{
            // Decrypting Auth Token.
            $authToken =  Crypt::decryptString(trim($request->header("AUTHTOKEN")));

            // After Decrypting, Converting Auth Token Into Array.
            $authToken = json_decode($authToken, true);
        } catch(DecryptException $e){
            return $this->fail([], "Invalid PayLoad.");            
        }
        
        // Validating Auth Token.
        $patientSession = PatientSession::firstWhere($authToken);
        
        // On Patient's Session Not Found.
        if(!$patientSession){
            return $this->fail([], "Invalid Auth Token.");
        }

        // Sending Back Decrypted Auth Token The Requested Route.
        $request["auth_token"] = $authToken;

        // Proceed Request.
        return $next($request);
    }
}
