<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use App\Models\Patient;
use App\Models\Member;

class MemberController extends BaseController{
    #==============================================#
    #----------- ADD MEMBER API FUNCTION ----------#
    #==============================================#
    
    public function addMember(Request $request){
        // Current Date So That Valid DOB Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "name" => "required",
            "contact" => "required|min:10",
            "gender" => "required",
            "dob" => "required|date|before_or_equal:$now",
            "email" => "email",
            "relation" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "name.required" => "Name Required",
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
            "contact.unique" => "Already A Registered Contact Number.",
            "gender.required" => "Gender Required",
            // "email.required" => "Email Address Required",
            "email.email" => "Invalid Email Address",
            "email.unique" => "Already A Registered Email Address.",
            "relation.required" => "Relation Required",
            "dob.required" => "Date Of Birth Required",
            "dob.date" => "Invalid Date",
            "dob.before_or_equal" => "Invalid Future Date",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Creating New Member Model For Adding Member With Their Patient Model.
        $patient = Patient::find(trim($request->input("auth_token")["patient_id"]));
        $member = new Member();
        $member->name = trim($request->input("name"));
        $member->contact = trim($request->input("contact"));
        $member->gender = trim($request->input("gender"));
        $member->dob = trim($request->input("dob"));
        $member->email = trim($request->input("email"));
        $member->relation = trim($request->input("relation"));
        $member->unique_code = bin2hex(random_bytes(3));
        $member->otp_verified = "no";        

        // On Saving Patient Member Model.
        $saveMember = $patient->member()->save($member);

        // On Registration Fail.
        if(!$saveMember){
            return $this->fail([], "Member Registration Not Succeed. Try Again...");
        }
        
        // On Registration Success.
        return $this->success($saveMember, "Member Registration Succeed. Verify OTP.");;
    }

    #==============================================#
    #----------- SEND OTP API FUNCTION ------------#
    #==============================================#
    
    public function sendOTP(Request $request){
        // Validation Rules.
        $rules = [ "id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "id.required" => "Member Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Get Member Details By Id.
        $member = Member::firstWhere([
                            "id" => trim($request->input("id")),
                            "patient_id" => trim($request->input("auth_token")["patient_id"])
                        ]);
        
        // Member Not Found.
        if(!$member){
            return $this->fail([], "No Member Details Found. Try Again...");
        }

        // SEND OTP URL.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/" . $member->contact . "/AUTOGEN";

        // Making Request
        $response = Http::get($url);
        
        // On Failure of Sending OTP.
        if(!$response->ok()){
            return $this->fail([], "OTP Not Sent. Try Again...");
        }

        // On Success Of Updating Member Details.
        $member->session_id = $response->json("Details");
        $updateMember = $member->save();
        
        // On Failure Of Updating Member Details.
        if(!$updateMember){
            return $this->fail($response->json(), "OTP Sent But Member's Details Not Updated.");
        }

        return $this->success($response->json(), "OTP Sent Successfully.");
    }
    
    #==============================================#
    #------- VERIFY MEMBER OTP API FUNCTION -------#
    #==============================================#
    
    public function verifyOTP(Request $request){
        // Validation Rules.
        $rules = [
            "id" => "required",
            "otp" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "id.required" => "Member Id Required",
            "otp.required" => "OTP Required"
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success
        $member = Member::firstWhere([
                            "id" => trim($request->input("id")),
                            "patient_id" => trim($request->input("auth_token")["patient_id"])
                        ]);
        
        // On Member Not Found.
        if(!$member){
            return $this->fail([], "No Member Details Found.");
        }
        
        // On Member Found.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/VERIFY/" . $member->session_id. "/". trim($request->input("otp"));
        
        // Making Request
        $response = Http::get($url);
        
        // On OTP Verification Fail.
        if(!$response->ok() || (strtolower($response->json("Details")) === "otp expired") || (strtolower($response->json("Details")) === "otp mismatch")){
            return $this->fail([], $response->json("Details"));
        }
        
        // On OTP Verification Success. Updating Member Details.
        $member->otp_verified = "yes";
        $member->session_id = "";
        $updateMember = $member->save();
        
        // Returning Response.
        return $this->success($member, $response->json("Details"));
    }

    #==============================================#
    #----------- GET MEMBER API FUNCTION ----------#
    #==============================================#
    
    public function getMemberById(Request $request){
        // Validation Rules.
        $rules = [ "id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "id.required" => "Member Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Retrieving Member Details By Member ID.
        $member = Member::firstWhere([
                            "id" => trim($request->input("id")),
                            "patient_id" => trim($request->input("auth_token")["patient_id"])
                        ]);

        // If Member Not Found
        if(!$member){
            return $this->fail([], "No Member Detials Found.");
        }

        // If Member Found.
        return $this->success($member, "Member Details");
    }
    
    #==============================================#
    #---------- EDIT MEMBER API FUNCTION ----------#
    #==============================================#
    
    public function editMember(Request $request){
        // Current Date So That Valid DOB Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "id" => "required",
            "name" => "required",
            "gender" => "required",
            "dob" => "required|date|before_or_equal:$now",
            "email" => "email",
            "relation" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "id.required" => "ID Address Required",
            "name.required" => "Name Required",
            "gender.required" => "Gender Required",
            "email.email" => "Invalid Email Address",
            "relation.required" => "Relation Required",
            "dob.required" => "Date Of Birth Required",
            "dob.date" => "Invalid Date",
            "dob.before_or_equal" => "Invalid Future Date",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Get Member Details By Id.
        $member = Member::firstWhere([
                            "id" => trim($request->input("id")),
                            "patient_id" => trim($request->input("auth_token")["patient_id"])
                        ]);
        
        // Member Not Found.
        if(!$member){
            return $this->fail([], "No Member Details Found. Try Again...");
        }

        // Storing Member Value To Their Respective Column.
        $member->name = trim($request->input("name"));
        $member->gender = trim($request->input("gender"));
        $member->dob = trim($request->input("dob"));
        $member->email = trim($request->input("email"));
        $member->relation = trim($request->input("relation"));

        // On Updating Patient Member Model.
        $updateMember = $member->save();

        // On Updating Fail.
        if(!$updateMember){
            return $this->fail([], "Member Details Not Updated. Try Again...");
        }
        
        // On Updating Success.
        return $this->success($member, "Member Details Updated Successfully.");;
    }

    #==============================================#
    #-------- GET ALL MEMBERS API FUNCTION --------#
    #==============================================#
    
    public function getAllMembers(Request $request){
        // Retrieving All Member Details Of Logged In Patient.
        $patient = Patient::find(trim($request->input("auth_token")["patient_id"]));
        
        // If Patient Not Found
        if(!$patient){
            return $this->fail([], "No Patient Found.");
        }
        
        // Response Data Of Patient And Their Members.
        $data["patient"] = $patient;
        $data["members"] = Member::where(["patient_id" => trim($request->input("auth_token")["patient_id"])])->get();

        // If Patient Found.
        return $this->success($data, "Member Details");
    }

    #==============================================#
    #------- GET ALL RELATIONS API FUNCTION -------#
    #==============================================#
    
    public function getAllFamilyRealtions(){
        // Retrieving All Available Family Realtions.
        $familyRealtions = config("constants.FAMILY_RELATIONS");

        // Returning Response.
        return $this->success($familyRealtions, "Family Realtions.");
    }
}