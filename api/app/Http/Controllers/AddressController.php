<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use App\Models\Patient;
use App\Models\Address;

class AddressController extends BaseController{
    #==============================================#
    #---------- ADD ADDRESS API FUNCTION ----------#
    #==============================================#
    
    public function addAddress(Request $request){
        // Validation Rules.
        $rules = [
            "address" => "required",
            "house" => "required",
            "pincode" => "required|digits:6",
            "city" => "required",
            "state" => "required",
            "locality" => "required",
            // "landmark" => "required",
            "type" => "required",
        ];

        // Validaton Custom Messages.
        $messages = [
            "address.required" => "Address Required",
            "house.required" => "House/Flat/Block Required",
            "pincode.required" => "Pincde Required",
            "pincode.digits" => "Invalid Pincode",
            "city.required" => "Area/City Required",
            "state.required" => "State Required",
            "locality.required" => "Locality Required",
            // "landmark.required" => "Landmark Required",
            "type.required" => "Address Type Required",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Creating New Address Model For Adding Address With Their Patient Model.
        $patient = Patient::find(trim($request->input("auth_token")["patient_id"]));

        // If No Address Is Registered Yet.
        $address = Address::firstWhere(["patient_id" => trim($request->input("auth_token")["patient_id"])]);

        // If Address No Found.
        if(!$address){
            $address = new Address();
            $address->is_primary = "yes";
        }else{
            $address = new Address();
            $address->is_primary = "no";            
        }

        $address->address = trim($request->input("address"));        
        $address->house = trim($request->input("house"));        
        $address->pincode = trim($request->input("pincode"));        
        $address->city = trim($request->input("city"));        
        $address->state = trim($request->input("state"));        
        $address->locality = trim($request->input("locality"));        
        $address->landmark = trim($request->input("landmark"));        
        $address->type = trim($request->input("type"));        
        
        // On Saving Patient Address Model.
        $saveAddress = $patient->address()->save($address);

        // On Fail.
        if(!$saveAddress){
            return $this->fail([], "New Address Not Saved. Try Again...");
        }
        
        // On Success.
        return $this->success($saveAddress, "New Address Saved Successfully.");;
    }

    #==============================================#
    #---------- GET ADDRESS API FUNCTION ----------#
    #==============================================#
    
    public function getAddressById(Request $request){
        // Validation Rules.
        $rules = [ "id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "id.required" => "Address Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Retrieving Address Details By Address ID.
        $address = Address::firstWhere([
                                "id" => trim($request->input("id")),
                                "patient_id" => trim($request->input("auth_token")["patient_id"])
                            ]);

        // If Address Not Found
        if(!$address){
            return $this->fail([], "No Address Detials Found.");
        }

        // If Address Found.
        return $this->success($address, "Address Details");
    }
    
    #==============================================#
    #---------- EDIT ADDRESS API FUNCTION ---------#
    #==============================================#
    
    public function editAddress(Request $request){
        // Validation Rules.
        $rules = [
            "id" => "required",
            "address" => "required",
            "house" => "required",
            "pincode" => "required|digits:6",
            "city" => "required",
            "state" => "required",
            "locality" => "required",
            // "landmark" => "required",
            "type" => "required",
        ];

        // Validaton Custom Messages.
        $messages = [
            "id.required" => "Address ID Required",
            "address.required" => "Address Required",
            "house.required" => "House/Flat/Block Required",
            "pincode.required" => "Pincde Required",
            "pincode.digits" => "Invalid Pincode",
            "city.required" => "Area/City Required",
            "state.required" => "State Required",
            "locality.required" => "Locality Required",
            // "landmark.required" => "Landmark Required",
            "type.required" => "Address Type Required",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Getting Address Details By Id.
        $address = Address::firstWhere([
                                "id" => trim($request->input("id")),
                                "patient_id" => trim($request->input("auth_token")["patient_id"])
                            ]);
        
        // On Address Not Found.
        if(!$address){
            return $this->fail([], "No Address Details Found.");
        }
        
        // Storing Address Value To Their Respective Column.
        $address->address = trim($request->input("address"));        
        $address->house = trim($request->input("house"));        
        $address->pincode = trim($request->input("pincode"));        
        $address->city = trim($request->input("city"));        
        $address->state = trim($request->input("state"));        
        $address->locality = trim($request->input("locality"));        
        $address->landmark = trim($request->input("landmark"));        
        $address->type = trim($request->input("type"));

        // On Updating Patient Address Model.
        $updateAddress = $address->save();

        // On Updating Fail.
        if(!$updateAddress){
            return $this->fail([], "Address Details Not Updated. Try Again...");
        }
        
        // On Updating Success.
        return $this->success($address, "Address Details Updated Successfully.");;
    }
    
    #==============================================#
    #------ MAKE ADDRESS PRIMARY API FUNCTION -----#
    #==============================================#
    
    public function makeAddressPrimaryById(Request $request){
        // Validation Rules.
        $rules = [ "id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "id.required" => "Address ID Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // If Any Patient Address Was Saved.
        $address = Address::firstWhere(["patient_id" => trim($request->input("auth_token")["patient_id"])]);

        // If No Address Saved Yet.
        if(!$address){
            return $this->fail([], "No Address Saved Yet.");
        }

        // Retrieving Address By Id.
        $address = Address::firstWhere([
                        "id" => trim($request->input("id")),
                        "patient_id" => trim($request->input("auth_token")["patient_id"])
                    ]);

        // If Address Not Found
        if(!$address){
            return $this->fail([], "No Address Detials Found.");
        }

        // On Validation Success Updating All Patient's Addressess is_primary To "no".
        $address = Address::where(["patient_id" => trim($request->input("auth_token")["patient_id"])])
                            ->update(["is_primary" => "no"]);
        
        // If Not Set is_primary Address To "no".
        if(!$address){
            return $this->fail([], "Setting Primary Address Failed. Try Again...");
        }
        
        // Making Selected Address As Primary Address.
        $makePrimaryAddress = Address::where([
                                        "id" => trim($request->input("id")),
                                        "patient_id" => trim($request->input("auth_token")["patient_id"])
                                    ])
                                    ->update(["is_primary" => "yes"]);

        // On Updating Fail.
        if(!$makePrimaryAddress){
            return $this->fail([], "Primary Address Not Set Successfully. Try Again...");
        }
        
        // On Updating Success.
        return $this->success([], "Primary Address Set Successfully.");
    }

    #==============================================#
    #------- GET ALL ADDRESSES API FUNCTION -------#
    #==============================================#
    
    public function getAllAddresses(Request $request){
        // Retrieving All Address Details Of Logged In Patient.
        $addresses = Patient::find(trim($request->input("auth_token")["patient_id"]))->address;
        
        // If Addresses Not Found
        if(!$addresses){
            return $this->fail([], "No Address Found.");
        }

        // If Address Found.
        return $this->success(["addresses" => $addresses], "Address Details");
    }
}
