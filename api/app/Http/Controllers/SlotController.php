<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\Patient;
use App\Models\Test;
use App\Models\Member;
use App\Models\Slot;
use App\Models\Cart;

class SlotController extends BaseController{
    #==============================================#
    #----------- BOOK SLOT API FUNCTION -----------#
    #==============================================#
    
    public function bookSlot(Request $request){
        // Converting Current Date Is Search Date Parameter.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "slot_date" => "required|date|after_or_equal:$now",
            "slot_time" => "required|integer|between:0,16",
        ];

        // Validaton Custom Messages.
        $messages = [
            "slot_date.required" => "Slot Booking Date Required",
            "slot_date.date" => "Invalid Slot Booking Date",
            "slot_time.required" => "Slot Booking Time Required",
            "slot_time.between" => "Slot Booking Time Must Be 0 To 16",
            "slot_date.after_or_equal" => "Past Date Not Allowed"
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // If Member ID Provided.
        if(trim($request->filled("member_id"))){
            $member = Member::firstWhere([
                                "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                "id" => trim($request->input("member_id"))
                            ]);

            // On No Member Found.
            if(!$member){
                return $this->fail([], "No Member Details Found.");
            }
        }

        // If Test Exist In The Cart?
        $cart = Cart::firstWhere([
                            "patient_id" => trim($request->input("auth_token")["patient_id"]),
                            "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                        ]);

        // No Test Found In The Cart.
        if(!$cart){
            return $this->fail([], "You Can Not Book Any Slot Because Your Cart Is Empty. Try Again...");
        }
        
        // Booking Or Updating Slot Model.
        $bookOrUpdateSlot = Slot::updateOrCreate([
                                "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0,
                                "order_number" => NULL
                            ], [
                                "slot_date" => date('Y-m-d', strtotime(trim($request->input("slot_date")))),
                                "slot_time" => trim($request->input("slot_time"))
                            ]);

        // On Fail.
        if(!$bookOrUpdateSlot){
            return $this->fail([], "Slot Not Booked. Try Again...");
        }
        
        // On Success.
        return $this->success([], "Slot Booked Successfully.");
    }

    #==============================================#
    #---------- SEARCH SLOT API FUNCTION ----------#
    #==============================================#
    
    public function searchSlotByDate(Request $request){
        // Converting Current Date Is Search Date Parameter.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "slot_date" => "required|date|after_or_equal:$now",
        ];

        // Validaton Custom Messages.
        $messages = [
            "slot_date.required" => "Slot Date Required",
            "slot_date.date" => "Invalid Slot Date",
            "slot_date.after_or_equal" => "Past Date Not Allowed"
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // Setting Avaialble Slot Timings Array Value As Unbooked.
        // 5 Means Slot is Empty.
        for($i=0; $i<=16; $i++){
            $slotTimings[$i] = [
                "slot_id"       => $i,
                "slot_time"    => config("constants.SLOT_TIMINGS.$i"),
                "booking_left"  => config("constants.SLOT_BOOKING_LIMIT")
            ];            
        }

        // Checking For Is Any Or All Slots Are Booked For The Provide Date?
        $bookedSlots = Slot::select("slot_time", DB::raw('count(slot_time) as booked_slots'))
                            ->whereDate("slot_date", date("Y-m-d", strtotime(trim($request->input("slot_date")))))
                            ->orderBy("slot_time", "asc")
                            ->groupBy("slot_time")
                            ->get();
        
        // On No Booked Slot Found.
        if(!$bookedSlots){
            return $this->success($slotTimings, "Slot Details.");
        }
        
        // On Booked Slot Found.
        foreach($bookedSlots as $slot){
            $slotTimings[$slot->getRawOriginal("slot_time")]["booking_left"] = config("constants.SLOT_BOOKING_LIMIT") - $slot["booked_slots"];
        }

        // Returning Slot.
        return $this->success($slotTimings, "Slot Details.");
    }
}