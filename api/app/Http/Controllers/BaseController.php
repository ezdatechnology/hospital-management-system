<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BaseController extends Controller{
    #==================================================#
    #--------- SUCCESS API RESPONSE FUNCTION ----------#
    #==================================================#
    
    public function success($data = null, $message = "Data Found."){
        $response = [
            "status" => "success",
            "message" => $message,
            "data" => $data            
        ];

        return response()->json($response);
    }
    
    #==================================================#
    #---------- FAILED API RESPONSE FUNCTION ----------#
    #==================================================#

    public function fail($data = null, $message = "No Data Found."){
        $response = [
            "status" => "fail",
            "message" => $message,
            "data" => $data          
        ];

        return response()->json($response);
    }

    #==================================================#
    #------------ HTTP REQUESTION FUNCTION ------------#
    #==================================================#
    
    public function httpRequest($url, $method="get", $body = null, $header = null){
        // Creating Absolute URL.
        $url = config("constants.BASEURL") . $url;

        // If Request Method Is Post.
        if(strtolower($method) === "post"){
            // If Request Method Is Post And Header Data Is Not Empty.
            if($header){
                return $resposne = Http::withHeaders($header)->$method($url, $body);
            }
            // If Request Data Is Post And Header Data Is Empty.
            else{
                return $resposne = Http::post($url, $body);
            }
        }
        
        // If Request Method Is Get.
        else if(strtolower($method) === "get"){
            return $resposne = Http::get($url, $body);
        }
    }
}