<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Dashboard;
use App\Models\Address;
use App\Models\Collection;
use App\Models\Member;
use App\Models\Observation;
use App\Models\Order;
use App\Models\Patient;
use App\Models\PatientPayment;
use App\Models\PatientTest;
use App\Models\Slot;
use App\Models\SampleCollector;
use App\Models\Test;

class DashboardController extends BaseController{

    #==============================================#
    #-------- GET TODAY SLOTS API FUNCTION --------#
    #==============================================#

    public function getTodaySlots(Request $request){
        // Default Empty Array.
        $data["today_slots"] = null;

        // Today's Total Orders
        $data["total"] = 0;

        // Today's Total Sample Collected Orders.
        $data["sample_collected"] = 0;

        // Today's Total Pending Orders.
        $data["pending"] = 0;

        // Joining Needed Tables For Retrieving Order Details With Today Slot Details
        // And Patient Details With Their Members Details.
        $query = DB::table("patient_payments as pp")
                    ->select("pp.*")
                    ->where([
                        "s.slot_date" => date("Y-m-d", strtotime(now())),
                        "pp.sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"]),
                    ])
                    ->join("slots as s", "s.order_number", "=", "pp.order_number");

        // If Patient Id Provided.
        if(trim($request->filled("patient_id"))){
            $query->where(["pp.patient_id" =>  trim($request->input("patient_id"))]);
        }

        // Retrieving Order Details With Today Slot Details
        // And Patient Details With Their Members Details.
        $today_bookings = $query->get();

        // On Today Slot Booking Found.
        if(!count($today_bookings)){
            return $this->fail($data, "You Have Not Been Assigned Any Slot Yet.");
        }

        // Counting Total Today's Persons.
        foreach($today_bookings as $key => $order){
            // Today's Total Orders
            $data["total"] += 1;

            // Counting Sample Collected Orders.
            if(($order->order_status >= config("constants.ORDER_STATUS.SAMPLE COLLECTED")) && ($order->order_status <= config("constants.ORDER_STATUS.REPORT ISSUED"))){
                $data["sample_collected"] += 1;
            }

            // Counting Pending Orders.
            if($order->order_status <= config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED")){
                $data["pending"] += 1;
            }

            // Converting Json StatusWithTime To Array.
            $order->status_with_time = json_decode($order->status_with_time, true);

            // Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
            $order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
        }

        // Retrieving Patient, Member, Address And Slot Details For Each Order.
        foreach($today_bookings as $key => $order){
            $data["today_slots"][$key]["patient"] = Patient::find($order->patient_id);
            $data["today_slots"][$key]["member"] = Member::find($order->member_id);
            $data["today_slots"][$key]["address"] = Address::find($order->address_id);
            $data["today_slots"][$key]["slot"] = Slot::find($order->slot_id);
            $data["today_slots"][$key]["order"] = $order;
            $data["today_slots"][$key]["tests"] = DB::table("patient_tests as pt")
                                                    ->select("t.id as test_id", "t.name as test", "t.price")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get();

            // Retrieving Parameters Covered Under The Test.
            foreach($data["today_slots"][$key]["tests"] as $test){
                $test->parameters = Observation::where([
                                    "test_id" => $test->test_id,
                                    "status" => "active"
                                ])->get();

                // Total Number Of Paramerters Covered Under The Test.
                $test->parameters_covered = count($test->parameters);
            }
        }

        // If Today's Slot Details Found.
        return $this->success($data, "Today's Slot Details.");
    }

    #==============================================#
    #------ TRACK MY ORDER BY ID API FUNCTION -----#
    #==============================================#

    public function trackMyOrder(Request $request){
        // Validation Rules.
        $rules = [
            "patient_id" => "required",
            "order_id" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "patient_id.required" => "Patient ID Required",
            "order_id.required" => "Order ID Required"
        ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // Order Details.
        $data["order"] = PatientPayment::firstWhere([
                            "patient_id" => trim($request->input("patient_id")),
                            "order_number" => trim($request->input("order_id"))
                        ]);

        // On Order Not Found.
        if(!$data["order"]){
            return $this->fail(null, "No Such Order Details Found.");
        }

        // Patient Details.
        $data["patient"] = Patient::find($data["order"]->patient_id);

        // Member Details.
        $data["member"] = $data["order"]->member_id ? Member::find($data["order"]->member_id) : null;

        // Address Details.
        $data["address"] = Address::find($data["order"]->address_id);

        // Slot Details.
        $data["slot"] = Slot::find($data["order"]->slot_id);

        // Test Details.
        $data["tests"] = DB::table("patient_payments as pp")
                            ->select("t.id", "t.name", "t.price")
                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                            ->join("tests as t", "t.id", "=", "pt.test_id")
                            ->where([
                                "pp.patient_id" => trim($request->input("patient_id")),
                                "pp.order_number" => trim($request->input("order_id"))
                            ])->get();

        // Retrieving Parameters Covered Under The Test.
        if($data["tests"]){
            // Retrieving Parameters Covered Under The Test.
            foreach($data["tests"] as $test){
                $test->parameters = Observation::where([
                                            "test_id" => $test->id,
                                            "status" => "active"
                                        ])->get();

                // Total Number Of Paramerters Covered Under The Test.
                $test->parameters_covered = count($test->parameters);
            }
        }

        // On Success.
        return $this->success($data, "Order Details.");
    }

    #==============================================#
    #---------- SEARCH SLOT API FUNCTION ----------#
    #==============================================#

    public function searchSlotByDate(Request $request){
        // Converting Current Date Is Search Date Parameter.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "slot_date" => "required|date|after_or_equal:$now",
        ];

        // Validaton Custom Messages.
        $messages = [
            "slot_date.required" => "Slot Date Required",
            "slot_date.date" => "Invalid Slot Date",
            "slot_date.after_or_equal" => "Past Date Not Allowed"
        ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // Setting Avaialble Slot Timings Array Value As Unbooked.
        // 5 Means Slot is Empty.
        for($i=0; $i<=16; $i++){
            $slotTimings[$i] = [
                "slot_id"       => $i,
                "slot_time"    => config("constants.SLOT_TIMINGS.$i"),
                "booking_left"  => config("constants.SLOT_BOOKING_LIMIT")
            ];
        }

        // Checking For Is Any Or All Slots Are Booked For The Provide Date?
        $bookedSlots = Slot::select("slot_time", DB::raw('count(slot_time) as booked_slots'))
                            ->whereDate("slot_date", date("Y-m-d", strtotime(trim($request->input("slot_date")))))
                            ->orderBy("slot_time", "asc")
                            ->groupBy("slot_time")
                            ->get();

        // On No Booked Slot Found.
        if(!$bookedSlots){
            return $this->success($slotTimings, "Slot Details.");
        }

        // On Booked Slot Found.
        foreach($bookedSlots as $slot){
            $slotTimings[$slot->getRawOriginal("slot_time")]["booking_left"] = config("constants.SLOT_BOOKING_LIMIT") - $slot["booked_slots"];
        }

        // Returning Slot.
        return $this->success($slotTimings, "Slot Details.");
    }

    #==============================================#
    #----------- RESCHEDULE API FUNCTION ----------#
    #==============================================#

    public function slotReschedule(Request $request){
        // Current Date So That Slot Date Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));

        // Validation Rules.
        $rules = [
            "patient_id" => "required",
            "slot_date" => "required|date|after_or_equal:$now",
            "slot_time" => "required|integer|between:0,16",
        ];

        // Validaton Custom Messages.
        $messages = [
            "patient_id.required" => "Patient ID Required",
            "slot_date.required" => "Slot Booking Date Required",
            "slot_date.date" => "Invalid Slot Booking Date",
            "slot_date.after_or_equal" => "Past Date Not Allowed",
            "slot_time.required" => "Slot Booking Time Required",
            "slot_time.between" => "Slot Booking Time Value Must Be 0 To 16"
        ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Creating Request Data For Making HTTP Client Request To getTodaySlots API.
        // Request URL.
        $url = "/dashboard/get-today-slots";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();

        // Making HTTP Client Request To getTodaySlots.
        $getTodaySlots = $this->httpRequest($url, $method, $body, $header);

        // Retrieving Response Data.
        $getTodaySlots = $getTodaySlots->object();

        if($getTodaySlots->status === "fail"){
            // Returning Response.
            return $this->fail($getTodaySlots->data, $getTodaySlots->message);
        }

        // Separating All Today's Booked Order's Order Number.
        $orderNumbers = [];

        // Provided Patient ID.
        $patient_id = trim($request->input("patient_id"));

        // Stroing All Orders, Order Number Into The OrderNumber Array.
        foreach($getTodaySlots->data->today_slots as $key => $value){
            $orderNumbers[] = $value->order->order_number;
        }

        // If Order Number Provided.
        if(trim($request->filled("order_id"))){
            // Retrieving Slot Details.
            $getSlot = Slot::firstWhere([
                                "patient_id" => trim($request->input("patient_id")),
                                "order_number" => trim($request->input("order_id")),
                            ]);

            // No Slot Found.
            if(!$getSlot){
                return $this->fail(null, "No Slot Details Found.");
            }

            // On Slot Found.
            $updateSlot = $getSlot->update([
                "slot_date" => date('Y-m-d', strtotime(trim($request->input("slot_date")))),
                "slot_time" => trim($request->input("slot_time"))
            ]);

            // On Fail.
            if(!$updateSlot){
                return $this->fail(null, "Slot Reschduling Failed. Try Again...");
            }
            
            // On Success. Retrieving Order Details.
            $getOrder = PatientPayment::firstWhere([
                                "patient_id" => trim($request->input("patient_id")),
                                "order_number" => trim($request->input("order_id")),
                                ["order_status", "!=", config("constants.ORDER_STATUS.RESCHEDULED")]
                            ]);

            // No Order Found.
            if(!$getOrder){
                return $this->fail(null, "No Order Details Found.");
            }

            // Updating Patient's Particular Order Model.
            $updateOrder = $getOrder->update([
                                        "status_with_time->rescheduled" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.RESCHEDULED"),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.RESCHEDULED")]
                                    ]);

            // On Fail.
            if(!$updateOrder){
                return $this->fail(null, "Slot Rescheduled Successfully But Order Status Not Updated. Try Again...");
            }

            // On Success.
            return $this->success(null, "Slot Rescheduled Successfully.");
        }

        // If Order ID Not Provided. Updating Patient's All Today's Slot Model.
        $getSlots = Slot::where(["patient_id" => trim($request->input("patient_id"))])
                                ->whereIn("order_number", $orderNumbers)
                                ->get();

        // On No Slot Found.
        if(!count($getSlots)){
            return $this->fail(null, "No Slot Details Found.");
        }

        // If Order ID Not Provided. Updating Patient's All Today's Slot Model.
        $updateSlots = Slot::where(["patient_id" => trim($request->input("patient_id"))])
                            ->whereIn("order_number", $orderNumbers)
                            ->update([
                                "slot_date" => date('Y-m-d', strtotime(trim($request->input("slot_date")))),
                                "slot_time" => trim($request->input("slot_time"))
                            ]);

        // On Fail.
        if(!$updateSlots){
            return $this->fail(null, "All Slots Rescheduling Failed. Try Again...");
        }

        // On Success.
        $getOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.RESCHEDULED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->get();

        // On No Order Found.
        if(!count($getOrders)){
            return $this->fail(null, "No Order Details Found.");
        }

        // If Order ID Not Provided. Updating Patient's All Today's Order Model.
        $updateOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.RESCHEDULED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->update([
                                        "status_with_time->rescheduled" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.RESCHEDULED")
                                    ]);

        // On Fail.
        if(!$updateOrders){
            return $this->fail(null, "All Orders Rescheduled Successfully But All Order's Status Not Updated. Try Again...");
        }

        // On Success.
        return $this->success(null, "All Orders Rescheduled Successfully.");
    }

    #==============================================#
    #------------- DENIED API FUNCTION ------------#
    #==============================================#

    public function orderCancel(Request $request){
        // Validation Rules.
        $rules = [ "patient_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "patient_id.required" => "Patient ID Required" ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Creating Request Data For Making HTTP Client Request To getTodaySlots API.
        // Request URL.
        $url = "/dashboard/get-today-slots";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();

        // Making HTTP Client Request To getTodaySlots.
        $getTodaySlots = $this->httpRequest($url, $method, $body, $header);

        // Retrieving Response Data.
        $getTodaySlots = $getTodaySlots->object();

        if($getTodaySlots->status === "fail"){
            // Returning Response.
            return $this->fail($getTodaySlots->data, $getTodaySlots->message);
        }

        // Separating All Today's Booked Order's Order Number.
        $orderNumbers = [];

        // Provided Patient ID.
        $patient_id = trim($request->input("patient_id"));

        // Stroing All Orders, Order Number Into The OrderNumber Array.
        foreach($getTodaySlots->data->today_slots as $key => $value){
            $orderNumbers[] = $value->order->order_number;
        }

        // If Order Number Provided.
        if(trim($request->filled("order_id"))){
            // Retrieving Order Details.
            $getOrder = PatientPayment::firstWhere([
                                "patient_id" => trim($request->input("patient_id")),
                                "order_number" => trim($request->input("order_id")),
                                ["order_status", "!=", config("constants.ORDER_STATUS.CANCELLED")]
                            ]);

            // No Order Found.
            if(!$getOrder){
                return $this->fail(null, "No Order Details Found.");
            }

            // Updating Patient's Particular Order.
            $updateOrder = $getOrder->update([
                                        "status_with_time->cancelled" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.CANCELLED"),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.CANCELLED")]
                                    ]);

            // On Fail.
            if(!$updateOrder){
                return $this->fail(null, "Order Cancellation Failed. Try Again...");
            }

            // On Success.
            return $this->success(null, "Order Cancelled Successfully.");
        }

        // If Order ID Not Provided. Updating All Patient's Today's Order.
        $getOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.CANCELLED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->get();

        // On No Order Found.
        if(!count($getOrders)){
            return $this->fail($getOrders, "No Order Details Found.");
        }

        // If Order ID Not Provided. Updating All Patient's Today's Order.
        $updateOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.CANCELLED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->update([
                                        "status_with_time->cancelled" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.CANCELLED")
                                    ]);

        // On Fail.
        if(!$updateOrders){
            return $this->fail(null, "All Orders Cancellation Failed. Try Again...");
        }

        // On Success.
        return $this->success(null, "All Orders Cancelled Successfully.");
    }

    #==============================================#
    #------------ COLLECT API FUNCTION ------------#
    #==============================================#

    public function sampleCollect(Request $request){
        // Validation Rules.
        $rules = [ "patient_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "patient_id.required" => "Patient ID Required" ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Creating Request Data For Making HTTP Client Request To getTodaySlots API.
        // Request URL.
        $url = "/dashboard/get-today-slots";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();

        // Making HTTP Client Request To getTodaySlots.
        $getTodaySlots = $this->httpRequest($url, $method, $body, $header);

        // Retrieving Response Data.
        $getTodaySlots = $getTodaySlots->object();

        if($getTodaySlots->status === "fail"){
            // Returning Response.
            return $this->fail($getTodaySlots->data, $getTodaySlots->message);
        }

        // Separating All Today's Booked Order's Order Number.
        $orderNumbers = [];

        // Provided Patient ID.
        $patient_id = trim($request->input("patient_id"));

        // Stroing All Orders, Order Number Into The OrderNumber Array.
        foreach($getTodaySlots->data->today_slots as $key => $value){
            $orderNumbers[] = $value->order->order_number;
        }

        // If Order Number Provided.
        if(trim($request->filled("order_id"))){
            // Retrieving Order Details.
            $getOrder = PatientPayment::firstWhere([
                                "patient_id" => trim($request->input("patient_id")),
                                "order_number" => trim($request->input("order_id")),
                                ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE COLLECTED")]
                            ]);

            // No Order Found.
            if(!$getOrder){
                return $this->fail(null, "No Order Details Found.");
            }

            // Updating Patient's Particular Order Model.
            $updateOrder = $getOrder->update([
                                        "status_with_time->sample_collected" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.SAMPLE COLLECTED"),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE COLLECTED")]
                                    ]);

            // On Fail.
            if(!$updateOrder){
                return $this->fail(null, "Order Sample Collection Failed. Try Again...");
            }

            // Creating Request Data For Making HTTP Client Request To getTodayAmounts API.
            // Request URL.
            $url = "/dashboard/get-today-amounts";
            // Request Method.
            $method = "post";
            // Request Headers.
            $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
            // Request Body.
            $body = $request->all();

            // Making HTTP Client Request To getTodayAmounts.
            $getTodayAmount = $this->httpRequest($url, $method, $body, $header);

            // Retrieving Response Data.
            $getTodayAmount = $getTodayAmount->object();

            // On Fail Response Returned.
            if($getTodayAmount->status === "fail"){
                // Returning Response.
                return $this->fail($getTodayAmount->data, $getTodayAmount->message);
            }

            // On Success Response Returned.
            if($getTodayAmount->status === "success"){
                // Storing Or Updating Collection Model.
                $collection = Collection::updateOrCreate([
                                        "sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"]),
                                        "collection_date" => date('Y-m-d', strtotime(now())),
                                    ], [ "amount" => $getTodayAmount->data->today_collection ]);

                // On Fail.
                if(!$collection){
                    return $this->fail(null, "Today Collection Not Saved. Try Again...");
                }
            }

            // On Success.
            return $this->success(null, "Order Sample Collected Successfully.");
        }

        // If Order ID Not Provided. Updating All Patient's Today Order Model.
        $getOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE COLLECTED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->get();

        // On No Order Found.
        if(!count($getOrders)){
            return $this->fail(null, "No Order Details Found.");
        }

        // If Order ID Not Provided. Updating All Patient's Today Order Model.
        $updateOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE COLLECTED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->update([
                                        "status_with_time->sample_collected" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.SAMPLE COLLECTED")
                                    ]);

        // On Fail.
        if(!$updateOrders){
            return $this->fail(null, "All Orders Sample Collection Failed. Try Again...");
        }

        // Creating Request Data For Making HTTP Client Request To getTodayAmounts API.
        // Request URL.
        $url = "/dashboard/get-today-amounts";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();

        // Making HTTP Client Request To getTodayAmounts.
        $getTodayAmount = $this->httpRequest($url, $method, $body, $header);

        // Retrieving Response Data.
        $getTodayAmount = $getTodayAmount->object();

        // On Fail Response Returned.
        if($getTodayAmount->status === "fail"){
            // Returning Response.
            return $this->fail($getTodayAmount->data, $getTodayAmount->message);
        }

        // On Success Response Returned.
        if($getTodayAmount->status === "success"){
            // Storing Or Updating Collection Model.
            $collection = Collection::updateOrCreate([
                                    "sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"]),
                                    "collection_date" => date('Y-m-d', strtotime(now())),
                                ], [ "amount" => $getTodayAmount->data->today_collection ]);

            // On Fail.
            if(!$collection){
                return $this->fail(null, "Today Collection Not Saved. Try Again...");
            }
        }

        // On Success.
        return $this->success(null, "All Orders Sample Collected Successfully.");
    }

    #==============================================#
    #------------ DEPOSIT API FUNCTION ------------#
    #==============================================#

    public function sampleDeposit(Request $request){
        // Validation Rules.
        $rules = [ "patient_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "patient_id.required" => "Patient ID Required" ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Creating Request Data For Making HTTP Client Request To getTodaySlots API.
        // Request URL.
        $url = "/dashboard/get-today-slots";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();

        // Making HTTP Client Request To getTodaySlots.
        $getTodaySlots = $this->httpRequest($url, $method, $body, $header);

        // Retrieving Response Data.
        $getTodaySlots = $getTodaySlots->object();

        if($getTodaySlots->status === "fail"){
            // Returning Response.
            return $this->fail($getTodaySlots->data, $getTodaySlots->message);
        }

        // Separating All Today's Booked Order's Order Number.
        $orderNumbers = [];

        // Provided Patient ID.
        $patient_id = trim($request->input("patient_id"));

        // Stroing All Orders, Order Number Into The OrderNumber Array.
        foreach($getTodaySlots->data->today_slots as $key => $value){
            $orderNumbers[] = $value->order->order_number;
        }

        // If Order Number Provided.
        if(trim($request->filled("order_id"))){
            // Retrieving Order Details.
            $getOrder = PatientPayment::firstWhere([
                                "patient_id" => trim($request->input("patient_id")),
                                "order_number" => trim($request->input("order_id")),
                                ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE SUBMITTED")]
                            ]);

            // No Order Found.
            if(!$getOrder){
                return $this->fail(null, "No Order Details Found.");
            }

            // Updating Patient's Particular Order Model.
            $updateOrder = $getOrder->update([
                                        "status_with_time->sample_submitted" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.SAMPLE SUBMITTED"),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE SUBMITTED")]
                                    ]);

            // On Fail.
            if(!$updateOrder){
                return $this->fail(null, "Order Sample Submition Failed. Try Again...");
            }

            // On Success.
            return $this->success(null, "Order Sample Submitted Successfully.");
        }

        // If Order ID Not Provided. Updating All Patient's Today Order Model.
        $getOrders = PatientPayment::where(
                                        ["patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE SUBMITTED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->get();

        // No Order Found.
        if(!count($getOrders)){
            return $this->fail(null, "No Order Details Found.");
        }

        // If Order ID Not Provided. Updating All Patient's Today Order Model.
        $updateOrders = PatientPayment::where([
                                        "patient_id" => trim($request->input("patient_id")),
                                        ["order_status", "!=", config("constants.ORDER_STATUS.SAMPLE SUBMITTED")]
                                    ])
                                    ->whereIn("order_number", $orderNumbers)
                                    ->update([
                                        "status_with_time->sample_submitted" => date('Y-m-d H:i:s', strtotime(now())),
                                        "order_status" => config("constants.ORDER_STATUS.SAMPLE SUBMITTED")
                                    ]);

        // On Fail.
        if(!$updateOrders){
            return $this->fail(null, "All Orders Sample Submition Failed. Try Again...");
        }

        // On Success.
        return $this->success(null, "All Orders Sample Submitted Successfully.");
    }

    #==============================================#
    #---------- TODAY AMOUNT API FUNCTION ---------#
    #==============================================#

    public function getTodayAmounts(Request $request){
        // Creating Default Empty Array For Storing Today Slot Details.
        $data["orders"] = null;

        // Today's Total Orders
        $data["total"] = 0;

        // Today's Total Prepaid Orders.
        $data["prepaid"] = 0;

        // Today's Total Pending Orders.
        $data["postpaid"] = 0;

        // Today's Sum Of Total Orders.
        $data["today_collection"] = 0;

        // Retrieving Order Details With Slot, Patient And Members.
        $todaySampleCollected = PatientPayment::where(["sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"])])
                                        ->where("order_status", ">=", config("constants.ORDER_STATUS.SAMPLE COLLECTED"))
                                        ->where("order_status", "<=", config("constants.ORDER_STATUS.REPORT ISSUED"))
                                        ->whereDate("status_with_time->sample_collected", date("Y-m-d", strtotime(now())))
                                        ->get();

        // On No Sample Collection Details Found For Today.
        if(!count($todaySampleCollected)){
            return $this->success(null, "No Today's Collection Details Found.");
        }

        // Total Sample Collected Orders.
        $data["total"] = count($todaySampleCollected);

        // Counting Total Today's Persons.
        foreach($todaySampleCollected as $key => $order){
            // Counting Prepaid Orders.
            if($order->payment_mode != config("constants.PAYMENT_OPTIONS.CASH")){
                $data["prepaid"] += 1;

                // If Payment Mode Was Not Cash, Then Order Payable Should Be Zero.
                $order->payable = 0;
            }

            // Counting Postpaid Orders.
            if($order->payment_mode == config("constants.PAYMENT_OPTIONS.CASH")){
                $data["postpaid"] += 1;

                // Sum Of Total Order's Amount.
                $data["today_collection"] += $order->payable;
            }
        }

        // Retrieving Patient, Member, And Test Details For Each Order.
        foreach($todaySampleCollected as $key => $order){
            $data["orders"][$key]["patient"] = Patient::find($order->patient_id);
            $data["orders"][$key]["member"] = Member::find($order->member_id);
            $data["orders"][$key]["order"] = $order;
            $data["orders"][$key]["tests"] = DB::table("patient_tests as pt")
                                                    ->select("t.id as test_id", "t.name as test", "t.price")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get();

            // Retrieving Parameters Covered Under The Test.
            foreach($data["orders"][$key]["tests"] as $test){
                $test->parameters = Observation::where([
                                    "test_id" => $test->test_id,
                                    "status" => "active"
                                ])->get();

                // Total Number Of Paramerters Covered Under The Test.
                $test->parameters_covered = count($test->parameters);
            }
        }

        // On Success.
        return $this->success($data, "Today's Amount Collection Details.");
    }

    #==============================================#
    #--------- AMOUNT HISTORY API FUNCTION --------#
    #==============================================#

    public function getAmountHistory(Request $request){
        // Retrieving Order Today Amount History.
        $data["amount_histories"] = Collection::where(["sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"])])
                                                ->latest("collection_date")
                                                ->get();

        // Today Amount.
        $data["today_amount"] = Collection::select(DB::raw("sum(amount) as today_amount"))
                                            ->where(["sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"])])
                                            ->whereDate("collection_date", date("Y-m-d", strtotime(now())))
                                            ->first()
                                            ->today_amount;

        // If Today Amount Is Null.
        $data["today_amount"] = $data["today_amount"] ? $data["today_amount"] : 0;

        // Yesterday Amount.
        $data["yesterday_amount"] = Collection::select(DB::raw("sum(amount) as yesterday_amount"))
                                            ->where(["sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"])])
                                            ->whereDate("collection_date", date("Y-m-d", strtotime("-1 days")))
                                            ->first()
                                            ->yesterday_amount;

        // If Yesterday Amount Is Null.
        $data["yesterday_amount"] = $data["yesterday_amount"] ? $data["yesterday_amount"] : 0;

        // Total Amount.
        $data["total_amount"] = Collection::select(DB::raw("sum(amount) as total_amount"))
                                            ->where(["sample_collector_id" => trim($request->input("auth_token")["sample_collector_id"])])
                                            ->first()
                                            ->total_amount;

        // If Total Amount Is Null.
        $data["total_amount"] = $data["total_amount"] ? $data["total_amount"] : 0;

        // On Success.
        return $this->success($data, "Today Amount History.");
    }

    #==============================================#
    #---- ASSIGN SAMPLE COLLECTOR API FUNCTION ----#
    #==============================================#

    public function assignSampleCollector(Request $request){
        // Validation Rules.
        $rules = [
            "order_id" => "required",
            "sample_collector_id" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "order_id.required" => "Order ID Required",
            "sample_collector_id.required" => "Sample Collector ID Required"
        ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Retrieving Sample Collector.
        $sampleCollector = SampleCollector::find(trim($request->input("sample_collector_id")));

        // On Sample Collector Found.
        if(!$sampleCollector){
            return $this->fail(null, "No Sample Collector Details Found.");
        }

        // Retrieving Order Details.
        $order = PatientPayment::firstWhere(["order_number" => trim($request->input("order_id"))]);

        // On No Order Details Found.
        if(!$order){
            return $this->fail(null, "No Order Details Failed.");
        }

        // Assinging Sample Collector To The Order.
        $order->sample_collector_id = trim($request->input("sample_collector_id"));
        $assignSampleCollector = $order->save();

        // On No Order Details Found.
        if(!$assignSampleCollector){
            return $this->fail(null, "Sample Collector Not Assigned. Try Again...");
        }

        // On Success.
        return $this->success($order, "Sample Collector Assigned Successfully.");
    }
}