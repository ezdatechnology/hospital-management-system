<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use App\Models\Patient;
use App\Models\Test;
use App\Models\Member;
use App\Models\Observation;

class PatientController extends BaseController{
    #==============================================#
    #---- SEARCH TEST BY KEAYWORD API FUNCTION ----#
    #==============================================#
    
    public function getAllTestsBySearchKeyword(Request $request){
        // // Validation Rules.
        // $rules = [ "search_keyword" => "required" ];

        // // Validaton Custom Messages.
        // $messages = [ "search_keyword.required" => "Search Keyword Required" ];
        
        // // Input Validation.
        // $validator = Validator::make($request->all(), $rules, $messages);
    
        // // On Validation Fail
        // if($validator->fails()){
        //     // Converting Validation Errors Array Into Key Value Pair.
        //     foreach($validator->messages()->getMessages() as $key => $value){
        //         $errors[$key] = $value[0];
        //     }

        //     // Returning Response.
        //     return $this->fail($errors, "Validation Failed.");
        // }
        
        // Retrieving Search Test Data By Search Keyword
        $testDetails = Test::select("id as test_id", "name", "department", "price", "gender", "color_code", "test_type", "billing_category", "quantity", "remark", "report_type", "created_at", "updated_at")
                                    ->where("name", "like", "%" . trim($request->input("search_keyword")) . "%")
                                    ->get();
                                    

        // Retrieving Parameters Covered Under The Test.
        foreach($testDetails as $test){
            $test->parameters = Observation::where([
                                        "test_id" => $test->test_id,
                                        "status" => "active"
                                    ])->get();

            // Total Number Of Paramerters Covered Under The Test.
            $test->parameters_covered = count($test->parameters);
        }

        // On Test Details Found.
        return $this->success(["tests" => $testDetails], "Test Details.");
    }
    
    #==============================================#
    #--------- GET TEST BY ID API FUNCTION --------#
    #==============================================#
    
    public function getTestById(Request $request){
        // Validation Rules.
        $rules = [ "id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "id.required" => "Test ID Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success
        $test = Test::select("id as test_id", "name", "department", "price", "gender", "color_code",
                            "test_type", "billing_category", "quantity", "remark", "report_type",
                            "created_at", "updated_at")
                            ->find(trim($request->input("id")));
        
        // On Test Not Found.
        if(!$test){
            return $this->fail([], "No Such Test Details Found.");
        }
                                    

        // Retrieving Parameters Covered Under The Test.        
        $test->parameters = Observation::where([
                                    "test_id" => $test->test_id,
                                    "status" => "active"
                                ])->get();

        // Total Number Of Paramerters Covered Under The Test.
        $test->parameters_covered = count($test->parameters);
        
        // On Test Found.
        return $this->success($test, "Test Details.");
    }

    #==============================================#
    #--------- GET ALL TESTS API FUNCTION ---------#
    #==============================================#    
    
    public function getAllTests(Request $request){
        // On Validation Success
        $tests = Test::select("id as test_id", "name", "department", "price", "gender", "color_code", "test_type", "billing_category", "quantity", "remark", "report_type", "created_at", "updated_at")->get();
        
        // On Test Not Found.
        if(!$tests){
            return $this->fail([], "No Test Found.");
        }
                                    

        // Retrieving Parameters Covered Under The Test.
        foreach($tests as $test){
            $test->parameters = Observation::where([
                                        "test_id" => $test->test_id,
                                        "status" => "active"
                                    ])->get();

            // Total Number Of Paramerters Covered Under The Test.
            $test->parameters_covered = count($test->parameters);
        }
        
        // On Test Found.
        return $this->success(["tests" => $tests], "Test Details.");
    }
}