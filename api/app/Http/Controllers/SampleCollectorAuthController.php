<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;
use App\Models\SampleCollector;
use App\Models\SampleCollectorSession;

class SampleCollectorAuthController extends BaseController{
    #==============================================#
    #----------- SEND OTP API FUNCTION ------------#
    #==============================================#
    
    public function sendOTP(Request $request){
        // Validation Rules.
        $rules = [
            "contact" => "required|min:10"
        ];

        // Validaton Custom Messages.
        $messages = [
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Retrieving Sample Collector By Contact
        $sampleCollector  = SampleCollector::firstWhere(["contact" => trim($request->input("contact"))]);

        // On No Sample Collector Found.
        if(!$sampleCollector){
            return $this->fail(null, "Not A Registered Sample Collector. Try Again...");
        }

        // SEND OTP URL.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/" . trim($request->input("contact")) . "/AUTOGEN";

        // Making Request
        $response = Http::get($url);
        
        // On Failure of Sending OTP.
        if(!$response->ok()){            
            return $this->fail(null, "OTP Not Sent. Try Again...");
        }
        
        // Updating Existing Sample Collector Model
        $sampleCollector->session_id = $response->json("Details");
        $sampleCollector->otp_verified = "no";
        $sampleCollectorResponse = $sampleCollector->save();
        
        // On Failure Of Saving Sample Collector Details.
        if(!$sampleCollectorResponse){
            return $this->fail(null, "OTP Sent But Sample Collector's Details Not Updated.");
        }

        // On Success Of Saving Sample Collector Details.
        return $this->success($response->json(), "OTP Sent Successfully.");
    }

    #==============================================#
    #------- OTP VERIFICATION API FUNCTION --------#
    #==============================================#
    
    public function verifyOTP(Request $request){
        // Validation Rules.
        $rules = [
            "contact" => "required|min:10",
            "otp" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
            "otp.required" => "OTP Required"
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success
        $sampleCollector = SampleCollector::firstWhere(["contact" => trim($request->input("contact"))]);
        
        // On Sample Collector Not Found.
        if(!$sampleCollector){
            return $this->fail(null, "No Such Contact Details Found.");
        }
        
        // On Sample Collector Found.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/VERIFY/" . $sampleCollector->session_id. "/". $request->input("otp");
        
        // Making Request
        $response = Http::get($url);
        
        // On OTP Verification Fail.
        if(!$response->ok() || (strtolower($response->json("Details")) === "otp expired") || (strtolower($response->json("Details")) === "otp mismatch")){
            return $this->fail(null, $response->json("Details"));
        }
        
        // Changing OTP Verified Status.
        $sampleCollector->otp_verified = "yes";
        // $sampleCollector->session_id = "";
        $saveSampleCollector = $sampleCollector->save();

        // On OTP Verification Success But Regsitration Status Is Pending.
        if($sampleCollector->registration_status === "pending"){
            return $this->success($sampleCollector, "OTP Verified. Complete Your Registraion First.");
        }
        
        // On OTP Verification Success And Regsitration Status Is Also Completed.
        // Creating Sample Collector's Login Session.
        $sampleCollectorLoginSession = new SampleCollectorSession();
        $sampleCollectorLoginSession->sample_collector_id = $sampleCollector->id;
        $sampleCollectorLoginSession->email = $sampleCollector->email;
        $login = $sampleCollectorLoginSession->save();
        
        // On Failure Of Creating Login Session
        if(!$login){
            return $this->fail(null, "Something Went Wrong While Login. Try Again...");
        }
        
        // On Success Of Creating Login Session.
        // Encrypting And Maintaning Sample Collector's Session.
        $authToken = Crypt::encryptString($sampleCollectorLoginSession);
        
        // Sending Auth Token For The Logged In Sample Collector.
        $sampleCollector["auth_token"] = $authToken;
        
        // On Successful Creating Login Session.
        return $this->success($sampleCollector, "Logged In Successfully.");
    }

    #==============================================#
    #---------- EDIT PROFILE API FUNCTION ---------#
    #==============================================#
    
    public function editProfile(Request $request){
        // Current Date So That Valid DOB Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));
        
        // Validation Rules.
        $rules = [
            "name" => "required",
            "gender" => "required",
            "dob" => "required|date|before_or_equal:$now",
            "email" => "email"
        ];

        // Validaton Custom Messages.
        $messages = [
            "name.required" => "Name Required",
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
            "gender.required" => "Gender Required",
            "email.email" => "Invalid Email Address",
            "email.unique" => "Already A Registered Email Address.",
            "dob.required" => "Date Of Birth Required",
            "dob.date" => "Invalid Date",
            "dob.before_or_equal" => "Invalid Future DOB",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Updating Sample Collector Details.
        $sampleCollector = SampleCollector::find(trim($request->input("auth_token")["sample_collector_id"]));
        
        // On Sample Collector Not Found.
        if(!$sampleCollector){
            return $this->fail(null, "No Sample Collector Data Found.");
        }

        // Storing New Sample Collector's Details In Their Respective Column.
        $sampleCollector->name = trim($request->input("name"));
        $sampleCollector->gender = trim($request->input("gender"));
        $sampleCollector->dob = trim($request->input("dob"));
        $sampleCollector->email = trim($request->input("email"));

        // On Updating Sample Collector Model.
        $updateProfile = $sampleCollector->save();

        // On Updating Fail .
        if(!$updateProfile){
            return $this->fail(null, "Profile Not Updated. Try Again...");
        }

        // On Updating Success.
        return $this->success($sampleCollector, "Profile Details Updated Successfully.");
    }

    #==============================================#
    #------------- LOGOUT API FUNCTION ------------#
    #==============================================#

    public function logout(Request $request){
        // Retrieving Sample Collector Session Details.
        $sampleCollectorSession = SampleCollectorSession::firstWhere(["id" => $request->input("auth_token")["id"]]);
        
        // Updating Sample Collector Logging Session For Logging Out.
        $sampleCollectorSession["status"] = "inactive";
        $logout = $sampleCollectorSession->save();
        
        // On Logging Out Fail.
        if(!$logout){
            return $this->fail(null, "Something Went Wrong While Logging Out. Try Again...");
        }
        
        // On Successful Logging Out.
        return $this->success(null, "Logged Out Successfully.");
    }
}