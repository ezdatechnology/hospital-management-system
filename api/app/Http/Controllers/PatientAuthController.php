<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Crypt;
use App\Models\Patient;
use App\Models\PatientSession;

class PatientAuthController extends BaseController{
    #==============================================#
    #----------- SEND OTP API FUNCTION ------------#
    #==============================================#
    
    public function sendOTP(Request $request){
        // Validation Rules.
        $rules = [
            "contact" => "required|min:10"
        ];

        // Validaton Custom Messages.
        $messages = [
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // SEND OTP URL.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/" . trim($request->input("contact")) . "/AUTOGEN";

        // Making Request
        $response = Http::get($url);
        
        // On Failure of Sending OTP.
        if(!$response->ok()){            
            return $this->fail([], "OTP Not Sent. Try Again...");
        }
        
        // Retrieving Patient By Contact
        $patient = Patient::firstWhere(["contact" => trim($request->input("contact"))]);
        
        // On Success Of Sending OTP But Patint Contact Details Not Exist.
        if(!$patient){
            // Creating New Patient Model
            $patient = new Patient();
            $patient->contact = trim($request->input("contact"));
            $patient->session_id = $response->json("Details");
            $patient->unique_code = bin2hex(random_bytes(3));
            $patient->otp_verified = "no";
            $patientResponse = $patient->save();
        }
        
        // If Patient Contact Details Already Exist.
        else{
            // Updating Existing Patient Model
            $patient->session_id = $response->json("Details");
            $patient->otp_verified = "no";
            $patientResponse = $patient->save();
        }
        
        // On Failure Of Saving Patient Details.
        if(!$patientResponse){
            return $this->fail([], "OTP Sent But Patient's Details Not Saved.");
        }

        // On Success Of Saving Patient Details.
        return $this->success($response->json(), "OTP Sent Successfully.");
    }

    #==============================================#
    #------- OTP VERIFICATION API FUNCTION --------#
    #==============================================#
    
    public function verifyOTP(Request $request){
        // Validation Rules.
        $rules = [
            "contact" => "required|min:10",
            "otp" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
            "otp.required" => "OTP Required"
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success
        $patient = Patient::firstWhere(["contact" => trim($request->input("contact"))]);
        
        // On Patient Not Found.
        if(!$patient){
            return $this->fail([], "No Such Contact Details Found.");
        }
        
        // On Patient Found.
        $url = "https://2factor.in/API/V1/" . config("constants.SMS_API_KEY") . "/SMS/VERIFY/" . $patient->session_id. "/". $request->input("otp");
        
        // Making Request
        $response = Http::get($url);
        
        // On OTP Verification Fail.
        if(!$response->ok() || (strtolower($response->json("Details")) === "otp expired") || (strtolower($response->json("Details")) === "otp mismatch")){
            return $this->fail([], $response->json("Details"));
        }
        
        // Changing OTP Verified Status.
        $patient->otp_verified = "yes";
        // $patient->session_id = "";
        $savePatient = $patient->save();

        // On OTP Verification Success But Regsitration Status Is Pending.
        if($patient->registration_status === "pending"){
            return $this->success($patient, "OTP Verified. Complete Your Registraion First.");
        }
        
        // On OTP Verification Success And Regsitration Status Is Also Completed.
        // Creating Patient's Login Session.
        $patientLoginSession = new PatientSession();
        $patientLoginSession->patient_id = $patient->id;
        $patientLoginSession->email = $patient->email;
        $login = $patientLoginSession->save();
        
        // On Failure Of Creating Login Session
        if(!$login){
            return $this->fail([], "Something Went Wrong While Login. Try Again...");
        }
        
        // On Success Of Creating Login Session.
        // Encrypting And Maintaning Patient's Session.
        $authToken = Crypt::encryptString($patientLoginSession);
        
        // Sending Auth Token For The Logged In Patient.
        $patient["auth_token"] = $authToken;
        
        // On Successful Creating Login Session.
        return $this->success($patient, "Logged In Successfully.");
    }

    #==============================================#
    #------------- REGISTER API FUNCTION ----------#
    #==============================================#
    
    public function register(Request $request){
        // Current Date So That Valid DOB Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));
        
        // Validation Rules.
        $rules = [
            "id" => "required",
            "name" => "required",
            "gender" => "required",
            "dob" => "required|date|before_or_equal:$now",
            "email" => "email"
        ];

        // Validaton Custom Messages.
        $messages = [
            "id.required" => "Patient ID Required",
            "name.required" => "Name Required",
            "gender.required" => "Gender Required",
            "email.email" => "Invalid Email Address",
            "dob.required" => "Date Of Birth Required",
            "dob.date" => "Invalid DOB",
            "dob.before_or_equal" => "Invalid Future DOB",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Updating Patient Details.
        $patient = Patient::find(trim($request->input("id")));
        
        // On Patient Not Found.
        if(!$patient){
            return $this->fail([], "No Patient Data Found.");            
        }

        // Storing New Patient's Details In Their Respective Column.
        $patient->name = trim($request->input("name"));
        $patient->gender = trim($request->input("gender"));
        $patient->dob = trim($request->input("dob"));
        $patient->email = trim($request->input("email"));
        $patient->registration_status = "completed";

        // On Updating Patient Model.
        $updatePatient = $patient->save();

        // On Registration Fail .
        if(!$updatePatient){
            return $this->fail([], "Registration Not Succeed. Try Again...");
        }

        // On Successful Registration
        // Creating Patient's Login Session.
        $patientLoginSession = new PatientSession();
        $patientLoginSession->patient_id = $patient->id;
        $patientLoginSession->email = $patient->email;
        $login = $patientLoginSession->save();
        
        // On Failure Of Creating Login Session
        if(!$login){
            return $this->fail([], "Something Went Wrong While Login. Try Again...");
        }
        
        // On Success Of Creating Login Session.
        // Encrypting And Maintaning Patient's Session.
        $authToken = Crypt::encryptString($patientLoginSession);
        
        // Sending Auth Token For The Logged In Patient.
        $patient["auth_token"] = $authToken;
        
        // On Successful Creating Login Session.
        return $this->success($patient, "Registraion Succeed.");
    }

    #==============================================#
    #---------- EDIT PROFILE API FUNCTION ---------#
    #==============================================#
    
    public function editProfile(Request $request){
        // Current Date So That Valid DOB Validation Could Be Made.
        $now = date("d-m-Y", strtotime(now()));
        
        // Validation Rules.
        $rules = [
            "name" => "required",
            "gender" => "required",
            "dob" => "required|date|before_or_equal:$now",
            "email" => "email"
        ];

        // Validaton Custom Messages.
        $messages = [
            "name.required" => "Name Required",
            "contact.required" => "Contact Required",
            "contact.min" => "Invalid Contact",
            "gender.required" => "Gender Required",
            "email.email" => "Invalid Email Address",
            "email.unique" => "Already A Registered Email Address.",
            "dob.required" => "Date Of Birth Required",
            "dob.date" => "Invalid DOB",
            "dob.before_or_equal" => "Invalid Future DOB",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Updating Patient Details.
        $patient = Patient::find(trim($request->input("auth_token")["patient_id"]));
        
        // On Patient Not Found.
        if(!$patient){
            return $this->fail([], "No Patient Data Found.");            
        }

        // Storing New Patient's Details In Their Respective Column.
        $patient->name = trim($request->input("name"));
        $patient->gender = trim($request->input("gender"));
        $patient->dob = trim($request->input("dob"));
        $patient->email = trim($request->input("email"));

        // On Updating Patient Model.
        $updateProfile = $patient->save();

        // On Updating Fail .
        if(!$updateProfile){
            return $this->fail([], "Profile Not Updated. Try Again...");
        }

        // On Updating Success.
        return $this->success($patient, "Profile Details Updated Successfully.");
    }

    #==============================================#
    #------------- LOGOUT API FUNCTION ------------#
    #==============================================#

    public function logout(Request $request){
        // Retrieving Patient Session Details.
        $PatientSession = PatientSession::firstWhere(["id" => $request->input("auth_token")["id"]]);
        
        // Updating Patient Logging Session For Logging Out.
        $PatientSession["status"] = "inactive";
        $logout = $PatientSession->save();
        
        // On Logging Out Fail.
        if(!$logout){
            return $this->fail([], "Something Went Wrong While Logging Out. Try Again...");
        }
        
        // On Successful Logging Out.
        return $this->success([], "Logged Out Successfully.");
    }
}