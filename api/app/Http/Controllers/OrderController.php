<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Address;
use App\Models\Member;
use App\Models\Order;
use App\Models\Observation;
use App\Models\Patient;
use App\Models\PatientPayment;
use App\Models\PatientTest;
use App\Models\SampleCollector;
use App\Models\Slot;
use App\Models\Test;

class OrderController extends BaseController{
    #==============================================#
    #---------- CREATE ORDER API FUNCTION ---------#
    #==============================================#
    
    public function createOrder(Request $request){
        // Validation Rules.
        $rules = [
            "address_id" => "required",
            "payment_mode" => "required",
        ];

        // Validaton Custom Messages.
        $messages = [
            "address_id.required" => "Address Id Required",
            "payment_mode.required" => "Payment Mode Required",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Creating Request Data For Making HTTP Client Request To ReviewMyOrder API.
        // Request URL.
        $url = "/cart/review-my-order";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];
        // Request Body.
        $body = $request->all();
        
        // Making HTTP Client Request To ReviewMyOrder API For reviewMyOrder Details?
        $reviewMyOrder = $this->httpRequest($url, $method, $body, $header);
    
        // On Fail Response Returned.
        if($reviewMyOrder->object()->status === "fail"){
            return $reviewMyOrder->json();
        }
    
        // On Success Response Returned.
        $orderDetails = $reviewMyOrder->object()->data;
        
        // Creating New Patient Test Model.
        $patientTestData = [];

        // Generating Unique Order Number.
        $orderNumber = bin2hex(random_bytes(4));

        foreach($orderDetails->tests as $test){
            $patientTestData[] = [
                "test_id" => $test->id,
                "order_number" => $orderNumber,
            ];
        }

        // Saving Patient Test Data.
        $savePatientTest = PatientTest::insert($patientTestData);

        // On Fail.
        if(!$savePatientTest){
            return $this->fail([], "Patient Test Details Not Saved. Try Again...");
        }

        // On Successful Saving Patient Test Data. Creating Payment Payment Data.
        $patientPayment = new PatientPayment();
        $patientPayment->order_number = $orderNumber;
        $patientPayment->patient_id = $orderDetails->patient->id;
        $patientPayment->member_id = $orderDetails->member ? $orderDetails->member->id : 0;
        $patientPayment->address_id = $orderDetails->address->id;
        $patientPayment->slot_id = $orderDetails->slot->id;
        $patientPayment->status_with_time = config("constants.ORDER_STATUS_WITH_TIME");
        $patientPayment->sub_total = $orderDetails->sub_total;
        $patientPayment->payable = $orderDetails->payable_total;
        $patientPayment->collection_charge = $orderDetails->sample_collection_charge;
        $patientPayment->payment_mode = trim($request->input("payment_mode"));
        $patientPayment->instruction = $request->filled("instruction") ? trim($request->input("instruction")) : "";
        $savePatientPayment = $patientPayment->save();

        // On Fail.
        if(!$savePatientPayment){
            return $this->fail([], "Order Not Placed. Try Again...");
        }
        
        // Creating Request Data For Making HTTP Client Request To ClearCart API.
        // Request URL.
        $url = "/cart/clear-cart";
        // Request Body.
        $body = ["member_id" => $orderDetails->member ? $orderDetails->member->id : 0];
        
        // Making HTTP Client Request To ClearCart API For Clearing Cart Details?
        $clearCart = $this->httpRequest($url, $method, $body, $header);
    
        // On Fail.
        if(!$clearCart->ok()){
            return $this->success($response->json()->data, "Order Successfully Placed. But Cart Not Cleared.");
        }

        // Updating Order Number In Patient Slot Model For Created Order.
        $slot = Slot::find($orderDetails->slot->id);
        $slot->order_number = $orderNumber;
        $updateSlot = $slot->save();                            
                           
        // On Fail.
        if(!$updateSlot){
            return $this->success(["order_number" => strtoupper($orderNumber)], "Order Successfully Placed But Slot Not Updated.");
        }
        
        // On Success.
        return $this->success(["order_number" => strtoupper($orderNumber)], "Order Successfully Placed.");
    }
    
    #==============================================#
    #------ TRACK MY ORDER BY ID API FUNCTION -----#
    #==============================================#
    
    public function trackMyOrder(Request $request){
        // Validation Rules.
        $rules = [ "order_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "order_id.required" => "Order ID Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
                                
        // Order Details.
        $data["order"] = PatientPayment::firstWhere([
                            "patient_id" => trim($request->input("auth_token")["patient_id"]),
                            "order_number" => trim($request->input("order_id"))
                        ]);

        // On Order Not Found.
        if(!$data["order"]){
            return $this->fail([], "No Such Order Details Found.");
        }

        // Patient Details.
        $data["patient"] = Patient::find($data["order"]->patient_id);
        
        // Member Details.
        $data["member"] = $data["order"]->member_id ? Member::find($data["order"]->member_id) : null;        

        // Address Details.
        $data["address"] = Address::find($data["order"]->address_id);
        
        // Slot Details.
        $data["slot"] = Slot::find($data["order"]->slot_id);
        
        // Sample Collector Details.
        $data["technician"] = SampleCollector::select("name", "contact")->find($data["order"]->sample_collector_id);

        // Test Details.
        $data["tests"] = DB::table("patient_payments as pp")
                            ->select("t.id", "t.name", "t.price")
                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                            ->join("tests as t", "t.id", "=", "pt.test_id")
                            ->where([
                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                "pp.order_number" => trim($request->input("order_id"))
                            ])->get();

        // Retrieving Parameters Covered Under The Test.
        if($data["tests"]){
            // Retrieving Parameters Covered Under The Test.
            foreach($data["tests"] as $test){
                $test->parameters = Observation::where([
                                            "test_id" => $test->id,
                                            "status" => "active"
                                        ])->get();
        
                // Total Number Of Paramerters Covered Under The Test.
                $test->parameters_covered = count($test->parameters);
            }
        }

        // On Success.
        return $this->success($data, "Order Details.");
    }
    
    #==============================================#
    #----- CANCEL MY ORDER BY ID API FUNCTION -----#
    #==============================================#
    
    public function cancelMyOrder(Request $request){
        // Validation Rules.
        $rules = [ "order_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "order_id.required" => "Order ID Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Creating Request Data For Making HTTP Client Request To TrackMyOrder API.
        // Request URL.
        $url = "/order/track-my-order";
        // Request Method.
        $method = "post";
        // Request Headers.
        $header = ["AUTHTOKEN" => trim($request->header("AUTHTOKEN"))];        
        // Request Body.
        $body = $request->all();
        
        // Making HTTP Client Request To TrackMyOrder API For TrackMyOrder Details?
        $trackMyOrder = $this->httpRequest($url, $method, $body, $header);
    
        // No Success Response Returned.
        if(!$trackMyOrder->ok()){
            return $trackMyOrder->json();
        }
    
        // On Success Response Data.
        $orderResponse = $trackMyOrder->object()->data;
        
        // On Order Not Found.
        if(!$orderResponse){
            return $this->fail([], "No Such Order Details Found.");
        }
        
        // If Order Already Cancelled.
        if(strtoupper($orderResponse->order->order_status) === "CANCELLED"){
            return $this->fail($orderResponse, "Order Already Cancelled. Your Refund Is In Progress.");
        }

        // If Order Already Cancelled And Amount Refunded.
        if(strtoupper($orderResponse->order->order_status) === "REFUNDED"){
            return $this->fail($orderResponse, "Order Already Cancelled And Amount Refunded Successfully.");
        }

        // If Sample Collected.
        if(
            ($orderResponse->order->order_status === "SAMPLE COLLECTED") ||
            ($orderResponse->order->order_status === "SAMPLE SUBMITTED") || 
            ($orderResponse->order->order_status === "REPORT ISSUED")
        ){
            return $this->fail($orderResponse, "Order Can Not Be Cancelled Because " . ucwords(strtolower($orderResponse->order->order_status)) . ".");
        }

        // Appending Cancel Status In Request Array.
        $request["order_status"] = config("constants.ORDER_STATUS.CANCELLED");
        
        // Creating Request Data For Making HTTP Client Request To UpdateOrderStatusById API.
        // Request URL.
        $url = "/order/update-order-status-by-id";
        // Request Body.
        $body = $request->all();
        
        // Making HTTP Client Request To UpdateOrderStatusById API For Updating Order Details?
        $updateOrderStatusById = $this->httpRequest($url, $method, $body, $header);
        
        // No Success Response Returned.
        if(!$updateOrderStatusById->ok()){
            return $updateOrderStatusById->json();
        }
    
        // On Success Response Returned.
        $updateOrderResponse = $updateOrderStatusById->object();
        
        // If Fail Response Returned.
        if($updateOrderResponse->status === "fail"){
            return $this->fail($updateOrderResponse->data, "Order Not Cancelled. Try Again...");
        }
        
        // If Success Response Returned.
        return $this->success($updateOrderResponse->data, "Order Cancelled Successfully. After Confirmation, Amount Will Be Refunded Soon.");
    }

    #==============================================#
    #--------- GET ALL ORDERS API FUNCTION --------#
    #==============================================#
    
    public function getAllOrders(Request $request){
        // Creating Default Empty AllOrders Array.
        $allOrders = [];
                                
        // Order Details.
        $orders = PatientPayment::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                ])
                                ->latest("created_at")
                                ->get();
        
        // On Order Not Found.
        if(!$orders){
            return $this->fail([], "No Order Details Found.");
        }

        // On Order Found.
        foreach($orders as $key => $order){
            // Patient Details.
            $allOrders[$key]["patient"] = Patient::find($order->patient_id);
            
            // Member Details.
            $allOrders[$key]["member"] = $order->member_id ? Member::find($order->member_id) : null;
        
            // Address Details.
            $allOrders[$key]["address"] = Address::find($order->address_id);
            
            // Slot Details.
            $allOrders[$key]["slot"] = Slot::find($order->slot_id);

            // Test Details.
            $allOrders[$key]["tests"] = DB::table("patient_payments as pp")
                                            ->select("t.id", "t.name", "t.price")
                                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                                            ->join("tests as t", "t.id", "=", "pt.test_id")
                                            ->where([
                                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                "pp.order_number" => $order->order_number
                                            ])->get();

            // Retrieving Parameters Covered Under The Test.
            if($allOrders[$key]["tests"]){
                // Retrieving Parameters Covered Under The Test.
                foreach($allOrders[$key]["tests"] as $test){
                    $test->parameters = Observation::where([
                                                "test_id" => $test->id,
                                                "status" => "active"
                                            ])->get();
        
                    // Total Number Of Paramerters Covered Under The Test.
                    $test->parameters_covered = count($test->parameters);
                }
            }
            
            // Order Details.
            $allOrders[$key]["order"] = $order;
        }

        // On Success.
        return $this->success(["orders" => $allOrders], "Orders Details.");
    }

    #==============================================#
    #------ GET ALL OPEN ORDERS API FUNCTION ------#
    #==============================================#
    
    public function getAllOpenOrders(Request $request){
        // Creating Default Empty AllOrders Array.
        $allOrders = [];
                                
        // Order Details.
        $orders = PatientPayment::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                ])
                                ->latest("created_at")
                                ->whereIn("order_status", [
                                                                config("constants.ORDER_STATUS.PLACED"),
                                                                config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED"),
                                                                config("constants.ORDER_STATUS.SAMPLE COLLECTED"),
                                                                config("constants.ORDER_STATUS.SAMPLE SUBMITTED"),
                                                            ])
                                ->get();
        
        // On Order Not Found.
        if(!$orders){
            return $this->fail([], "No Order Details Found.");
        }

        // On Order Found.
        foreach($orders as $key => $order){
            // Patient Details.
            $allOrders[$key]["patient"] = Patient::find($order->patient_id);
            
            // Member Details.
            $allOrders[$key]["member"] = $order->member_id ? Member::find($order->member_id) : null;
        
            // Address Details.
            $allOrders[$key]["address"] = Address::find($order->address_id);
            
            // Slot Details.
            $allOrders[$key]["slot"] = Slot::find($order->slot_id);

            // Test Details.
            $allOrders[$key]["tests"] = DB::table("patient_payments as pp")
                                            ->select("t.id", "t.name", "t.price")
                                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                                            ->join("tests as t", "t.id", "=", "pt.test_id")
                                            ->where([
                                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                "pp.order_number" => $order->order_number
                                            ])->get();

            // Retrieving Parameters Covered Under The Test.
            if($allOrders[$key]["tests"]){
                // Retrieving Parameters Covered Under The Test.
                foreach($allOrders[$key]["tests"] as $test){
                    $test->parameters = Observation::where([
                                                "test_id" => $test->id,
                                                "status" => "active"
                                            ])->get();
        
                    // Total Number Of Paramerters Covered Under The Test.
                    $test->parameters_covered = count($test->parameters);
                }
            }
            
            // Order Details.
            $allOrders[$key]["order"] = $order;
        }

        // On Success.
        return $this->success(["orders" => $allOrders], "All Open Orders Details.");
    }

    #==============================================#
    #------ GET ALL PAST ORDERS API FUNCTION ------#
    #==============================================#
    
    public function getAllPastOrders(Request $request){
        // Creating Default Empty AllOrders Array.
        $allOrders = [];
                                
        // Order Details.
        $orders = PatientPayment::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                ])
                                ->latest("created_at")
                                ->where("order_status", config("constants.ORDER_STATUS.REPORT ISSUED"))
                                ->get();
        
        // On Order Not Found.
        if(!$orders){
            return $this->fail([], "No Order Details Found.");
        }

        // On Order Found.
        foreach($orders as $key => $order){
            // Patient Details.
            $allOrders[$key]["patient"] = Patient::find($order->patient_id);
            
            // Member Details.
            $allOrders[$key]["member"] = $order->member_id ? Member::find($order->member_id) : null;
        
            // Address Details.
            $allOrders[$key]["address"] = Address::find($order->address_id);
            
            // Slot Details.
            $allOrders[$key]["slot"] = Slot::find($order->slot_id);

            // Test Details.
            $allOrders[$key]["tests"] = DB::table("patient_payments as pp")
                                            ->select("t.id", "t.name", "t.price")
                                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                                            ->join("tests as t", "t.id", "=", "pt.test_id")
                                            ->where([
                                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                "pp.order_number" => $order->order_number
                                            ])->get();

            // Retrieving Parameters Covered Under The Test.
            if($allOrders[$key]["tests"]){
                // Retrieving Parameters Covered Under The Test.
                foreach($allOrders[$key]["tests"] as $test){
                    $test->parameters = Observation::where([
                                                "test_id" => $test->id,
                                                "status" => "active"
                                            ])->get();
        
                    // Total Number Of Paramerters Covered Under The Test.
                    $test->parameters_covered = count($test->parameters);
                }
            }
            
            // Order Details.
            $allOrders[$key]["order"] = $order;
        }

        // On Success.
        return $this->success(["orders" => $allOrders], "All Past Orders Details.");
    }

    #==============================================#
    #---- GET ALL CANCELLED ORDERS API FUNCTION ---#
    #==============================================#
    
    public function getAllCancelledOrders(Request $request){
        // Creating Default Empty AllOrders Array.
        $allOrders = [];
                                
        // Order Details.
        $orders = PatientPayment::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0,
                                    "order_status" => config("constants.ORDER_STATUS.CANCELLED")
                                ])
                                ->latest("created_at")
                                ->get();
        
        // On Order Not Found.
        if(!$orders){
            return $this->fail([], "No Order Details Found.");
        }

        // On Order Found.
        foreach($orders as $key => $order){
            // Patient Details.
            $allOrders[$key]["patient"] = Patient::find($order->patient_id);
            
            // Member Details.
            $allOrders[$key]["member"] = $order->member_id ? Member::find($order->member_id) : null;
        
            // Address Details.
            $allOrders[$key]["address"] = Address::find($order->address_id);
            
            // Slot Details.
            $allOrders[$key]["slot"] = Slot::find($order->slot_id);

            // Test Details.
            $allOrders[$key]["tests"] = DB::table("patient_payments as pp")
                                            ->select("t.id", "t.name", "t.price")
                                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                                            ->join("tests as t", "t.id", "=", "pt.test_id")
                                            ->where([
                                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                "pp.order_number" => $order->order_number
                                            ])->get();

            // Retrieving Parameters Covered Under The Test.
            if($allOrders[$key]["tests"]){
                // Retrieving Parameters Covered Under The Test.
                foreach($allOrders[$key]["tests"] as $test){
                    $test->parameters = Observation::where([
                                                "test_id" => $test->id,
                                                "status" => "active"
                                            ])->get();
        
                    // Total Number Of Paramerters Covered Under The Test.
                    $test->parameters_covered = count($test->parameters);
                }
            }
            
            // Order Details.
            $allOrders[$key]["order"] = $order;
        }

        // On Success.
        return $this->success(["orders" => $allOrders], "All Cancelled Orders Details.");
    }

    #==============================================#
    #---- GET ALL REFUNDED ORDERS API FUNCTION ----#
    #==============================================#
    
    public function getAllRefundedOrders(Request $request){
        // Creating Default Empty AllOrders Array.
        $allOrders = [];
                                
        // Order Details.
        $orders = PatientPayment::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0,
                                    "order_status" => config("constants.ORDER_STATUS.REFUNDED")
                                ])
                                ->latest("created_at")
                                ->get();
        
        // On Order Not Found.
        if(!$orders){
            return $this->fail([], "No Order Details Found.");
        }

        // On Order Found.
        foreach($orders as $key => $order){
            // Patient Details.
            $allOrders[$key]["patient"] = Patient::find($order->patient_id);
            
            // Member Details.
            $allOrders[$key]["member"] = $order->member_id ? Member::find($order->member_id) : null;
        
            // Address Details.
            $allOrders[$key]["address"] = Address::find($order->address_id);
            
            // Slot Details.
            $allOrders[$key]["slot"] = Slot::find($order->slot_id);

            // Test Details.
            $allOrders[$key]["tests"] = DB::table("patient_payments as pp")
                                            ->select("t.id", "t.name", "t.price")
                                            ->join("patient_tests as pt", "pt.order_number", "=", "pp.order_number")
                                            ->join("tests as t", "t.id", "=", "pt.test_id")
                                            ->where([
                                                "pp.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                "pp.order_number" => $order->order_number
                                            ])->get();

            // Retrieving Parameters Covered Under The Test.
            if($allOrders[$key]["tests"]){
                // Retrieving Parameters Covered Under The Test.
                foreach($allOrders[$key]["tests"] as $test){
                    $test->parameters = Observation::where([
                                                "test_id" => $test->id,
                                                "status" => "active"
                                            ])->get();
        
                    // Total Number Of Paramerters Covered Under The Test.
                    $test->parameters_covered = count($test->parameters);
                }
            }
            
            // Order Details.
            $allOrders[$key]["order"] = $order;
        }

        // On Success.
        return $this->success(["orders" => $allOrders], "All Refunded Orders Details.");
    }
    
    #==============================================#
    #------- UPDATE ORDER BY ID API FUNCTION ------#
    #==============================================#
    
    public function updateOrderStatusById(Request $request){
        // Validation Rules.
        $rules = [
            "order_id" => "required",
            "order_status" => "required",
            "sample_collector_id" => "required_if:order_status, 1",

        ];

        // Validaton Custom Messages.
        $messages = [
            "order_id.required" => "Order ID Required",
            "order_status.required" => "Order Status Required",
            "sample_collector_id.required_if" => "Sample Collector Id",
        ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success. Retrieving Order Details.
        $order = PatientPayment::firstWhere([
                                "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                "order_number" => trim($request->input("order_id"))
                            ]);

        // On Order Not Found.
        if(!$order){
            return $this->fail([], "No Such Order Details Found.");
        }

        // If Status Already Assigned.
        if($order->order_status === trim($request->input("order_status"))){            
            // Returning Response.
            return $this->fail($order, "Order Is Already With This Status.");
        }

        // If Try To Skip Status On Updating.
        if((trim($request->input("order_status")) != config("constants.ORDER_STATUS.CANCELLED")) && (trim($request->input("order_status")) != config("constants.ORDER_STATUS.REFUNDED")) && ((int) $order->getRawOriginal("order_status")) != config("constants.ORDER_STATUS.CANCELLED")  && ((int) $order->getRawOriginal("order_status")) != config("constants.ORDER_STATUS.REFUNDED")){
            if(((int) $order->getRawOriginal("order_status") + 1) !== ((int) trim($request->input("order_status")))){
                // Creating Custom Response Message.
                $message = "Can Not Be Skipped Any Order Status. After " . ucwords(str_replace("_", " ", array_search($order->getRawOriginal("order_status"), config("constants.ORDER_STATUS")))) . ", Status Should Be " . ucwords(str_replace("_", " ", array_search(((int)$order->getRawOriginal("order_status") + 1), config("constants.ORDER_STATUS"))));            
    
                // Returning Response.
                return $this->fail($order, $message);
            }
        }

        // If Admin Try To Take Order In Back Status.
        if($order->getRawOriginal("order_status") > trim($request->input("order_status"))){ 
            // Returning Response.
            return $this->fail($order, "Order Status Can Not Be Changed To Back Status.");
        }

        // If Order Status Is 6 Which Means Cancelled.
        if(trim($request->input("order_status")) == 5){
            // If Report Issued.
            if($order->order_status === "REPORT ISSUED"){
                return $this->fail($order, "Order Can Not Be Cancelled Because Report Has Been Issued.");
            }
        }

        // If Order Status Is 6 Which Means Refunded.
        if(trim($request->input("order_status")) == 6){
            // If Report Issued.
            if($order->order_status === "REPORT ISSUED"){
                return $this->fail($order, "Order Can Not Be Cancelled Because Report Has Been Issued.");
            }
            
            if($order->order_status !== "CANCELLED"){
                // Returning Response.
                return $this->fail($order, "Only Cancelled Order Could Be Refunded.");
            }            
        }

        // On Order With Provided Status Not Found.
        // Creating Variable For Dynamic Updating statusWithTime Json Values.
        $statusWithTime = "status_with_time->" . array_keys(config("constants.ORDER_STATUS_WITH_TIME"))[trim($request->input("order_status"))];

        
        // Where Condition Array.
        $where = [
            "order_status" => trim($request->input("order_status")),
            $statusWithTime  => date("Y-m-d H:i:s", strtotime(now()))
        ];

        // If Order Status Is Technician Assigned Then Need To Be Update Confirmed Status Too.
        if(trim($request->input("order_status")) == 1){
            $where["status_with_time->confirmed"] =  date("Y-m-d H:i:s", strtotime(now()));
        }

        // If Order Status Is 3 Which Means Sample Collected.
        if((trim($request->input("order_status")) == 3) && $request->filled("collected_by")){
            $where["collected_by"] = trim($request->input("collected_by"));
        }

        // Updating Order Status.
        $updateOrderStatus = $order->update($where);

        // On Fail.
        if(!$updateOrderStatus){
            // Returning Response.
            return $this->success($order, "Order Status Not Updated. Try Again...");
        }

        // On Success.
        return $this->success($order, "Order Status Updated Successfully.");
    }

    #==================================================#
    #------- ORDER STATUS API RESPONSE FUNCTION -------#
    #==================================================#
    
    public function getAllOrderStatus(){
        // Retrieving All Available Order Status.
        $status = config("constants.ORDER_STATUS");

        // Unsetting Cancelled And Refunded Status.
        unset($status["CANCELLED"]);
        unset($status["REFUNDED"]);

        // Returning Response.
        return $this->success($status, "Order Status.");
    }

    #==================================================#
    #------ PAYMENT OPTION API RESPONSE FUNCTION ------#
    #==================================================#
    
    public function getAllPaymentOptions(){
        // Retrieving All Available Payment Options.
        $paymentOptions = config("constants.PAYMENT_OPTIONS");

        // Returning Response.
        return $this->success($paymentOptions, "Payment Options.");
    }
}