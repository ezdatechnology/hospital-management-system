<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Member;
use App\Models\Observation;
use App\Models\Patient;
use App\Models\Slot;
use App\Models\Test;

class CartController extends BaseController{
    #==============================================#
    #-------- ADD TEST IN CART API FUNCTION -------#
    #==============================================#
    
    public function addTestInCart(Request $request){
        // Validation Rules.
        $rules = [ "test_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "test_id.required" => "Test Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Checking For Test Exists?
        $testDetails = Test::find(trim($request->input("test_id")));

        // No Test Details Found.
        if(!$testDetails){
            return $this->fail([], "No Such Test Details Found.");
        }

        // Test Details Found. Creating whereData Array.
        $whereData["patient_id"] = trim($request->input("auth_token")["patient_id"]);
        $whereData["test_id"] = trim($request->input("test_id"));
        $whereData["member_id"] = $request->filled("member_id") ? trim($request->input("member_id")) : 0;
        
        // If Member ID Provided.
        if($request->filled("member_id")){
            // Member Details By ID.
            $member = Member::firstWhere([
                                "id" => trim($request->input("member_id")),
                                "patient_id" => trim($request->input("auth_token")["patient_id"])
                            ]);
    
            // If Member Not Found.
            if(!$member){
                return $this->fail([], "No Member Detials Found.");
            }
        }
        
        // On Validation Success Checking If Test Already Added In The Cart.
        $testAlreadyAdded = Cart::firstWhere($whereData);
        
        // If Test Not Already Added In The Cart Item Found.
        if($testAlreadyAdded){
            return $this->fail([], "Test Already Added In The Cart.");
        }

        // Saving Test Details In The Cart. Creating Cart New Cart Model.
        $cart = new Cart();
        $cart->patient_id = trim($request->input("auth_token")["patient_id"]);
        $cart->member_id = $request->filled("member_id") ? trim($request->input("member_id")) : 0;
        $cart->test_id = trim($request->input("test_id"));
        
        // On Saving Test Details In The Cart Model.
        $saveTest = $cart->save();

        // On Fail.
        if(!$saveTest){
            return $this->fail([], "Test Not Added In The Cart. Try Again...");
        }
        
        // On Success.
        return $this->success($cart, "Test Successfully Added In The Cart.");;
    }

    #==============================================#
    #----- GET CART DETAILS BY ID API FUNCTION ----#
    #==============================================#
    
    public function getCartDetails(Request $request){
        // Patient Details.
        $data["patient"] = Patient::find(trim($request->input("auth_token")["patient_id"]));

        // Member Details.
        $data["member"] = trim($request->filled("member_id")) ? Member::firstWhere([
                                                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                                                    "id" => trim($request->input("member_id"))
                                                                ])
                                                            : null;

        // Retrieving Cart Details.
        $data["tests"] = DB::table("carts as c")
                            ->select("c.id", "t.id as test_id", "t.name as test", "t.price")
                            ->where([
                                "c.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                "c.member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                            ])
                            ->join("tests as t", "t.id", "=", "c.test_id")
                            ->get();
                            
        // No Test Added In The Cart.
        if(!count($data["tests"])){
            $data["tests"] = null;
            
            // Returning Response.
            return $this->success($data, "Cart Details");
        }

        // Retrieving Parameters Covered Under The Test.
        foreach($data["tests"] as $test){
            $test->parameters = Observation::where([
                                        "test_id" => $test->test_id,
                                        "status" => "active"
                                    ])->get();

            // Total Number Of Paramerters Covered Under The Test.
            $test->parameters_covered = count($test->parameters);
        }
            
        // On Success
        return $this->success($data, "Cart Details");
    }

    #==============================================#
    #----- REVIEW MY ORDER BY ID API FUNCTION -----#
    #==============================================#
    
    public function reviewMyOrder(Request $request){
        // Validation Rules.
        $rules = [ "address_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "address_id.required" => "Address Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }
        
        // Creating Review My Order Resposne Empty Data Array.
        $reviewOrder["patient"] = null;
        $reviewOrder["member"] = null;
        $reviewOrder["address"] = null;
        $reviewOrder["slot"] = null;
        $reviewOrder["tests"] = null;
        $reviewOrder["sample_collection_charge"] = config("constants.SAMPLE_COLLECTION_CHARGE");
        $reviewOrder["sub_total"] = 0;
        $reviewOrder["payable_total"] = 0;

        // Retrieving Patient Details.
        $reviewOrder["patient"] = Patient::find(trim($request->input("auth_token")["patient_id"]));

        // No Patient Found.
        if(!$reviewOrder["patient"]){
            return $this->fail([], "No Patient Details Found.");
        }

        // If Member Id Provided.
        if($request->filled("member_id")){
            // Member Details.
            $reviewOrder["member"] = Member::firstWhere([
                                        "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                        "id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                    ]);
    
            // No Member Found.
            if(!$reviewOrder["member"]){
                return $this->fail([], "No Member Details Found.");
            }
        }        
        
        // Retrieving Address Details.
        $reviewOrder["address"] = Address::firstWhere([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "id" => trim($request->input("address_id"))
                                ]);

        // No Address Found.
        if(!$reviewOrder["address"]){
            return $this->fail([], "No Address Details Found.");
        }

        // Retrieving Slot Details.
        $reviewOrder["slot"] = Slot::where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0,
                                ])
                                ->whereDate("slot_date", ">=", date("Y-m-d", strtotime(now())),)
                                ->whereNull("order_number")
                                ->first();
        
        // No Patient Found.
        if(!$reviewOrder["slot"]){
            return $this->fail([], "You Have Not Booked Any Slot For Creating Order.");
        }

        // Retrieving Cart Details.
        $carts = Cart::firstWhere([
                        "patient_id" => trim($request->input("auth_token")["patient_id"]),
                        "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                    ]);

        // No Test Found In The Cart.
        if(!$carts){
            return $this->fail([], "Your Cart Doesn't Have Any Test For Creating Order.");
        }
        
        // On Cart Details Found.
        if($carts){            
            // Cart Response Data.
            $reviewOrder["tests"] = DB::table("carts as c")
                                        ->select("t.id", "t.name as test", "t.price")
                                        ->where([
                                            "c.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                            "c.member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                        ])
                                        ->join("tests as t", "t.id", "=", "c.test_id")
                                        ->get();

            // Retrieving Parameters Covered Under The Test.
            foreach($reviewOrder["tests"] as $test){
                $test->parameters = Observation::where([
                                            "test_id" => $test->id,
                                            "status" => "active"
                                        ])->get();
    
                // Total Number Of Paramerters Covered Under The Test.
                $test->parameters_covered = count($test->parameters);
            }
            
            // Retrieving Cart Sub Total.
            $reviewOrder["sub_total"] = DB::table("carts as c")
                                ->select(DB::raw("sum(t.price) as sub_total"))
                                ->where([
                                    "c.patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "c.member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                ])
                                ->join("tests as t", "t.id", "=", "c.test_id")
                                ->first()
                                ->sub_total;
    
            // Total Payable Amount.
            $reviewOrder["payable_total"] = $reviewOrder["sub_total"] + $reviewOrder["sample_collection_charge"];
        }
    
        // On Success. Returning Response Data 
        return $this->success($reviewOrder, "Order Details");
    }
    
    #==============================================#
    #--- REMOVE TEST FROM THE CART API FUNCTION ---#
    #==============================================#
    
    public function removeTestFromCart(Request $request){
        // Validation Rules.
        $rules = [ "cart_test_id" => "required" ];

        // Validaton Custom Messages.
        $messages = [ "cart_test_id.required" => "Cart Test Id Required" ];
        
        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);
    
        // On Validation Fail
        if($validator->fails()){
            // Converting Validation Errors Array Into Key Value Pair.
            foreach($validator->messages()->getMessages() as $key => $value){
                $errors[$key] = $value[0];
            }

            // Returning Response.
            return $this->fail($errors, "Validation Failed.");
        }

        // On Validation Success Getting Address Details By Id.
        $getItemFromCart = Cart::firstWhere([
                                "id" => trim($request->input("cart_test_id")),
                                "patient_id" => trim($request->input("auth_token")["patient_id"])
                            ]);
        
        // On Test Not Found In The Cart.
        if(!$getItemFromCart){
            return $this->fail([], "No Such Test Details Found In The Cart.");
        }

        // Removing Item Details From The Cart.
        $deleteItemFromCart = $getItemFromCart->delete();

        // On Deleting Fail.
        if(!$deleteItemFromCart){
            return $this->fail([], "Test Not Removed From The Cart. Try Again...");
        }
        
        // On Deleting Success.
        return $this->success([], "Test Details Successfully Removed From The Cart.");;
    }
    
    #==============================================#
    #----------- CLEAR CART API FUNCTION ----------#
    #==============================================#
    
    public function clearCart(Request $request){
        // Retrieving Cart Details.
        $getCartItemIds = Cart::select("id")
                                ->where([
                                    "patient_id" => trim($request->input("auth_token")["patient_id"]),
                                    "member_id" => trim($request->filled("member_id")) ? trim($request->input("member_id")) : 0
                                ])
                                ->get()
                                ->toArray();
        
        // On No Cart Data Found.
        if(empty($getCartItemIds)){
            return $this->fail([], "Cart Already Empty.");
        }
        
        // On Cart Data Found. Clearing Cart.
        $clearCart = Cart::destroy($getCartItemIds);
        
        // On Cart Not Clear.
        if(!$clearCart){
            return $this->fail([], "Cart Not Clear. Try Again...");
        }        
        
        // On Cart Successfully Clear.
        return $this->success([], "Cart Cleared Successfully.");
    }
}
