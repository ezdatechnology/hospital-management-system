<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    use HasFactory;

    // The attributes that are mass assignable.
    protected $fillable = [
        'slot_date',
        'slot_time',
        'patient_id',
        'member_id'
    ];

    // Converting PHP Date To MYSQL Date.
    public function setSlotDateAttribute($value){
        $this->attributes["slot_date"] = date('Y-m-d', strtotime($value));
    }

    // Converting MYSQL Date To PHP Date With Day And Month Name.
    public function getSlotDateAttribute($value){
        return date('d F, l', strtotime($value));
    }
        
    // Retrieving Slot Time From Slot Timings Array According To Slot Time Value.
    public function getSlotTimeAttribute($value){
        return config("constants.SLOT_TIMINGS." . $value);
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
