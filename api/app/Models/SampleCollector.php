<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Member;

class SampleCollector extends Model
{
    use HasFactory;

    // The attributes that are mass assignable.
    protected $fillable = [
        'name',
        'email',
        'contact',
        'gender',
        'dob',
        'password',
    ];

    // The attributes that are guarded.
    protected $guarded = [
        'registration_status',
        "unique_code",
        'session_id'
    ];

    // Converting PHP Date To MYSQL Date
    public function setDobAttribute($value){
        $this->attributes["dob"] = date('Y-m-d', strtotime($value));
    }

    // Converting MYSQL Date Into Human Readable Date.
    public function getDobAttribute($value){
        return date('d/M/Y', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
