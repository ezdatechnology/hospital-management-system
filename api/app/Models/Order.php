<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    // Converting Array To Json.
    protected $casts = [
        "test_id" => "array",
        "status_with_time" => "array",
    ];

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
