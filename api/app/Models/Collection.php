<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    use HasFactory;

    // The attributes that are mass assignable.
    protected $fillable = [
        'collection_date',
        'sample_collector_id',
        'amount',
        'deposited'
    ];

    // Converting PHP Date To MYSQL Date.
    public function setCollectionDateAttribute($value){
        $this->attributes["collection_date"] = date('Y-m-d', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCollectionDateAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
