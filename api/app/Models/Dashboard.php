<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    use HasFactory;

    // Converting MYSQL StatusWithTime Date Into Human Readable Date.
    public function getStatusWithTimeAttribute($value){
        $statusWithTime = json_decode($value, true);
        
        $statusWithTime["confirmed"] = $statusWithTime["confirmed"] ? date('d/M/Y h:i A', strtotime($statusWithTime["confirmed"])) : "";
        $statusWithTime["technician_assigned"] = $statusWithTime["technician_assigned"] ? date('d/M/Y h:i A', strtotime($statusWithTime["technician_assigned"])) : "";
        $statusWithTime["sample_collected"] = $statusWithTime["sample_collected"] ? date('d/M/Y h:i A', strtotime($statusWithTime["sample_collected"])) : "";
        $statusWithTime["sample_submitted"] = $statusWithTime["sample_submitted"] ? date('d/M/Y h:i A', strtotime($statusWithTime["sample_submitted"])) : "";
        $statusWithTime["report_issued"] = $statusWithTime["report_issued"] ? date('d/M/Y h:i A', strtotime($statusWithTime["report_issued"])) : "";
        $statusWithTime["cancelled"] = $statusWithTime["cancelled"] ? date('d/M/Y h:i A', strtotime($statusWithTime["cancelled"])) : "";
        $statusWithTime["refunded"] = $statusWithTime["refunded"] ? date('d/M/Y h:i A', strtotime($statusWithTime["refunded"])) : "";
        return $statusWithTime;
    }
}
