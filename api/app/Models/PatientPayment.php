<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientPayment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'status_with_time->confirmed',
        'status_with_time->technician_assigned',
        'status_with_time->sample_collected',
        'status_with_time->sample_submitted',
        'status_with_time->report_issued',
        'status_with_time->cancelled',
        'status_with_time->refunded',
        'order_status',
        "collected_by",
    ];

    // Converting Array To Json.
    protected $casts = [
        "test_id" => "array",
        // "status_with_time" => "array",
    ];

    // Converting PHP Date To MYSQL Date.
    public function setStatusWithTimeAttribute($value){
        $this->attributes["status_with_time"] = json_encode($value);
    }

    // Retrieving Order Status From Order Status Array According To Order Status Value.
    public function getOrderStatusAttribute($value){
        return array_search($value, config("constants.ORDER_STATUS"));
    }

    // Converting MYSQL StatusWithTime Date Into Human Readable Date.
    public function getStatusWithTimeAttribute($value){
        $statusWithTime = json_decode($value, true);
        
        $statusWithTime["confirmed"] = $statusWithTime["confirmed"] ? date('d/M/Y h:i A', strtotime($statusWithTime["confirmed"])) : "";
        $statusWithTime["technician_assigned"] = $statusWithTime["technician_assigned"] ? date('d/M/Y h:i A', strtotime($statusWithTime["technician_assigned"])) : "";
        $statusWithTime["sample_collected"] = $statusWithTime["sample_collected"] ? date('d/M/Y h:i A', strtotime($statusWithTime["sample_collected"])) : "";
        $statusWithTime["sample_submitted"] = $statusWithTime["sample_submitted"] ? date('d/M/Y h:i A', strtotime($statusWithTime["sample_submitted"])) : "";
        $statusWithTime["report_issued"] = $statusWithTime["report_issued"] ? date('d/M/Y h:i A', strtotime($statusWithTime["report_issued"])) : "";
        $statusWithTime["cancelled"] = $statusWithTime["cancelled"] ? date('d/M/Y h:i A', strtotime($statusWithTime["cancelled"])) : "";
        $statusWithTime["refunded"] = $statusWithTime["refunded"] ? date('d/M/Y h:i A', strtotime($statusWithTime["refunded"])) : "";
        return $statusWithTime;
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
