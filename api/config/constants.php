<?php
    // All Constants Goes Here...
    return [
        // SMS API KEY INFORMATION
        "SMS_API_KEY" => "45d11d6d-cde0-11e7-94da-0200cd936042",
        
        // LOCAL WEBSITE BASEURL
        "BASEURL" => "http://192.168.43.231/hms/api/public/api",
        // "BASEURL" => "http://192.168.43.154/hms/api/public/api",
        
        // SERVER WEBSITE BASEURL
        // "BASEURL" => "https://hms.ezdatechnology.com/hms/api/public/api",
        
        // PER DAY SLOT BOOKING LIMIT FOR A PARTICULAR TIME.
        "SLOT_BOOKING_LIMIT" =>  5,

        // LAB'S FIXED SAMPLE COLLECTION CHARGES.
        // "SAMPLE_COLLECTION_CHARGE" => 150,
        "SAMPLE_COLLECTION_CHARGE" => 0,
        
        // ALL AVAILABLE ORDER STATUS WITH TIME.
        "ORDER_STATUS_WITH_TIME" => [
            "confirmed"             => "",
            "technician_assigned"   => "",
            "sample_collected"      => "",
            "sample_submitted"      => "",
            "report_issued"         => "",
            "cancelled"             => "",
            "refunded"              => "",
            "rescheduled"           => "",
        ],

        // ALL AVAILABLE ORDER STATUS.
        "ORDER_STATUS" => [
            "PLACED"                => "0",
            "TECHNICIAN ASSIGNED"   => "1",
            "SAMPLE COLLECTED"      => "2",
            "SAMPLE SUBMITTED"      => "3",
            "REPORT ISSUED"         => "4",
            "CANCELLED"             => "5",
            "REFUNDED"              => "6",
            "RESCHEDULED"           => "7",
        ],

        // ALL AVAILABLE PAYMENT OPTIONS.
        "PAYMENT_OPTIONS" => [
            "CASH"      => "cash",
            "ONLINE"    => "online",
            "UPI"       => "upi",
        ],

        // ALL AVAILABLE SLOT TIMINGS.
        "SLOT_TIMINGS" => [
            "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 PM", "12:30 PM",
            "01:00 PM", "01:30 PM", "02:00 PM", "02:30 PM", "03:00 PM", "03:30 PM",
            "04:00 PM", "04:30 PM", "05:00 PM", "05:30 PM", "06:00 PM",
        ],

        // ALL AVAIABLE FAMILY RELATIONS.
        "FAMILY_RELATIONS" => [
            "Grandfather (Dadaji)",
            "Grandmother (Dadiji)",
            "Mother (Mata)",
            "Father (Pita)",
            "Brother (Bhai)",
            "Sister (Bahin)",
            "Uncle (Chacha)",
            "Aunty (Chachi)",
            "Son (Putra)",
            "Daughter (Putri)",
            "Grandson (Nati)",
            "Granddaughter (Natin)",
            "Nephew (Bhatija)",
            "Niece (Bhatiji)",
            "Nephew (Bhanja)",
            "Niece (Bhanji)",
            "Aunty (Bua)",
            "Uncle (Fufa)",
            "Maternal Uncle (Mama)",
            "Maternal Aunt (Mami)",
            "Aunty (Mausi)",
            "Uncle (Mausa)",
            "Mother in law (Sas)",
            "Father in law (Shwsur)",
            "Brother in law (Sala)",
            "Sister in law (Sali)",
            "Sister in law (Nand)",
            "Brother in law (Devar)",
            "Sister in law (Bhabhi)",
            "Husband (Pati)",
            "Wife (Patni)",
            "Brother in law (Jijaji)",
            "Son In Law (Damad)",
            "Aunty (Tai)",
            "Uncle (Tau)",
            "Daughter In Law (Putra Vadhu)",
            "Grandfather (Nana)",
            "Grandmother (Nani)",
            "Step Brother (Sautela Bahi)",
            "Step Sister (Sauteli Bahan)",
            "Step Mother (Sauteli Maa)",
            "Step Father (Sautala Pita)",
            "Adopted Son (God Liya Hua Beta)",
            "Adopted Daughter (God Li hui Beti)",
        ],
    ];