<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("department")->nullable();
            $table->float("price", 8, 2)->default(0);
            $table->enum("gender", ["male", "female", "both m/f"])->nullable("male");
            $table->enum("color_code", ["red", "green", "blue", "yellow", "black", "purple"])->nullable();
            $table->enum("test_type", ["sample_required", "sample_not_required", "segregation_required"])->default("sample_required");
            $table->enum("billing_category", ["special", "routine"])->default("routine");
            $table->integer("quantity")->default(0);
            $table->string("remark")->nullable();
            $table->enum("report_type", ["parth_numeric", "part_rich_text"])->default("parth_numeric");
            $table->enum("is_nabh", ["yes", "no"])->default("no");
            $table->enum("reporting", ["yes", "no"])->default("no");
            $table->enum("print_sample_name", ["yes", "no"])->default("no");
            $table->enum("booking", ["yes", "no"])->default("no");
            $table->enum("show_name_in_patient_report", ["yes", "no"])->default("no");
            $table->enum("online_report", ["yes", "no"])->default("no");
            $table->enum("is_active", ["yes", "no"])->default("no");
            $table->enum("is_urgent", ["yes", "no"])->default("no");
            $table->enum("print_separate", ["yes", "no"])->default("no");
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
