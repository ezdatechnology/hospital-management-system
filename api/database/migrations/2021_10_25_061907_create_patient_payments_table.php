<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_payments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("patient_id")->references("id")->on("patients")->default(0);
            $table->unsignedBigInteger("member_id")->references("id")->on("members")->default(0);
            $table->unsignedBigInteger("address_id")->references("id")->on("addresses")->default(0);
            $table->unsignedBigInteger("slot_id")->references("id")->on("slots")->default(0);
            $table->unsignedBigInteger("sample_collector_id")->references("id")->on("sample_collectors")->default(0);
            $table->json("status_with_time");
            $table->string("order_number");
            $table->string("report")->nullable();
            $table->enum("order_status", [0, 1, 2, 3, 4, 5, 6, 7])->default(0);
            $table->float("collection_charge")->default(0);
            $table->text("instruction")->nullable();
            $table->float("sub_total")->default(0);
            $table->float("payable")->default(0);
            $table->enum("payment_mode", ["cash", "online", "upi"])->default("cash");
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_payments');
    }
}
