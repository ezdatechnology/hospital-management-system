<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string('contact')->unique();
            $table->string("email")->nullable();
            $table->string("unique_code")->unique();
            $table->date("dob")->default("1996-11-06");
            $table->enum("gender", ["male", "female", "child"])->default("male");
            $table->enum("registration_status", ["pending", "completed"])->default("pending");
            $table->enum("otp_verified", ["yes", "no"])->default("no");
            $table->string("image", )->default("06.jpg");
            $table->string('session_id')->nullable();
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
