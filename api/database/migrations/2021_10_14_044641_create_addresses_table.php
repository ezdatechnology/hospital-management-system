<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("patient_id")->references("id")->on("patients");
            $table->text("address")->nullable();
            $table->string("house")->nullable();
            $table->string("pincode")->nullable();
            $table->string("locality")->nullable();
            $table->string("landmark")->nullable();
            $table->string("city")->nullable();
            $table->string("state")->nullable();
            $table->enum("type", ["home", "office", "other"])->default("home");
            $table->enum("is_primary", ["yes", "no"])->default("no");
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
