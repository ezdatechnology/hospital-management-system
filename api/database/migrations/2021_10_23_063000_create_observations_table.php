<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('observations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("test_id")->references("id")->on("tests")->default(0);
            $table->string("name");
            $table->string("short_name")->nullable();
            $table->string("units")->nullable();
            $table->string("suffix")->nullable();
            $table->string("min_value");
            $table->string("max_value");
            $table->string("ref_range");
            $table->string("remark");
            $table->enum("header", ["yes", "no"])->default("no");
            $table->enum("critical", ["yes", "no"])->default("no");
            $table->enum("comment", ["yes", "no"])->default("no");
            $table->enum("bold", ["yes", "no"])->default("no");
            $table->enum("underline", ["yes", "no"])->default("no");
            $table->enum("is_culture_report", ["yes", "no"])->default("no");
            $table->enum("show_in_patient_report", ["yes", "no"])->default("no");
            $table->enum("patient_separate", ["yes", "no"])->default("no");
            $table->enum("gender", ["male", "female", "both"])->default("male");
            $table->enum("status", ["active", "inactive"])->default("active");
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('observations');
    }
}
