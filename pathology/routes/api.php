<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PatientController;
// use App\Http\Controllers\TeacherController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

#==============================================#
#--------- AUTHENTICATION API ROUTES  ---------#
#==============================================#  
    
Route::post("/register", [PatientController::class, "register"]);
Route::post("/login", [PatientController::class, "login"]);
Route::post("/send-otp", [PatientController::class, "sendOTP"]);
Route::post("/verify-otp", [PatientController::class, "verifyOTP"]);
Route::post("/login-with-email-password", [PatientController::class, "loginWithEmailPassword"]);
Route::post("/login-with-contact-otp", [PatientController::class, "loginWithContactOTP"]);
Route::post("/logout", [PatientController::class, "logout"]);

#==============================================#
#------------- TEACHER API ROUTES  ------------#
#==============================================#  
    
// Route::post("/teacher/register", [TeacherController::class, "register"]);
