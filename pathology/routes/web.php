<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\PathologyController;
use App\Http\Middleware\AuthenticationMiddleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// // This is Home Page Routes.
// Route::get('/', function () {
//     return view('login');
// });

#==============================================#
#------------ AUTHENTICATION ROUTES -----------#
#==============================================#

Route::match(["get", "post"], "/login", [BaseController::class, "index"])->name("login");

Route::middleware(AuthenticationMiddleware::class)->group(function(){    
    Route::match(["get", "post"], "/logout", [BaseController::class, "logout"])->name("logout");
});

#==============================================#
#-------------- PATHOLOGY ROUTES --------------#
#==============================================#

Route::middleware(AuthenticationMiddleware::class)->group(function(){
	#==================================================#
	#----------- PATHOLOGY DASHBOARD ROUTES -----------#
	#==================================================#
	
    Route::match(["get", "post"], "/dashboard", [PathologyController::class, "index"])->name("pathology-dashboard");
    
	#==================================================#
	#----------- PATHOLOGY COLLECTOR ROUTES -----------#
	#==================================================#

    Route::match(["get", "post"], "/collectors", [PathologyController::class, "collectors"])->name("pathology-collectors");
    Route::match(["get", "post"], "/collector-add", [PathologyController::class, "addCollector"])->name("pathology-add-collector");
    Route::match(["get", "post"], "/collector-edit/{id}", [PathologyController::class, "editCollector"])->name("pathology-edit-collector");
    Route::match(["get", "post"], "/get-all-sample-collectors", [PathologyController::class, "getAllSampleCollectors"])->name("pathology-all-sample-collectors");
    Route::match(["get", "post"], "/get-collector-by-id", [PathologyController::class, "getCollectorById"])->name("pathology-get-collector-by-id");
    
    #==============================================#
    #-------------- STATE/CITY ROUTES -------------#
    #==============================================#
    
    Route::post('/getState', [PathologyController::class, 'getStates'])->name("pathology-get-states");
    Route::post('/getCity', [PathologyController::class, 'getCities'])->name("pathology-get-cities");
    
	#==================================================#
	#------------- PATHOLOGY ORDER ROUTES -------------#
	#==================================================#

    Route::match(["get", "post"], "/order-received", [PathologyController::class, "orderReceived"])->name("pathology-order-received");
    Route::match(["get", "post"], "/get-order-by-id", [PathologyController::class, "getOrderById"])->name("pathology-get-order-by-id");
    Route::match(["get", "post"], "/assign-sample-collector", [PathologyController::class, "assignSampleCollector"])->name("pathology-assign-sample-collector");
    Route::match(["get", "post"], "/all-orders", [PathologyController::class, "allOrders"])->name("pathology-all-orders");
    // Route::match(["get", "post"], "/search-orders", [PathologyController::class, "allOrders"])->name("pathology-all-orders");
    
	#==================================================#
	#------------ PATHOLOGY PATIENT ROUTES ------------#
	#==================================================#
    
    Route::match(["get", "post"], "/patient-members/{id}", [PathologyController::class, "patientMembers"])->name("pathology-patient-members");
    Route::match(["get", "post"], "/patient", [PathologyController::class, "patient"])->name("pathology-patient");
    Route::match(["get", "post"], "/patient-orders/{id}", [PathologyController::class, "patientOrders"])->name("pathology-patient-orders");
    Route::match(["get", "post"], "/patient-member-orders/{patient_id}/{member_id}", [PathologyController::class, "patientMemberOrders"])->name("pathology-patient-member-orders");
    Route::match(["get", "post"], "/get-patient-member-by-id", [PathologyController::class, "getPatientMemberById"])->name("pathology-get-patient-member-by-id");
    
	#==================================================#
	#------------ PATHOLOGY PAYMENT ROUTES ------------#
	#==================================================#
    Route::match(["get", "post"], "/payment", [PathologyController::class, "payment"])->name("pathology-payment");
});