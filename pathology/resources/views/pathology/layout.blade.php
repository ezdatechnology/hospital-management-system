<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>{{ !empty($title) ? $title : "Pathology Panel" }}</title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/pathology/images/favicon.ico') }}" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/bootstrap.min.css') }}"/>
        <!-- Typography CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/typography.css') }}" />
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/style.css') }}" />
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/responsive.css') }}" />
        <!-- Data Tables -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/jquery.dataTables.min.css') }}" />
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/toastr.css') }}" />
        <!-- Full calendar -->
        <!--<link href="{{ asset('assets/pathology/fullcalendar/core/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/daygrid/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/timegrid/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/list/main.css') }}" rel="stylesheet" />-->

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" />
    </head>
    <body>
        <!-- loader Start -->
        <div id="loading">
            <div id="loading-center"></div>
        </div>
        <!-- loader END -->
        <!-- Wrapper Start -->
        <div class="wrapper">
            <!-- Sidebar  -->
            <div class="iq-sidebar">
                <div class="iq-sidebar-logo d-flex justify-content-between">
                    <a href="index.html">
                        <!-- <img src="{{ asset('assets/pathology/images/demo2.png') }}" class="img-fluid" alt="" /> -->
                        <span>Logo Here</span>
                    </a>
                    <div class="iq-menu-bt-sidebar">
                        <div class="iq-menu-bt align-self-center">
                            <div class="wrapper-menu">
                                <div class="main-circle"><i class="ri-more-fill"></i></div>
                                <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="sidebar-scrollbar">
                    <nav class="iq-sidebar-menu">
                        <ul id="iq-sidebar-toggle" class="iq-menu">
                            <li class="<?php echo (!empty($active) && ($active === 'Dashboard')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-dashboard') }}" class="iq-waves-effect"><i class="ri-hospital-fill"></i><span>Dashboard</span></a>
                            </li>
                            <li class="<?php echo (!empty($active) && ($active === 'Collector')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-collectors') }}" class="iq-waves-effect"><i class="ri-user-add-fill"></i><span>Collectors</span></a>
                            </li>
                            <li class="<?php echo (!empty($active) && ($active === 'Order Received')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-order-received') }}" class="iq-waves-effect"><i class="ri-calendar-2-fill"></i><span>Today Orders</span></a>
                            </li>
                            <li class="<?php echo (!empty($active) && ($active === 'All Orders')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-all-orders') }}" class="iq-waves-effect"><i class="ri-calendar-2-fill"></i><span>All Orders</span></a>
                            </li>
                            <li class="<?php echo (!empty($active) && ($active === 'Patient')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-patient') }}" class="iq-waves-effect"><i class="ri-user-fill"></i><span>Patient</span></a>
                            </li>
                            <li class="<?php echo (!empty($active) && ($active === 'Payment')) ? 'active main-active' : ''; ?>">
                                <a href="{{ route('pathology-payment') }}" class="iq-waves-effect"><i class="ri-hospital-fill"></i><span>Payment</span></a>
                            </li>
                        </ul>
                    </nav>
                    <div class="p-3"></div>
                </div>
            </div>

            <!-- Page Content  -->
            <div id="content-page" class="content-page">
                <!-- TOP Nav Bar -->
                <div class="iq-top-navbar">
                    <div class="iq-navbar-custom">
                        <div class="iq-sidebar-logo">
                            <div class="top-logo">
                                <a href="{{ route('pathology-dashboard') }}" class="logo">
                                    <img src="{{ asset('assets/pathology/images/demo2.png') }}" class="img-fluid" alt="" />
                                    <!-- <span>XRay</span> -->
                                </a>
                            </div>
                        </div>
                        <nav class="navbar navbar-expand-lg navbar-light p-0">
                            <!-- <div class="iq-search-bar">
                                <form action="#" class="searchbox">
                                    <input type="text" class="text search-input" placeholder="Type here to search..." />
                                    <a class="search-link" href="#"><i class="ri-search-line"></i></a>
                                </form>
                            </div> -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <i class="ri-menu-3-line"></i>
                            </button>
                            <div class="iq-menu-bt align-self-center">
                                <div class="wrapper-menu">
                                    <div class="main-circle"><i class="ri-more-fill"></i></div>
                                    <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                                </div>
                            </div>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto navbar-list">
                                    <li class="nav-item">
                                        <a href="#" class="search-toggle iq-waves-effect">
                                            <i class="ri-notification-3-fill"></i>
                                            <span class="bg-danger dots"></span>
                                        </a>
                                        <div class="iq-sub-dropdown">
                                            <div class="iq-card shadow-none m-0">
                                                <div class="iq-card-body p-0">
                                                    <div class="bg-primary p-3">
                                                        <h5 class="mb-0 text-white">All Notifications<small class="badge badge-light float-right pt-1">4</small></h5>
                                                    </div>

                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/01.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Emma Watson Bini</h6>
                                                                <small class="float-right font-size-12">Just Now</small>
                                                                <p class="mb-0">95 MB</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="images\user\02.jpg" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">New customer is join</h6>
                                                                <small class="float-right font-size-12">5 days ago</small>
                                                                <p class="mb-0">Jond Bini</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/03.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Two customer is left</h6>
                                                                <small class="float-right font-size-12">2 days ago</small>
                                                                <p class="mb-0">Jond Bini</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/04.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">New Mail from Fenny</h6>
                                                                <small class="float-right font-size-12">3 days ago</small>
                                                                <p class="mb-0">Jond Bini</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a href="#" class="search-toggle iq-waves-effect">
                                            <i class="ri-mail-open-fill"></i>
                                            <span class="bg-primary count-mail"></span>
                                        </a>
                                        <div class="iq-sub-dropdown">
                                            <div class="iq-card shadow-none m-0">
                                                <div class="iq-card-body p-0">
                                                    <div class="bg-primary p-3">
                                                        <h5 class="mb-0 text-white">All Messages<small class="badge badge-light float-right pt-1">5</small></h5>
                                                    </div>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/01.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Bini Emma Watson</h6>
                                                                <small class="float-left font-size-12">13 Jun</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src='{{ asset("assets/pathology/images/user/02.jpg") }}' alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Lorem Ipsum Watson</h6>
                                                                <small class="float-left font-size-12">20 Apr</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/03.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Why do we use it?</h6>
                                                                <small class="float-left font-size-12">30 Jun</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/04.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Variations Passages</h6>
                                                                <small class="float-left font-size-12">12 Sep</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="iq-sub-card">
                                                        <div class="media align-items-center">
                                                            <div class="">
                                                                <img class="avatar-40 rounded" src="{{ asset('assets/pathology/images/user/05.jpg') }}" alt="" />
                                                            </div>
                                                            <div class="media-body ml-3">
                                                                <h6 class="mb-0">Lorem Ipsum generators</h6>
                                                                <small class="float-left font-size-12">5 Dec</small>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <ul class="navbar-list">
                                <li>
                                    <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                                        <img src="{{ asset('assets/pathology/images/user/1.jpg') }}" class="img-fluid rounded mr-3" alt="user" />
                                        <div class="caption">
                                            <h6 class="mb-0 line-height"> {{ session()->get("login")["first_name"] }} {{ session()->get("login")["last_name"] }}</h6>
                                            <span class="font-size-12">Available</span>
                                        </div>
                                    </a>
                                    <div class="iq-sub-dropdown iq-user-dropdown">
                                        <div class="iq-card shadow-none m-0">
                                            <div class="iq-card-body p-0">
                                                <div class="bg-primary p-3">
                                                    <h5 class="mb-0 text-white line-height">Hello {{ session()->get("login")["first_name"] }} {{ session()->get("login")["last_name"] }}</h5>
                                                    <span class="text-white font-size-12">Available</span>
                                                </div>
                                                <a href="{{ asset('pathology/doctor-view') }}" class="iq-sub-card iq-bg-primary-hover">
                                                    <div class="media align-items-center">
                                                        <div class="rounded iq-card-icon iq-bg-primary">
                                                            <i class="ri-file-user-line"></i>
                                                        </div>
                                                        <div class="media-body ml-3">
                                                            <h6 class="mb-0">My Profile</h6>
                                                            <p class="mb-0 font-size-12">View personal profile details.</p>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="profile-edit.html" class="iq-sub-card iq-bg-primary-hover">
                                                    <div class="media align-items-center">
                                                        <div class="rounded iq-card-icon iq-bg-primary">
                                                            <i class="ri-profile-line"></i>
                                                        </div>
                                                        <div class="media-body ml-3">
                                                            <h6 class="mb-0">Edit Profile</h6>
                                                            <p class="mb-0 font-size-12">Modify your personal details.</p>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="account-setting.html" class="iq-sub-card iq-bg-primary-hover">
                                                    <div class="media align-items-center">
                                                        <div class="rounded iq-card-icon iq-bg-primary">
                                                            <i class="ri-account-box-line"></i>
                                                        </div>
                                                        <div class="media-body ml-3">
                                                            <h6 class="mb-0">Account settings</h6>
                                                            <p class="mb-0 font-size-12">Manage your account parameters.</p>
                                                        </div>
                                                    </div>
                                                </a>
                                                <a href="privacy-setting.html" class="iq-sub-card iq-bg-primary-hover">
                                                    <div class="media align-items-center">
                                                        <div class="rounded iq-card-icon iq-bg-primary">
                                                            <i class="ri-lock-line"></i>
                                                        </div>
                                                        <div class="media-body ml-3">
                                                            <h6 class="mb-0">Privacy Settings</h6>
                                                            <p class="mb-0 font-size-12">Control your privacy parameters.</p>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="d-inline-block w-100 text-center p-3">
                                                    <a class="bg-primary iq-sign-btn" href="{{ route('logout') }}" role="button">Sign out<i class="ri-login-box-line ml-2"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- TOP Nav Bar END -->
                
                @yield("main-content")
                
                @yield("modal")
            
                <!-- Footer -->
                <footer class="bg-white iq-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-inline mb-0">
                                    <li class="list-inline-item"><a href="privacy-policy.html">Privacy Policy</a></li>
                                    <li class="list-inline-item"><a href="terms-of-service.html">Terms of Use</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-6 text-right">Copyright 2021 <a href="#">Ezdat Technology Private Limited</a> All Rights Reserved.</div>
                        </div>
                    </div>
                </footer>
                <!-- Footer END -->
            </div>
        </div>
        <!-- Wrapper END -->        

        @section("modal")            
            <div class="modal fade bd-example-modal-lg" tabindex="-1" id="patientdetails" role="dialog"  aria-hidden="true">
                <div class="modal-dialog  modal-dialog-centered modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">View Patient Details</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="rounded text-center">
                                <div class="donter-profile">
                                    <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' class="img-fluid rounded-circle" id="patient_member_image_pd" alt="user-img">
                                </div>
                            </div>
                            <div class="doctor-description mt-3">
                                <table class="table mb-0">
                                    <tbody>
                                        <tr>
                                            <th>Patient ID - Name :</th>
                                            <td id="patient_member_name_unique_code_pd"></td>
                                        </tr>
                                        <tr>
                                            <th>Contact No :</th>
                                            <td id="patient_member_contact_pd"></td>
                                        </tr>
                                        <tr>
                                            <th>Email ID :</th>
                                            <td id="patient_member_email_pd"></td>
                                        </tr>
                                        <tr>
                                            <th>Address :</th>
                                            <td id="patient_member_address_pd"></td>
                                        </tr>
                                        <tr>
                                            <th>Created Date & Time :</th>
                                            <td id="patient_member_created_pd"></td>
                                        </tr>
                                        <!-- <tr>
                                            <th>Status :</th>
                                            <td id="patient_member_order_status_pd">Active</td>
                                        </tr> -->
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bd-example-modal-lg" tabindex="-1" id="uploadreport" role="dialog"  aria-hidden="true">
                <div class="modal-dialog  modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Upload Report</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="rounded text-center">
                                <div class="donter-profile">
                                    <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' class="img-fluid rounded-circle" alt="user-img" id="patient_member_image">
                                </div>
                                <div class="doctor-detail mt-3">
                                    <h5 id="patient_member_name"></h5>
                                </div>
                                <div class="doctor-description">
                                    <table id="" class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <th>Patient ID:</th>
                                                <td id="patient_member_unique_code">CM9999</td>
                                            </tr>
                                            <tr>
                                                <th>Placed Date:</th>
                                                <td id="order_created">01/Dec/2021 9.33 AM</td>
                                            </tr>
                                            <tr>
                                                <th>Upload Report:</th>
                                                <td>
                                                    <form action='' method="post" id="form-upload-report" enctype="multipart/form-data">
                                                        <div class="form-group col-md-12 mb-0">
                                                            @csrf
                                                            <input type="hidden" name="order_number" id="order_number" />
                                                            <input type="file" name="order_number" id="order_number" />
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="sumbit" form="form-assign-sample-collector" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade bd-example-modal-lg" tabindex="-1" id="collectordetails" role="dialog"  aria-hidden="true">
                <div class="modal-dialog  modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">View Collector Details</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="rounded text-center">
                                <div class="donter-profile">
                                    <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' id="collector_image" class="img-fluid rounded-circle" id="patient_member_image" alt="user-img">
                                </div>
                                <div class="doctor-detail mt-3">
                                    <h5 id="collector_name"></h5>
                                </div>
                                <div class="doctor-description">
                                    <table id="" class="table mb-0">
                                        <tbody>
                                            <tr>
                                                <th>Collector ID - Name :</th>
                                                <td id="collector_unique_code"> EA1897 - Parth</td>
                                            </tr>
                                            <tr>
                                                <th>Contact No :</th>
                                                <td id="collector_contact">901474575</td>
                                            </tr>
                                            <tr>
                                                <th>Email ID :</th>
                                                <td id="collector_email">parth11gupta@gmail.com</td>
                                            </tr>
                                            <tr>
                                                <th>Address :</th>
                                                <td id="collector_address">12/24 karol bagh - Delhi, Delhi, India (306116)</td>
                                            </tr>
                                            <tr>
                                                <th>Created Date & Time :</th>
                                                <td id="collector_created">26/Nov/2021 10:29 AM</td>
                                            </tr>
                                            <tr>
                                                <th>Status :</th>
                                                <td id="collector_status">Active</td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @show
        
        @section("javascript")
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <script src="{{ asset('assets/pathology/js/jquery.min.js') }}"></script>
            <script src="{{ asset('assets/pathology/js/popper.min.js') }}"></script>
            <script src="{{ asset('assets/pathology/js/bootstrap.min.js') }}"></script>
            <!-- Appear JavaScript -->
            <script src="{{ asset('assets/pathology/js/jquery.appear.js') }}"></script>
            <!-- Countdown JavaScript -->
            <script src="{{ asset('assets/pathology/js/countdown.min.js') }}"></script>
            <!-- Counterup JavaScript -->
            <script src="{{ asset('assets/pathology/js/waypoints.min.js') }}"></script>
            <script src="{{ asset('assets/pathology/js/jquery.counterup.min.js') }}"></script>
            <!-- Wow JavaScript -->
            <script src="{{ asset('assets/pathology/js/wow.min.js') }}"></script>
            <!-- Apexcharts JavaScript -->
            <script src="{{ asset('assets/pathology/js/apexcharts.js') }}"></script>
            <!-- Slick JavaScript -->
            <script src="{{ asset('assets/pathology/js/slick.min.js') }}"></script>
            <!-- Select2 JavaScript -->
            <script src="{{ asset('assets/pathology/js/select2.min.js') }}"></script>
            <!-- Owl Carousel JavaScript -->
            <script src="{{ asset('assets/pathology/js/owl.carousel.min.js') }}"></script>
            <!-- Magnific Popup JavaScript -->
            <script src="{{ asset('assets/pathology/js/jquery.magnific-popup.min.js') }}"></script>
            <!-- Smooth Scrollbar JavaScript -->
            <script src="{{ asset('assets/pathology/js/smooth-scrollbar.js') }}"></script>
            <!-- lottie JavaScript -->
            <script src="{{ asset('assets/pathology/js/lottie.js') }}"></script>
            <!-- am core JavaScript -->
            <script src="{{ asset('assets/pathology/js/core.js') }}"></script>
            <!-- am charts JavaScript -->
            <script src="{{ asset('assets/pathology/js/charts.js') }}"></script>
            <!-- am animated JavaScript -->
            <script src="{{ asset('assets/pathology/js/animated.js') }}"></script>
            <!-- am kelly JavaScript -->
            <script src="{{ asset('assets/pathology/js/kelly.js') }}"></script>
            <!-- Flatpicker Js -->
            <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
            <!-- Chart Custom JavaScript -->
            <script src="{{ asset('assets/pathology/js/chart-custom.js') }}"></script>
            <!-- Custom JavaScript -->
            <script src="{{ asset('assets/pathology/js/custom.js') }}"></script>
            <!-- Data Tables -->
            <script src="{{ asset('assets/pathology/js/jquery.dataTables.min.js') }}"></script>
            <!-- Toastr -->
            <script src="{{ asset('assets/pathology/js/toastr.min.js') }}"></script>
            
            <script type="text/javascript">
                $('.select2').select2();
                
                $(document).ready(function(){
                    // Assign Sample Collector.
                    $(document).on("click", ".assign-order", function(e){
                        $.ajax({
                            url: '{{ route("pathology-get-order-by-id") }}',
                            type: "post",
                            data: { 
                                "order_number" : $(this).attr("order_number").trim(),
                                "_token" : '{{ csrf_token() }}'
                            },
                        })
                        .done(function(responseData){
                            if(responseData.status == "success"){
                                if(responseData.data.member_name == null){
                                    let patientImageUrl = '{{ config("constants.IMAGE_URL") . "uploads/patient/images/"}}' + responseData.data.patient_image;

                                    $("#patient_member_name").text(responseData.data.patient_name);
                                    $("#patient_member_unique_code").text(responseData.data.patient_unique_code.toUpperCase());
                                    $("#patient_member_image").attr("src", patientImageUrl); 
                                    $("#order_number").val(responseData.data.order_number);
                                    $("#order_created").text(responseData.data.created_at);
                                    $("#sample_collector_id").val(responseData.data.sample_collector_id);
                                }
                                else{
                                    let memberImageUrl = '{{ config("constants.IMAGE_URL") . "uploads/member/images/"}}' + responseData.data.member_image;

                                    $("#patient_member_name").text(responseData.data.member_name);
                                    $("#patient_member_unique_code").text(responseData.data.member_unique_code.toUpperCase());
                                    $("#patient_member_image").attr("src", memberImageUrl);
                                    $("#order_number").val(responseData.data.order_number);
                                    $("#order_created").text(responseData.data.created_at);
                                    $("#sample_collector_id").val(responseData.data.sample_collector_id);
                                }
                            } else{
                                console.log(responseData);
                            }
                        });
                    });

                    // Retrieving Patient Member Details On Modal.
                    $(document).on("click", ".patient-member-details", function(e){
                        $.ajax({
                            url: '{{ route("pathology-get-patient-member-by-id") }}',
                            type: "post",
                            data: { 
                                "order_number" : $(this).attr("order_number").trim(),
                                "_token" : '{{ csrf_token() }}'
                            },
                        })
                        .done(function(responseData){
                            if(responseData.status == "success"){
                                if(responseData.data.member == null){
                                    let patientImageUrl = '{{ config("constants.IMAGE_URL") . "uploads/patient/images/"}}' + responseData.data.patient.image;

                                    $("#patient_member_name_unique_code_pd").text(responseData.data.patient.name + " - " + responseData.data.patient.unique_code.toUpperCase());
                                    $("#patient_member_contact_pd").text(responseData.data.patient.contact);
                                    $("#patient_member_email_pd").text(responseData.data.patient.email);
                                    $("#patient_member_address_pd").text(
                                        responseData.data.address.house + " " + responseData.data.address.landmark + " " +
                                        responseData.data.address.locality + " " + responseData.data.address.city + " " +
                                        responseData.data.address.state + " - (" + responseData.data.address.pincode + ")"
                                    );
                                    $("#patient_member_image_pd").attr("src", patientImageUrl); 
                                    $("#order_number_pd").val(responseData.data.order.order_number);
                                    $("#patient_member_created_pd").text(responseData.data.patient.created_at);
                                }
                                else{
                                    let memberImageUrl = '{{ config("constants.IMAGE_URL") . "uploads/member/images/"}}' + responseData.data.member.image;

                                    $("#patient_member_name_unique_code_pd").text(responseData.data.member.name + " - " + responseData.data.member.unique_code.toUpperCase());
                                    $("#patient_member_contact_pd").text(responseData.data.member.contact);
                                    $("#patient_member_email_pd").text(responseData.data.member.email);
                                    $("#patient_member_address_pd").text(
                                        responseData.data.address.house + " " + responseData.data.address.landmark + " " +
                                        responseData.data.address.locality + " " + responseData.data.address.city + " " +
                                        responseData.data.address.state + " - (" + responseData.data.address.pincode + ")"
                                    );
                                    $("#patient_member_image_pd").attr("src", memberImageUrl); 
                                    $("#order_number_pd").val(responseData.data.order.order_number);
                                    $("#patient_member_created_pd").text(responseData.data.member.created_at);
                                }
                            } else{
                                console.log(responseData);
                            }
                        });
                    });

                    //Retrieving Collector details
                    $(document).on("click", ".collector-details", function(e){
                        $.ajax({
                            url: '{{route("pathology-get-collector-by-id")}}',
                            type: "post",
                            data: {
                                "_token": "{{csrf_token()}}",
                                "collector_id": $(this).attr("collector_id").trim(),
                            }
                        })
                        .done(function(responseData){                            
                            if(responseData.status == "success") {
                                let collectorImageUrl = '{{ asset("uploads/collector/images") }}' + "/" + responseData.data.collector.image;
                                
                                $('#collector_image').attr("src", collectorImageUrl);
                                $('#collector_unique_code').text(responseData.data.collector.unique_code.toUpperCase() + " - " + responseData.data.collector.name);
                                $('#collector_contact').text(responseData.data.collector.contact);
                                $("#collector_email").text(responseData.data.collector.email);
                                $("#collector_address").text(responseData.data.collector.address + " - " +
                                                            responseData.data.collector.city + ", " + responseData.data.collector
                                                            .state + ", " + responseData.data.collector.country + " - (" +
                                                            responseData.data.collector.pincode + ")"
                                                        );
                                $('#collector_created').text(responseData.data.collector.created_at);
                                $("#collector_status").text(responseData.data.collector.status);
                            } else {
                                console.log(responseData);
                            }
                        }).fail(function(responseData){
                            console.log(responseData);
                        });
                    })

                    // Applying Toastr On Success Message.
                    if('{{ session()->has("status") }}' && '{{ session()->get("status") }}' == "success"){
                        toastr.success('{{ session()->get("message") }}');
                    }

                    // Applying Toastr On Fail Message.
                    if('{{ session()->has("status") }}' && '{{ session()->get("status") }}' == "danger"){
                        toastr.error('{{ session()->get("message") }}');
                    }
                });
            </script>
        @show
    </body>
</html>