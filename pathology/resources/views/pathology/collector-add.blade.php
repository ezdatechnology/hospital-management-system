@extends("pathology.layout")
@section("main-content")
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb iq-bg-primary mb-0">
                        <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('pathology-collectors') }}">Collectors
                                Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Collector</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Add New Collector</h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="new-user-info">
                        <form action='{{ route("pathology-add-collector") }}' method="post" id="add-new-collector" form="add-new-collector" enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <div class="form-group col-md-4">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value='{{ old("name") }}' />
                                    
                                    @if($errors->addcollector->first("name"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("name") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact">Contact No:</label>
                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" value='{{ old("contact") }}'>
                                    
                                    @if($errors->addcollector->first("contact"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("contact") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value='{{ old("email") }}'>
                                    
                                    @if($errors->addcollector->first("email"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("email") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="address">Address:</label>
                                    <input type="text" class="form-control" id="address" placeholder="Address" name="address" value='{{ old("address") }}'>
    
                                    @if($errors->addcollector->first("address"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("address") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Country">Country:</label>
                                    <select id="country" name="country" class="form-control" required>
                                        <option>Select Country</option>
                                        @foreach ($countries as $country)
                                            <option {{ (old("country") == $country->id) ? "selected=selected" : "" }} value="{{ $country->id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->addcollector->first("country"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("country") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="state">State:</label>
                                    <select id="state" name="state" class="form-control">
                                        <option>Select State</option>                                        
                                        @foreach ($states as $state)
                                            @if(old("state") == $state->id)
                                                <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                            @elseif(old("state") != $state->id && old("country") == $state->country_id)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->addcollector->first("state"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("state") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="city">City:</label>
                                    <select id="city" name="city" class="form-control">
                                        <option selected>Select City</option>
                                        @foreach ($cities as $city)
                                            @if(old("city") == $city->id)
                                                <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                            @elseif(old("city") != $city->id && old("state") == $city->state_id)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->addcollector->first("city"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("city") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="pincode">Pincode:</label>
                                    <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode" value='{{ old("pincode") }}'>
                                    
                                    @if($errors->addcollector->first("pincode"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("pincode") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dob">DOB:</label>
                                    <input type="date" class="form-control" id="dob" name="dob" value='{{ old("dob") }}'>
                                    
                                    @if($errors->addcollector->first("dob"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("dob") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="gender">Gender:</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Gender</option>
                                        <option {{ old("gender") === "male" ? "selected=selected" : "" }} value="male">Male</option>
                                        <option {{ old("gender") === "female" ? "selected=selected" : "" }} value="female">Female</option>
                                        <option {{ old("gender") === "other" ? "selected=selected" : "" }} value="other">Other</option>
                                    </select>
                                    
                                    @if($errors->addcollector->first("gender"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("gender") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="vehicle_no">Vehicle No:</label>
                                    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Vehicle No" value='{{ old("vehicle_no") }}'>
                                    @if($errors->addcollector->first("vehicle_no"))
                                    <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("vehicle_no") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="collector_image">Image:</label>
                                    <input type="file" class="form-control" id="collector_image" name="collector_image">
                                    
                                    @if($errors->addcollector->first("collector_image"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("collector_image") }}</i>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn iq-bg-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@parent
<script>
    jQuery(document).ready(function() {
        // Retrieving All States On Changing Of Country.
        jQuery('#country').change(function() {
            console.log("hello");
            // Retrieving Country ID.
            let cid = jQuery(this).val();
            
            // Creating Select City Option Element.
            jQuery('#city').html('<option value="">Select City</option>')

            // Making Ajax Request To Get States According To Country ID.
            $.ajax({
                url: '{{ route("pathology-get-states") }}',
                type: 'post',
                data: { "cid" : cid, "_token": '{{csrf_token()}}' }
            }).done(function(result) {
                // Creating Select State Option Element.
                let html = '<option value="">Select State</option>';
                
                // Looping Over States For Appending All States In The Dropdown.
                for (state of result['states']) {
                    html += '<option value="'+ state['id'] + '">' + state['name'] + '</option>';
                }
                
                // Finally Appending All State In The State Select Element.
                jQuery('#state').html(html)
            }).fail(function(data) {
                console.log(data);
            });
        });

        // Retrieving All Cities On Changing Of State.
        jQuery('#state').change(function(){
            // Retrieving State ID.
            let sid = jQuery(this).val();

            // Making Ajax Request To Get Cities According To State ID.
            jQuery.ajax({
                url: '{{ route("pathology-get-cities") }}',
                type: 'post',
                data: { "sid" : sid, "_token" : '{{csrf_token()}}' }
            }).done(function(result) {
                // Creating Select City Option Element.
                let html = '<option value="">Select City</option>';

                // Looping Over Citiest For Appending All Citiest In The Dropdown.
                for (city of result['cities']) {
                    html += '<option value="' + city['id'] + '">' + city['name'] + '</option>';
                }

                // Finally Appending All Citiest In The City Select Element.
                jQuery('#city').html(html)
            }).fail(function(data) {
                console.log(data);
            });
        });
    });
</script>
@endsection