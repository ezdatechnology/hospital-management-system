@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb iq-bg-primary mb-0">
                            <li class="breadcrumb-item"><a href="#"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Patient Members</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!-- <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-primary rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-primary"><i class="ri-user-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter"></span></h2>
                                            <h5 class="">Total Orders</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-warning rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-warning"><i class="ri-women-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter"></span></h2>
                                            <h5 class="">In Processing</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-danger rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-danger"><i class="ri-group-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter"></span></h2>
                                            <h5 class="">Complete Orders</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-info rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-info"><i class="ri-hospital-line"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter"></span></h2>
                                            <h5 class="">Total Earning</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            <div class="col-lg-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Patient Members </h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table mb-0 table-borderless data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">Member ID | Name</th>
                                        <th scope="col">Created Date & Time</th>
                                        <th scope="col" class="text-center">Orders</th>
                                        <!-- <th scope="col" class="text-center">Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $IMAGE_URL = config("constants.IMAGE_URL");
                                    @endphp
                                    
                                    @if(!empty($members))
                                        @foreach($members as $member)
                                            <tr>
                                                <td>
                                                    <div class="training-block d-flex align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $member->image }}'  class="img-fluid" alt="icon">
                                                        </div>
                                                        <div class="ml-3">
                                                            <h5 class="">{{ strtoupper($member->unique_code) }} - {{ ucwords($member->name) }} </h5>
                                                            <p class="mb-0">{{ $member->contact }} </p>
                                                            <p class="mb-0">{{ $member->email }} </p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $member->created_at }}</td>
                                                <td class="text-center">
                                                    <a href='{{ route("pathology-patient-member-orders", ["patient_id" => $member->patient_id, "member_id" => $member->id]) }}' class="btn btn-primary">Show Orders</a>
                                                </td> 
                                                <!-- <td class="text-center">
                                                    <div class="iq-card-header-toolbar">
                                                        <div class="dropdown show">
                                                            <span class="dropdown-toggle text-primary" id="dropdownMenuButton50" data-toggle="dropdown" aria-expanded="true" role="button">
                                                                <i class="ri-more-2-line"></i>
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <a class="dropdown-item" href="#"><i class="ri-eye-line mr-2"></i>Edit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td> -->
                                            </tr>                                        
                                        @endforeach
                                    @else
                                        <tr><td colspan="4" class="text-mute text-center">No member Registered Yet.</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content End Here -->
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection