@extends("pathology.layout")
@section("main-content")
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb iq-bg-primary mb-0">
                        <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('pathology-collectors') }}">Collectors
                                Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Collector</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                        <h4 class="card-title">Edit Collector</h4>
                    </div>
                </div>
                <div class="iq-card-body">
                    <div class="new-user-info">
                        <form action='{{ route("pathology-edit-collector", ["id" => $collector->id]) }}' method="post" id="add-new-collector" form="add-new-collector" enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <div class="form-group col-md-4">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ isset($collector->name) ? $collector->name : '' }}" />
                                    @if($errors->editcollector->first("name"))
                                    <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("name") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="contact">Contact No:</label>
                                    <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact" value="{{ isset($collector->contact) ? $collector->contact : '' }}" />
                                    @if($errors->editcollector->first("contact"))
                                    <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("contact") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ isset($collector->email) ? $collector->email : '' }}" />
                                    @if($errors->editcollector->first("email"))
                                    <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("email") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="address">Address:</label>
                                    <input type="text" class="form-control" id="address" placeholder="Address" name="address" value="{{ isset($collector->address) ? $collector->address : '' }}" />
                                    
                                    @if($errors->editcollector->first("address"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("address") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="Country">Country:</label>
                                    <select id="country" name="country" class="form-control" required>
                                        <option>Select Country</option>
                                    
                                        @foreach ($countries as $country)
                                            @if ($country->id == $collector->country)
                                                <option value="{{$country->id}}" selected>
                                                    {{ $country->name }}
                                                </option>
                                            @else
                                                <option value="{{$country->id}}">{{ $country->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->editcollector->first("country"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("country") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="state">State:</label>
                                    <select id="state" name="state" class="form-control">
                                        <option>Select State</option>
                                        
                                        @foreach ($states as $state)
                                            @if ($state->id==$collector->state)
                                                <option value="{{ $state->id }}" selected>{{ $state->name }}</option>
                                            @elseif($state->id != $collector->state)
                                                <option value="{{ $state->id }}">{{ $state->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->editcollector->first("state"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("state") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="city">City:</label>
                                    <select id="city" name="city" class="form-control">
                                        <option selected>Select City</option>
                                        
                                        @foreach ($cities as $city)
                                            @if ($city->id == $collector->city)
                                                <option value="{{ $city->id }}" selected>{{ $city->name }}</option>
                                            @elseif($city->id != $collector->city)
                                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    
                                    @if($errors->editcollector->first("city"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("city") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dob">DOB:</label>
                                    <input type="date" class="form-control" id="dob" name="dob" value="{{ isset($collector->dob) ? $collector->getRawOriginal('dob') : '' }}" />
                                    
                                    @if($errors->editcollector->first("dob"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("dob") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="gender">Gender:</label>
                                    <select class="form-control" id="gender" name="gender">
                                        <option value="">Gender</option>
                                        <option {{ (isset($collector->dob) && ($collector->gender == "male")) ? "selected" : '' }} value="male">Male</option>
                                        <option {{ (isset($collector->dob) && ($collector->gender == "female")) ? "selected" : '' }} value="female">Female</option>
                                        <option {{ (isset($collector->dob) && ($collector->gender == "other")) ? "selected" : '' }} value="other">Other</option>
                                    </select>
                                    
                                    @if($errors->editcollector->first("gender"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("gender") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="vehicle_no">Vehicle No:</label>
                                    <input type="text" class="form-control" id="vehicle_no" name="vehicle_no" placeholder="Vehicle No" value="{{ isset($collector->vehicle_no) ? $collector->vehicle_no : '' }}" />
                                    @if($errors->addcollector->first("vehicle_no"))
                                    <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("vehicle_no") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="pincode">Pincode:</label>
                                    <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Pincode" value="{{ isset($collector->pincode) ? $collector->pincode : '' }}" />
                                    
                                    @if($errors->addcollector->first("pincode"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("pincode") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="status">Status:</label>
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Status</option>
                                        <option {{ (isset($collector->status) && ($collector->status == "active")) ? "selected" : '' }} value="active">Active</option>
                                        <option {{ (isset($collector->status) && ($collector->status == "inactive")) ? "selected" : '' }} value="inactive">Inactive</option>
                                    </select>
                                    
                                    @if($errors->editcollector->first("status"))
                                        <i class="text-danger font-weight-bold">{{ $errors->editcollector->first("status") }}</i>
                                    @endif
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="collector_image">Image:</label>
                                    <input type="file" class="form-control" id="collector_image" name="collector_image">
                                    
                                    @if($errors->addcollector->first("collector_image"))
                                        <i class="text-danger font-weight-bold">{{ $errors->addcollector->first("collector_image") }}</i>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <button type="reset" class="btn iq-bg-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
@parent
<script>
    jQuery(document).ready(function() {
        // Retrieving All States On Chaning Of Country.
        jQuery('#country').change(function() {
            // Retrieving Country ID.
            let cid = jQuery(this).val();
            
            // Creating Select City Option Element.
            jQuery('#city').html('<option value="">Select City</option>')

            // Making Ajax Request To Get States According To Country ID.
            $.ajax({
                url: '{{ route("pathology-get-states") }}',
                type: 'post',
                data: { "cid" : cid, "_token": '{{csrf_token()}}' }
            }).done(function(result) {
                // Creating Select State Option Element.
                let html = '<option value="">Select State</option>';
                
                // Looping Over States For Appending All States In The Dropdown.
                for (state of result['states']) {
                    html += '<option value="'+ state['id'] + '">' + state['name'] + '</option>';
                }
                
                // Finally Appending All State In The State Select Element.
                jQuery('#state').html(html)
            }).fail(function(data) {
                console.log(data);
            });
        });

        // Retrieving All Cities On Changing Of State.
        jQuery('#state').change(function(){
            // Retrieving State ID.
            let sid = jQuery(this).val();

            // Making Ajax Request To Get Cities According To State ID.
            jQuery.ajax({
                url: '{{ route("pathology-get-cities") }}',
                type: 'post',
                data: { "sid" : sid, "_token" : '{{csrf_token()}}' }
            }).done(function(result) {
                // Creating Select City Option Element.
                let html = '<option value="">Select City</option>';

                // Looping Over Citiest For Appending All Citiest In The Dropdown.
                for (city of result['cities']) {
                    html += '<option value="' + city['id'] + '">' + city['name'] + '</option>';
                }

                // Finally Appending All Citiest In The City Select Element.
                jQuery('#city').html(html)
            }).fail(function(data) {
                console.log(data);
            });
        });
    });
</script>
@endsection