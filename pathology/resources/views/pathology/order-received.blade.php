@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb iq-bg-primary mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Orders Received</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                            <div class="iq-card-body iq-bg-primary rounded">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="rounded-circle iq-card-icon bg-primary"><i class="ri-user-fill"></i></div>
                                    <div class="text-right">
                                        <h2 class="mb-0"><span class="counter"> {{ $totalPlacedOrders }} </span></h2>
                                        <h5 class="">Total Orders</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                            <div class="iq-card-body iq-bg-warning rounded">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="rounded-circle iq-card-icon bg-warning"><i class="ri-women-fill"></i></div>
                                    <div class="text-right">
                                        <h2 class="mb-0"><span class="counter"> {{ $totalInProcessingOrders }}</span></h2>
                                        <h5 class="">In Processing</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                            <div class="iq-card-body iq-bg-danger rounded">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="rounded-circle iq-card-icon bg-danger"><i class="ri-group-fill"></i></div>
                                    <div class="text-right">
                                        <h2 class="mb-0"><span class="counter"> {{ $totalInLabOrders }} </span></h2>
                                        <h5 class="">In Lab</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                            <div class="iq-card-body iq-bg-info rounded">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="rounded-circle iq-card-icon bg-info"><i class="ri-hospital-line"></i></div>
                                    <div class="text-right">
                                        <h2 class="mb-0"><span class="counter"> {{ $totalCompletedOrders }} </span></h2>
                                        <h5 class="">Complete</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="col-lg-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Orders Received</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-order-received-tab-fill" data-toggle="pill" href="#pills-order-received-fill" role="tab" aria-controls="pills-order-received" aria-selected="true">Order Received</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-collector-assignd-tab-fill" data-toggle="pill" href="#pills-collector-assign-fill" role="tab" aria-controls="pills-collector-assign" aria-selected="true">Collector Assign</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-sample-collect-tab-fill" data-toggle="pill" href="#pills-sample-collect-fill" role="tab" aria-controls="pills-sample-collect" aria-selected="false">Sample Collect</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-deposite-tab-fill" data-toggle="pill" href="#pills-deposite-fill" role="tab" aria-controls="pills-deposite" aria-selected="false">Sample Deposite in Lab</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-gengrate-tab-fill" data-toggle="pill" href="#pills-gengrate-fill" role="tab" aria-controls="pills-gengrate" aria-selected="false">Report Gengrate</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-rescheduled-tab-fill" data-toggle="pill" href="#pills-rescheduled-fill" role="tab" aria-controls="pills-rescheduled" aria-selected="false">Rescheduled</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-cancelled-tab-fill" data-toggle="pill" href="#pills-cancelled-fill" role="tab" aria-controls="pills-cancelled" aria-selected="false">Cancelled</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent-1">
                                <div class="tab-pane fade show active" id="pills-order-received-fill" role="tabpanel" aria-labelledby="pills-order-received-tab-fill">
                                    <div class="table-responsive">
                                        <table id="received-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Placed Date & Time</th>
                                                    <th scope="col">Slot Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $IMAGE_URL = config("constants.IMAGE_URL");
                                                @endphp
                                                
                                                @if(!empty($placedOrders))
                                                    @foreach($placedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                            <p class="mb-0">{{ $order->patient_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at)) }}</td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                                    <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords( strtolower($order->member_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->member_contact }}</p>
                                                                            <p class="mb-0">{{ $order->member_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at) ) }}</td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                                    <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade show" id="pills-collector-assign-fill" role="tabpanel" aria-labelledby="pills-collector-assign-tab-fill">
                                    <div class="table-responsive">
                                        <table id="assigned-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Assigned Date & Time</th>
                                                    <th scope="col">Slot Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if($collectorAssignedOrders)
                                                    @foreach($collectorAssignedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                            <p class="mb-0">{{ $order->patient_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->status_with_time["technician_assigned"])) }}</td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords( strtolower($order->member_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->member_contact }}</p>
                                                                            <p class="mb-0">{{ $order->member_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->status_with_time["technician_assigned"])) }}</td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-sample-collect-fill" role="tabpanel" aria-labelledby="pills-sample-collect-tab-fill">
                                    <div class="table-responsive">
                                        <table id="collected-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Collected Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($sampleCollectedOrders))
                                                    @foreach($sampleCollectedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                        <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                            </tr>                                                        
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-deposite-fill" role="tabpanel" aria-labelledby="pills-deposite-tab-fill">
                                    <div class="table-responsive">
                                        <table id="submitted-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Collected | Deposite Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($sampleSubmittedOrders))
                                                    @foreach($sampleSubmittedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                    <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                        <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }} 
                                                                    |
                                                                    {{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_submitted"])) }} 
                                                                </td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td data-toggle="modal" data-target="#uploadreport"><button class="btn btn-primary">Upload Report</button></td> 
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}
                                                                    |
                                                                    {{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_submitted"])) }}
                                                                </td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td data-toggle="modal" data-target="#uploadreport"><button class="btn btn-primary">Upload Report</button></td> 
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-gengrate-fill" role="tabpanel" aria-labelledby="pills-gengrate-tab-fill">
                                    <div class="table-responsive">
                                        <table id="report-generated-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Report Issued Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($reportIssuedOrders))
                                                    @foreach($reportIssuedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["report_issued"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#uploadreport"><i class="ri-check-line"></i>Upload Report</button>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["report_issued"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#uploadreport"><i class="ri-check-line"></i>Upload Report</button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-rescheduled-fill" role="tabpanel" aria-labelledby="pills-rescheduled-tab-fill">
                                    <div class="table-responsive">
                                        <table id="rescheduled-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Rescheduled Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($rescheduledOrders))
                                                    @foreach($rescheduledOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $order->sample_collector_id }}'>
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("uploads/collector/images/" . $order->sample_collector_image) }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-cancelled-fill" role="tabpanel" aria-labelledby="pills-cancelled-tab-fill">
                                    <div class="table-responsive">
                                        <table id="cancelled-orders" class="table mb-0 table-borderless text-nowrap data-table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Order ID</th>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Cancelled Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($cancelledOrders))
                                                    @foreach($cancelledOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["cancelled"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>{{ strtoupper($order->order_number) }}</td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}'  class="rounded-circle avatar-50" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["cancelled"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection

@section("modal")
    @parent
    <div class="modal fade bd-example-modal-lg" tabindex="-1" id="assignorder" role="dialog"  aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="rounded text-center">
                        <div class="donter-profile">
                            <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' class="rounded-circle avatar-50 rounded-circle" alt="user-img" id="patient_member_image">
                        </div>
                        <div class="doctor-detail mt-3">
                            <h5 id="patient_member_name"></h5>
                        </div>
                        <div class="doctor-description">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <th>Patient ID :</th>
                                        <td id="patient_member_unique_code"></td>
                                    </tr>
                                    <tr>
                                        <th>Placed Date :</th>
                                        <td id="order_created"></td>
                                    </tr>
                                    <tr>
                                        <th>Assign Collector :</th>
                                        <td>
                                            <form action='{{ route("pathology-assign-sample-collector") }}' method="post" id="form-assign-sample-collector">
                                                <div class="form-group col-md-12 mb-0">
                                                    @csrf
                                                    <input type="hidden" name="order_number" id="order_number" />
                                                    <select class="form-control" id="sample_collector_id" name="sample_collector_id">
                                                        <option value="0">Select Collector</option>
                                                        @if($collectors)
                                                            @foreach($collectors as $collector)
                                                                <option value="{{ $collector->id }}">{{ $collector->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="sumbit" form="form-assign-sample-collector" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".text-nowrap data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection