@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb iq-bg-primary mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">All Orders Received</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Patient Orders Received</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-borderless text-nowrap data-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order ID</th>
                                            <th scope="col">Patient ID | Name</th>
                                            <th scope="col">Placed Date & Time</th>
                                            <th scope="col">Slot Date & Time</th>
                                            <th scope="col">Report Generate Date & Time</th>
                                            <th scope="col">Test Name</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Status</th>
                                            <!-- <th scope="col">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $IMAGE_URL = config("constants.IMAGE_URL");
                                        @endphp
                                        
                                        @if(!empty($patientOrders))
                                            @foreach($patientOrders as $order)                                                
                                                <tr>
                                                    <td>{{ strtoupper($order->order_number) }}</td>
                                                    <td>
                                                        <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                            <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}' class="rounded-circle avatar-50" alt="icon">
                                                            </div>
                                                            <div class="ml-3">
                                                                <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                <p class="mb-0">{{ $order->patient_email }}</p>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at)) }}</td>
                                                    <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                    <td>{{ $order->status_with_time["report_issued"] ? date("d-M-Y h:i: A", strtotime($order->status_with_time["report_issued"])) : "00:00:0000 00:00:00" }}</td>
                                                    <td>{{ $order->tests }}</td>
                                                    <td>₹ {{ $order->payable }}</td>
                                                    <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                    <!-- <td>
                                                        <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                        <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                    </td> -->
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection