@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->    
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb iq-bg-primary mb-0">
                            <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Collectors</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Collectors</h4>
                        </div>
                        <div class="iq-card-header-toolbar d-flex align-items-center">
                            <a href='{{ route("pathology-add-collector") }}' class="btn btn-primary"><i class="ri-add-line mr-1 "></i>Add Collector</a>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table mb-0 table-borderless data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">Collector ID | Name</th>
                                        <th scope="col">Contact</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Vehicle No</th>
                                        <th scope="col">Address</th>
                                        <th scope="col">Created Date & Time</th>
                                        <th scope="col">Status</th>
                                        <th scope="col" class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $IMAGE_URL = config("constants.IMAGE_URL");
                                    @endphp
                                    
                                    @if(count($collectors))
                                        @foreach($collectors as $collector)
                                            <tr>
                                                <td>
                                                    <div class="training-block d-flex align-items-center collector-details" data-toggle="modal" data-target="#collectordetails" collector_id='{{ $collector->id }}'>
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                            <img src='{{ asset("/uploads/collector/images/" . $collector->image) }}' class="rounded-circle avatar-50" alt="icon">
                                                        </div>
                                                        <div class="ml-3">
                                                            <h5 class="">{{ strtoupper($collector->unique_code) }} - {{ $collector->name }}</h5>
                                                            <p class="mb-0">{{ $collector->contact }}</p>
                                                            <p class="mb-0">{{ $collector->email }}</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $collector->contact }}</td>
                                                <td>{{ $collector->email }}</td>
                                                <td>{{ $collector->vehicle_no }}</td>
                                                <td>{{ $collector->address }} - {{ $collector->city }}, {{ $collector->state }}, {{ $collector->country }} - ({{ $collector->pincode }})</td>
                                                <td>{{ date("d/M/Y h:i A", strtotime($collector->created_at)) }}</td>
                                                <td>{{ ucwords($collector->status) }}</td>
                                                <td class="text-center">
                                                    <div class="iq-card-header-toolbar">
                                                        <div class="dropdown show">
                                                            <span class="dropdown-toggle text-primary" id="dropdownMenuButton50" data-toggle="dropdown" aria-expanded="true" role="button">
                                                                <i class="ri-more-2-line"></i>
                                                            </span>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <a class="dropdown-item" href='{{ route("pathology-edit-collector", ["id" => $collector->id]) }}'><i class="ri-eye-line mr-2"></i>Edit</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content End Here -->
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection