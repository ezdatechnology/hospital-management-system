@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb iq-bg-primary mb-0">
                                <li class="breadcrumb-item"><a href="#"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Orders Received</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Orders Received</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <ul class="nav nav-pills mb-3 nav-fill" id="pills-tab-1" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-home-tab-fill" data-toggle="pill" href="#pills-home-fill" role="tab" aria-controls="pills-home" aria-selected="true">Order Received</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab-fill" data-toggle="pill" href="#pills-profile-fill" role="tab" aria-controls="pills-profile" aria-selected="false">Sample Collect</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab-fill" data-toggle="pill" href="#pills-contact-fill" role="tab" aria-controls="pills-contact" aria-selected="false">Sample Deposite in Lab</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab-fill" data-toggle="pill" href="#pills-report-fill" role="tab" aria-controls="pills-contact" aria-selected="false">Report Gengrate</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent-1">
                                <div class="tab-pane fade show active" id="pills-home-fill" role="tabpanel" aria-labelledby="pills-home-tab-fill">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-borderless">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Placed Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($placedOrders))
                                                    @foreach($placedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->patient_image) }}' class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                            <p class="mb-0">{{ $order->patient_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at)) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#assignorder"><i class="ri-check-line"></i>Assign Order</button>
                                                                    <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->member_image) }}' class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords( strtolower($order->member_name)) }}</h5>
                                                                            <p class="mb-0">{{ $order->member_contact }}</p>
                                                                            <p class="mb-0">{{ $order->member_email }}</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at) ) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#assignorder"><i class="ri-check-line"></i>Assign Order</button>
                                                                    <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="6" class="text-mute text-center">No Today Placed Orders Yet.</td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-profile-fill" role="tabpanel" aria-labelledby="pills-profile-tab-fill">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-borderless">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Collected Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Amount</th>
                                                    <th scope="col">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($sampleCollectedOrders))
                                                    @foreach($sampleCollectedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->patient_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->member_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                        <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>₹ {{ $order->payable }}</td>
                                                                <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                            </tr>                                                        
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="6" class="text-mute text-center">No Today Sample Collected Orders Yet.</td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-contact-fill" role="tabpanel" aria-labelledby="pills-contact-tab-fill">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-borderless">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Collected | Deposite Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($sampleSubmittedOrders))
                                                    @foreach($sampleSubmittedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                    <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                        <img src='{{ asset("assets/pathology/images/user/" . $order->patient_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                    <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}
                                                                    |
                                                                    {{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_submitted"])) }}
                                                                </td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->member_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_collected"])) }}
                                                                    |
                                                                    {{ date("d/M/Y h:i A", strtotime($order->status_with_time["sample_submitted"])) }}
                                                                </td>
                                                                <td>{{ $order->tests }}</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="6" class="text-mute text-center">No Today Sample Deposited Orders Yet.</td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-report-fill" role="tabpanel" aria-labelledby="pills-report-tab-fill">
                                    <div class="table-responsive">
                                        <table class="table mb-0 table-borderless">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Patient ID | Name</th>
                                                    <th scope="col">Collector ID | Name</th>
                                                    <th scope="col">Report Issued Date & Time</th>
                                                    <th scope="col">Test Name</th>
                                                    <th scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(!empty($reportIssuedOrders))
                                                    @foreach($reportIssuedOrders as $order)
                                                        @if($order->member_name == null)
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->patient_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords($order->patient_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->patient_contact }} </p>
                                                                            <p class="mb-0">{{ $order->patient_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["report_issued"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#uploadreport"><i class="ri-check-line"></i>Upload Report</button>
                                                                </td>
                                                            </tr>
                                                        @else
                                                            <tr>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->member_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords($order->member_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->member_contact }} </p>
                                                                            <p class="mb-0">{{ $order->member_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="training-block d-flex align-items-center">
                                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                            <img src='{{ asset("assets/pathology/images/user/" . $order->sample_collector_image) }}'  class="img-fluid" alt="icon">
                                                                        </div>
                                                                        <div class="ml-3">
                                                                            <h5 class="">{{ strtoupper($order->sample_collector_unique_code) }} - {{ ucwords($order->sample_collector_name) }} </h5>
                                                                            <p class="mb-0">{{ $order->sample_collector_contact }} </p>
                                                                            <p class="mb-0">{{ $order->sample_collector_email }} </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>{{ date("d/M/Y h:i A", strtotime($order->status_with_time["report_issued"])) }}</td>
                                                                <td>{{ $order->tests }}</td>
                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#uploadreport"><i class="ri-check-line"></i>Upload Report</button>
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <tr><td colspan="6" class="text-mute text-center">No Today Report Issued Orders Yet.</td></tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection