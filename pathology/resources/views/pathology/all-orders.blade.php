@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb iq-bg-primary mb-0">
                                <li class="breadcrumb-item"><a href="{{ route('pathology-dashboard') }}"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">All Orders Received</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Search Orders</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="new-user-info">
                                <form action='{{ route("pathology-all-orders") }}' method="post" id="form-search-orders" name="form-search-orders">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="Country">Status:</label>
                                            @csrf
                                            <select id="search-orders" name="search-orders" class="form-control" required>
                                                <option selected="false" disabled>Select Status</option>
                                                <option {{ old("search-orders") === "all" ? "selected=selected" : "" }} value="all">All</option>
                                                <option {{ (config("constants.ORDER_STATUS.PLACED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.PLACED") }}'>Placed</option>
                                                <option {{ (config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED") }}'>Technician Assigned</option>
                                                <option {{ (config("constants.ORDER_STATUS.SAMPLE COLLECTED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.SAMPLE COLLECTED") }}'>Sample Collected</option>
                                                <option {{ (config("constants.ORDER_STATUS.SAMPLE SUBMITTED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.SAMPLE SUBMITTED") }}'>Sample Submitted</option>
                                                <option {{ (config("constants.ORDER_STATUS.REPORT ISSUED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.REPORT ISSUED") }}'>Report Issued</option>
                                                <option {{ (config("constants.ORDER_STATUS.CANCELLED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.CANCELLED") }}'>Cancelled</option>
                                                <option {{ (config("constants.ORDER_STATUS.REFUNDED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.REFUNDED") }}'>Refunded</option>
                                                <option {{ (config("constants.ORDER_STATUS.RESCHEDULED") == old("search-orders")) ? "selected=selected" : "" }} value='{{ config("constants.ORDER_STATUS.RESCHEDULED") }}'>Rescheduled</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 text-left open-button">
                                            <button type="submit" class="btn btn-primary">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">All Orders Received</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-borderless text-nowrap data-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order ID</th>
                                            <th scope="col">Patient ID | Name</th>
                                            <th scope="col">Placed Date & Time</th>
                                            <th scope="col">Slot Date & Time</th>
                                            <th scope="col">Test Name</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Status</th>
                                            <!-- <th scope="col">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $IMAGE_URL = config("constants.IMAGE_URL");
                                        @endphp
                                        
                                        @if(!empty($allOrders))
                                            @foreach($allOrders as $order)
                                                @if($order->member_name == null)
                                                    <tr>
                                                        <td>{{ strtoupper($order->order_number) }}</td>
                                                        <td>
                                                        <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                    <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                    <p class="mb-0">{{ $order->patient_email }}</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at)) }}</td>
                                                        <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                        <td>{{ $order->tests }}</td>
                                                        <td>₹ {{ $order->payable }}</td>
                                                        <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                        <!-- <td>
                                                            <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                            <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                        </td> -->
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{ strtoupper($order->order_number) }}</td>
                                                        <td>
                                                            <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords( strtolower($order->member_name)) }}</h5>
                                                                    <p class="mb-0">{{ $order->member_contact }}</p>
                                                                    <p class="mb-0">{{ $order->member_email }}</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at) ) }}</td>
                                                        <td>{{ date("d/M/Y", strtotime($order->slot_date)) }} {{ $order->slot_time }}</td>
                                                        <td>{{ $order->tests }}</td>
                                                        <td>₹ {{ $order->payable }}</td>
                                                        <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                        <!-- <td>
                                                            <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                            <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                        </td> -->
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection

@section("modal")
    @parent
    <div class="modal fade bd-example-modal-lg" tabindex="-1" id="assignorder" role="dialog"  aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="rounded text-center">
                        <div class="donter-profile">
                            <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' class="rounded-circle avatar-50 rounded-circle" alt="user-img" id="patient_member_image">
                        </div>
                        <div class="doctor-detail mt-3">
                            <h5 id="patient_member_name"></h5>
                        </div>
                        <div class="doctor-description">
                            <table class="table mb-0">
                                <tbody>
                                    <tr>
                                        <th>Patient ID :</th>
                                        <td id="patient_member_unique_code"></td>
                                    </tr>
                                    <tr>
                                        <th>Placed Date :</th>
                                        <td id="order_created"></td>
                                    </tr>
                                    <tr>
                                        <th>Assign Collector :</th>
                                        <td>
                                            <form action='{{ route("pathology-assign-sample-collector") }}' method="post" id="form-assign-sample-collector">
                                                <div class="form-group col-md-12 mb-0">
                                                    @csrf
                                                    <input type="hidden" name="order_number" id="order_number" />
                                                    <select class="form-control" id="sample_collector_id" name="sample_collector_id">
                                                        <option value="0">Select Collector</option>
                                                        @if($collectors)
                                                            @foreach($collectors as $collector)
                                                                <option value="{{ $collector->id }}">{{ $collector->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="sumbit" form="form-assign-sample-collector" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection