@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->  
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb iq-bg-primary mb-0">
                            <li class="breadcrumb-item"><a href="#"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Patients</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Patients</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="table-responsive">
                            <table class="table mb-0 table-borderless text-nowrap data-table">
                                <thead>
                                    <tr>
                                        <th scope="col">Patient ID | Name</th>
                                        <th scope="col">Created Date & Time</th>
                                        <th scope="col">Members</th>
                                        <th scope="col">Status</th>
                                        <th scope="col" class="text-center">Orders</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $IMAGE_URL = config("constants.IMAGE_URL");
                                    @endphp
                                    
                                    @if(!empty($patients))
                                        @foreach($patients as $patient)
                                            <tr>
                                                <td>
                                                    <div class="training-block d-flex align-items-center">
                                                        <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                            <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $patient->image }}'  class="rounded-circle avatar-50" alt="icon">
                                                        </div>
                                                        <div class="ml-3">
                                                            <h5 class="">{{ strtoupper($patient->unique_code) }} - {{ ucwords($patient->name) }} </h5>
                                                            <p class="mb-0">{{ $patient->contact }} </p>
                                                            <p class="mb-0">{{ $patient->email }} </p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>{{ $patient->created_at }}</td>
                                                <td><a href='{{ route("pathology-patient-members", ["id" => $patient->id]) }}' class="btn btn-primary">{{ $patient->member_count }}</a></td>
                                                <td>{{ ucwords($patient->registration_status) }}</td>
                                                <td class="text-center">
                                                    <a href='{{ route("pathology-patient-orders", ["id" => $patient->id]) }}' class="btn btn-primary">Show Orders</a>
                                                </td> 
                                            </tr>                                        
                                        @endforeach
                                    @else
                                        <tr><td colspan="4" class="text-mute text-center">No Patient Registered Yet.</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content End Here -->
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection