@extends("pathology.layout")
@section("main-content")
    <!-- Main Content Start Here -->    
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-primary rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-primary"><i class="ri-user-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter">{{ $totalOrders }}</span></h2>
                                            <h5 class="">Total Orders</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-warning rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-warning"><i class="ri-women-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter">{{ $totalInProcessing }}</span></h2>
                                            <h5 class="">In Processing</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-danger rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-danger"><i class="ri-group-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0"><span class="counter">{{ $totalCompletedOrders }}</span></h2>
                                            <h5 class="">Complete Orders</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-info rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-info"><i class="ri-hospital-line"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0">₹ <span class="counter"> {{ $totalEarning }}</span></h2>
                                            <h5 class="">Total Earning</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Hospital Management</h4>
                            </div>
                        </div>
                        <div class="iq-card-body hospital-mgt">
                            <div class="progress mb-3" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 20%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">OPD</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 80%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">80%</div>
                            </div>
                            <div class="progress mb-3" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 30%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">Treatment</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 70%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">70%</div>
                            </div>
                            <div class="progress mb-3" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 60%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">Laboratory Test</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 40%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">85%</div>
                            </div>
                            <div class="progress mb-3" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 40%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">New Patient</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 60%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">70%</div>
                            </div>
                            <div class="progress mb-3" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 35%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">Doctors</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 65%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">95%</div>
                            </div>
                            <div class="progress" style="height: 30px;">
                                <div class="progress-bar bg-primary" role="progressbar" style="width: 28%;" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100">Discharge</div>
                                <div class="progress-bar bg-warning" role="progressbar" style="width: 75%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100">35%</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Order Received Details</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 text-nowrap table-borderless data-table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Order ID</th>
                                            <th scope="col">Patient ID | Name</th>
                                            <th scope="col">Placed Date & Time</th>
                                            <th scope="col">Slot Date & Time</th>
                                            <th scope="col">Test Name</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $IMAGE_URL = config("constants.IMAGE_URL");
                                        @endphp
                                        
                                        @if(!empty($placedOrders))
                                            {{!empty($placedOrders)}}
                                            @foreach($placedOrders as $order)
                                                @if($order->member_name == null)
                                                    <tr>
                                                        <td>{{ strtoupper($order->order_number) }}</td>
                                                        <td>
                                                            <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $order->patient_image }}' class="rounded-circle avatar-50" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($order->patient_unique_code) }} - {{ ucwords( strtolower($order->patient_name)) }}</h5>
                                                                    <p class="mb-0">{{ $order->patient_contact }}</p>
                                                                    <p class="mb-0">{{ $order->patient_email }}</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at)) }}</td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->slot_date)) }}</td>
                                                        <td>{{ $order->tests }}</td>
                                                        <td>₹ {{ $order->payable }}</td>
                                                        <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                        <td>
                                                            <button class="btn btn-primary assign-order" data-toggle="modal" data-target="#assignorder" order_number="{{ $order->order_number }}"><i class="ri-check-line"></i>Assign Order</button>
                                                            <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{ strtoupper($order->order_number) }}</td>
                                                        <td>
                                                            <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($order->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/member/images/" . $order->member_image }}' class="img-fluid" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($order->member_unique_code) }} - {{ ucwords( strtolower($order->member_name)) }}</h5>
                                                                    <p class="mb-0">{{ $order->member_contact }}</p>
                                                                    <p class="mb-0">{{ $order->member_email }}</p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->created_at) ) }}</td>
                                                        <td>{{ date("d-M-Y h:i: A", strtotime($order->slot_date)) }}</td>
                                                        <td>{{ $order->tests }}</td>
                                                        <td>₹ {{ $order->payable }}</td>
                                                        <td>{{ ucwords(strtolower($order->order_status)) }}</td>
                                                        <td>
                                                            <button class="btn btn-primary assign-order" order_number="{{ $order->order_number }}" data-toggle="modal" data-target="#assignorder"><i class="ri-check-line"></i>Assign Order</button>
                                                            <button class="btn iq-bg-danger"><i class="ri-close-line"></i>Decline</button>
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Collector Lists</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <ul class="doctors-lists m-0 p-0">
                                @if(count($collectors))
                                    @foreach($collectors as $collector)
                                        <li class="d-flex mb-4 align-items-center">
                                            <div class="user-img img-fluid"><img src='{{ asset("uploads/collector/images/" . $collector->image) }}' alt="story-img" class="rounded-circle avatar-40" /></div>
                                            <div class="media-support-info ml-3">
                                                <h6>{{ ucwords($collector->name) }}</h6>
                                                <p class="mb-0 font-size-12">{{ ucwords($collector->contact) }}</p>
                                            </div>
                                            <!-- <div class="iq-card-header-toolbar d-flex align-items-center">
                                                <div class="dropdown show">
                                                    <span class="dropdown-toggle text-primary" id="dropdownMenuButton41" data-toggle="dropdown" aria-expanded="true" role="button">
                                                        <i class="ri-more-2-line"></i>
                                                    </span>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="#"><i class="ri-eye-line mr-2"></i>View</a>
                                                        <a class="dropdown-item" href="#"><i class="ri-bookmark-line mr-2"></i>Appointment</a>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                            <a href='{{ route("pathology-collectors") }}' class="btn btn-primary d-block mt-3"><i class="ri-add-line"></i> View All Collectors </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Patients In</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div id="bar-chart-6"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection

@section("modal")
    @parent
    <div class="modal fade bd-example-modal-lg" tabindex="-1" id="assignorder" role="dialog"  aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Assign Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="rounded text-center">
                        <div class="donter-profile">
                            <img src='{{ asset("assets/pathology/images/user/06.jpg") }}' class="img-fluid rounded-circle" alt="user-img" id="patient_member_image">
                        </div>
                        <div class="doctor-detail mt-3">
                            <h5 id="patient_member_name"></h5>
                        </div>
                    </div>
                    <div class="doctor-description mt-3">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <th>Patient ID :</th>
                                    <td id="patient_member_unique_code"></td>
                                </tr>
                                <tr>
                                    <th>Placed Date :</th>
                                    <td id="order_created"></td>
                                </tr>
                                <tr>
                                    <th>Assign Collector :</th>
                                    <td>
                                        <form action='{{ route("pathology-assign-sample-collector") }}' method="post" id="form-assign-sample-collector">
                                            <div class="form-group pl-0 col-md-12 mb-0">
                                                @csrf
                                                <input type="hidden" name="order_number" id="order_number" />
                                                <select class="form-control select2" id="sample_collector_id" name="sample_collector_id">
                                                    <option value="0">Select Collector</option>
                                                    @if($collectors)
                                                        @foreach($collectors as $collector)
                                                            <option value="{{ $collector->id }}">{{ $collector->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="sumbit" form="form-assign-sample-collector" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
    
        $('.select2').select2();
    
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection