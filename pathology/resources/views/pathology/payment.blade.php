@extends("pathology.layout")
@section("main-content")
   <!-- Main Content Start Here -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="iq-card">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb iq-bg-primary mb-0">
                                <li class="breadcrumb-item"><a href="#"><i class="ri-home-4-line mr-1 float-left"></i>Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Payment Management</li>
                            </ol>
                        </nav>
                    </div>
                </div>
                
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-primary rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-primary"><i class="ri-user-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0">₹ <span class="counter">{{ $totalEarning }}</span></h2>
                                            <h5 class="">Total Earning</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-warning rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-warning"><i class="ri-women-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0">₹ <span class="counter">{{ $todayEarning }}</span></h2>
                                            <h5 class="">Today Earning</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-danger rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-danger"><i class="ri-group-fill"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0">₹ <span class="counter">{{ $lastWeekEarning }}</span></h2>
                                            <h5 class="">Last Week</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                                <div class="iq-card-body iq-bg-info rounded">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div class="rounded-circle iq-card-icon bg-info"><i class="ri-hospital-line"></i></div>
                                        <div class="text-right">
                                            <h2 class="mb-0">₹ <span class="counter">{{ $lastMonthEarning }}</span></h2>
                                            <h5 class="">Last Month</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="iq-card iq-card-block iq-card-stretch iq-card-height">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Payment Management</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-borderless data-table">
                                    <thead>
                                        <tr>
                                            <!-- <th scope="col">Invoice No</th> -->
                                            <th scope="col">Order No</th>
                                            <th scope="col">Patient ID | Name</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Test</th>
                                            <th scope="col">Payment Date</th>
                                            <th scope="col">Payment Mode</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $IMAGE_URL = config("constants.IMAGE_URL");
                                        @endphp

                                        @if(!empty($payments))
                                            @foreach($payments as $payment)
                                                @if($payment->member_name == null)
                                                    <tr>
                                                        <td>{{ strtoupper($payment->order_number) }}</td>
                                                        <td>
                                                            <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($payment->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/patient/images/" . $payment->patient_image }}'  class="img-fluid" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($payment->patient_unique_code) }} - {{ ucwords($payment->patient_name) }} </h5>
                                                                    <p class="mb-0">{{ $payment->patient_contact }}</p>
                                                                    <p class="mb-0">{{ $payment->patient_email }} </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>₹ {{ $payment->payable }}</td>
                                                        <td>{{ $payment->tests }}</td>
                                                        <td>{{ date("d/M/Y h:i A", strtotime($payment->created_at)) }}</td>
                                                        <td>{{ ucwords($payment->payment_mode) }}</td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>{{ strtoupper($payment->order_number) }}</td>
                                                        <td>
                                                            <div class="training-block d-flex align-items-center patient-member-details" data-toggle="modal" data-target="#patientdetails" order_number="{{ strtoupper($payment->order_number) }}">
                                                                <div class="rounded-circle iq-card-icon iq-bg-primary">
                                                                    <img src='{{ $IMAGE_URL . "uploads/member/images/" . $payment->member_image }}'  class="img-fluid" alt="icon">
                                                                </div>
                                                                <div class="ml-3">
                                                                    <h5 class="">{{ strtoupper($payment->member_unique_code) }} - {{ ucwords($payment->member_name) }} </h5>
                                                                    <p class="mb-0">{{ $payment->member_contact }}</p>
                                                                    <p class="mb-0">{{ $payment->member_email }} </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>₹ {{ $payment->payable }}</td>
                                                        <td>{{ $payment->tests }}</td>
                                                        <td>{{ date("d/M/Y h:i A", strtotime($payment->created_at)) }}</td>
                                                        <td>{{ ucwords($payment->payment_mode) }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- Main Content End Here -->
@endsection

@section("javascript")
    @parent
    <script type="text/javascript">
        $(document).ready(function(e){
            // Applying Data Table.
            $(".data-table").DataTable({
                "ordering" : false,
            });
        });
    </script>
@endsection