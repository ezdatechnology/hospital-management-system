<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title><?php echo !empty($title) ? $title : "Pathology Login Panel" ?></title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('assets/pathology/images/favicon.ico') }}" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/bootstrap.min.css') }}"/>
        <!-- Typography CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/typography.css') }}" />
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/responsive.css') }}" />
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/style.css') }}" />
        <!-- Toastr -->
        <link rel="stylesheet" href="{{ asset('assets/pathology/css/toastr.css') }}" />
        <!-- Full calendar -->
        <link href="{{ asset('assets/pathology/fullcalendar/core/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/daygrid/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/timegrid/main.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/pathology/fullcalendar/list/main.css') }}" rel="stylesheet" />

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" />
    </head>
    <body>
        <!-- loader Start -->
        <div id="loading">
            <div id="loading-center"></div>
        </div>
        <!-- loader END -->

        <!-- Sign In Start -->
            <section class="sign-in-page">
                <div class="container sign-in-page-bg mt-5 p-0">
                    <div class="row no-gutters">
                        <div class="col-md-6 text-center">
                            <div class="sign-in-detail text-white">
                                    <!-- <a class="sign-in-logo mb-5" href="#"><img src="{{ asset('assets/pathology/images/demo2.png') }}" class="img-fluid" alt="logo" /></a> -->
                                    <h1 class="text-white mb-5">Logo Here</h1>
                                    <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="0"
                                    >
                                        <div class="item">
                                            <img src="{{ asset('assets/pathology/images/login/1.png') }}" class="img-fluid mb-4" alt="logo" />
                                            <h4 class="mb-1 text-white">Manage your orders</h4>
                                            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('assets/pathology/images/login/2.png') }}" class="img-fluid mb-4" alt="logo" />
                                            <h4 class="mb-1 text-white">Manage your orders</h4>
                                            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        </div>
                                        <div class="item">
                                            <img src="{{ asset('assets/pathology/images/login/3.png') }}" class="img-fluid mb-4" alt="logo" />
                                            <h4 class="mb-1 text-white">Manage your orders</h4>
                                            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 position-relative">
                                <div class="sign-in-from">
                                    <h1 class="mb-0">Login Access</h1>
                                    <p>You must become a member to login and access the entire site.</p>
                                    <form class="mt-4" action="{{ route('login') }}" method="post">
                                        <div class="form-group">
                                            @csrf
                                            <label for="exampleInputEmail1">Email Address</label>
                                            <input type="email" name="email" class="form-control mb-0" id="exampleInputEmail1" placeholder="Enter email" value="{{ old('email') }}" />
                                            @if($errors->login->first("email"))
                                                <i class="text-danger">{{ $errors->login->first("email") }}</i>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <a href="#" class="float-right">Forgot password?</a>
                                            <input type="password" name="password" class="form-control mb-0" id="exampleInputPassword1" placeholder="Password" />
                                            @if($errors->login->first("password"))
                                                <i class="text-danger">{{ $errors->login->first("password") }}</i>
                                            @endif
                                        </div>
                                        <div class="d-inline-block w-100">
                                            <!--<div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1" />
                                                <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                            </div>-->
                                            <button type="submit" class="btn btn-primary float-right">Sign in</button>
                                        </div>
                                        <!-- <div class="sign-info">
                                            <span class="dark-color d-inline-block line-height-2">Don't have an account? <a href="#">Sign up</a></span>
                                            <ul class="iq-social-media">
                                                <li>
                                                    <a href="#"><i class="ri-facebook-box-line"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="ri-twitter-line"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="ri-instagram-line"></i></a>
                                                </li>
                                            </ul>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        <!-- Sign In End -->

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="{{ asset('assets/pathology/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/pathology/js/popper.min.js') }}"></script>
        <script src="{{ asset('assets/pathology/js/bootstrap.min.js') }}"></script>
        <!-- Appear JavaScript -->
        <script src="{{ asset('assets/pathology/js/jquery.appear.js') }}"></script>
        <!-- Countdown JavaScript -->
        <script src="{{ asset('assets/pathology/js/countdown.min.js') }}"></script>
        <!-- Counterup JavaScript -->
        <script src="{{ asset('assets/pathology/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('assets/pathology/js/jquery.counterup.min.js') }}"></script>
        <!-- Wow JavaScript -->
        <script src="{{ asset('assets/pathology/js/wow.min.js') }}"></script>
        <!-- Apexcharts JavaScript -->
        <script src="{{ asset('assets/pathology/js/apexcharts.js') }}"></script>
        <!-- Slick JavaScript -->
        <script src="{{ asset('assets/pathology/js/slick.min.js') }}"></script>
        <!-- Select2 JavaScript -->
        <script src="{{ asset('assets/pathology/js/select2.min.js') }}"></script>
        <!-- Owl Carousel JavaScript -->
        <script src="{{ asset('assets/pathology/js/owl.carousel.min.js') }}"></script>
        <!-- Magnific Popup JavaScript -->
        <script src="{{ asset('assets/pathology/js/jquery.magnific-popup.min.js') }}"></script>
        <!-- Smooth Scrollbar JavaScript -->
        <script src="{{ asset('assets/pathology/js/smooth-scrollbar.js') }}"></script>
        <!-- lottie JavaScript -->
        <script src="{{ asset('assets/pathology/js/lottie.js') }}"></script>
        <!-- am core JavaScript -->
        <script src="{{ asset('assets/pathology/js/core.js') }}"></script>
        <!-- am charts JavaScript -->
        <script src="{{ asset('assets/pathology/js/charts.js') }}"></script>
        <!-- am animated JavaScript -->
        <script src="{{ asset('assets/pathology/js/animated.js') }}"></script>
        <!-- am kelly JavaScript -->
        <script src="{{ asset('assets/pathology/js/kelly.js') }}"></script>
        <!-- Flatpicker Js -->
        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <!-- Chart Custom JavaScript -->
        <script src="{{ asset('assets/pathology/js/chart-custom.js') }}"></script>
        <!-- Custom JavaScript -->
        <script src="{{ asset('assets/pathology/js/custom.js') }}"></script>
       <!-- Toastr -->
       <script src="{{ asset('assets/pathology/js/toastr.min.js') }}"></script>
             
        <script type="text/javascript">
            $('.select2').select2();
            
            $(document).ready(function(){
                // Applying Toastr On Success Message.
                if('{{ session()->has("status") }}' && '{{ session()->get("status") }}' == "success"){
                    toastr.success('{{ session()->get("message") }}');
                }

                // Applying Toastr On Fail Message.
                if('{{ session()->has("status") }}' && '{{ session()->get("status") }}' == "danger"){
                    toastr.error('{{ session()->get("message") }}');
                }
            });
        </script>
    </body>
</html>