<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    // This attributes will update the parent model updated_at while child model is updated.
    protected $touches = ["patient"];

    // Defining One To Many Inverse Relationship.
    public function patient(){
        return $this->belongsTo(Patient::class);
    }
    
    // Serializing Data For Retrieving In Asia/Kolkata Format.
    // protected function serializeDate(\DateTimeInterface $date){
    //     return $date->format('Y-m-d H:i:s');
    // }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
