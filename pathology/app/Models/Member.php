<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Patient;

class Member extends Model
{
    use HasFactory;

    // This attributes will update the parent model updated_at while child model is updated.
    protected $touches = ["patient"];

    // The attributes that are mass assignable.     
    protected $fillable = [
        'name',
        'email',
        'contact',
        'gender',
        'dob',
        'relation',
    ];

    // The attributes that are guarded.     
    protected $guarded = [
        'session_id',
        "unique_code",
        'status',
    ];

    // Defining One To Many Inverse Relationship.
    public function patient(){
        return $this->belongsTo(Patient::class);
    }

    // Converting PHP Date To MYSQL Date
    public function setDobAttribute($value){
        $this->attributes["dob"] = date('Y-m-d', strtotime($value));
    }

    // Converting MYSQL Date Into Human Readable Date.
    public function getDobAttribute($value){
        return date('d/M/Y', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getCreatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }

    // Converting MYSQL Created At And Updated At Date Into Human Readable Date.
    public function getUpdatedAtAttribute($value){
        return date('d/M/Y h:i A', strtotime($value));
    }
}
