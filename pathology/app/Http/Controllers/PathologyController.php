<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Address;
use App\Models\Member;
use App\Models\Patient;
use App\Models\PatientPayment;
use App\Models\SampleCollector;
use Carbon\Carbon;

class PathologyController extends Controller{
    #==============================================#
    #------------- DASHBOARD FUNCTION -------------#
    #==============================================#
    
    public function index(Request $request){
		// Counting Of Total Orders.
		$data["totalOrders"] = PatientPayment::select(DB::raw("count(*) as total_orders"))
												->first()
												->total_orders;
			
		// Counting Of Total In Processing Orders.
		$data["totalInProcessing"] = PatientPayment::select(DB::raw("count(*) as in_processing_orders"))
													->where("order_status", config("constants.ORDER_STATUS.PLACED"))
													->orWhere("order_status", config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED"))
													->orWhere("order_status", config("constants.ORDER_STATUS.SAMPLE COLLECTED"))
													->orWhere("order_status", config("constants.ORDER_STATUS.SAMPLE SUBMITTED"))
													->orWhere("order_status", config("constants.ORDER_STATUS.RESCHEDULED"))
													->first()
													->in_processing_orders;
		
		// Counting Of Total Completed Orders.
		$data["totalCompletedOrders"] = PatientPayment::select(DB::raw("count(*) as total_report_issued"))
														->where("order_status", config("constants.ORDER_STATUS.REPORT ISSUED"))
														->orWhere("order_status", config("constants.ORDER_STATUS.CANCELLED"))
														->orWhere("order_status", config("constants.ORDER_STATUS.REFUNDED"))
														->first()
														->total_report_issued;
			
		// Total Earnings.
		$data["totalEarning"] = PatientPayment::select(DB::raw("sum(payable) as total_earning"))
												->where("order_status", config("constants.ORDER_STATUS.REPORT ISSUED"))
												->first()
												->total_earning;

		// Retrieving Received Order For Today Slots With Patient Details With Their Members Details.
        $data["placedOrders"] = DB::table("patient_payments as pp")
                    				->select("pp.*", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email",
											"p.unique_code as patient_unique_code", "p.image as patient_image", 
											"m.name as member_name", "m.contact as member_contact", "m.email as member_email",
											"m.unique_code as member_unique_code", "m.image as member_image",
											"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
											"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date")
									->whereDate("s.slot_date", Carbon::now())
									->where([
										"pp.order_status" => config("constants.ORDER_STATUS.PLACED"),
										"pp.sample_collector_id" => 0
									])
									->join("patients as p", "p.id", "=", "pp.patient_id")
									->leftJoin("members as m", "m.id", "=", "pp.member_id")
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
									->latest("pp.created_at")
									->get()
									->toArray();
		
		// Retrieving Test Details For Each Order.
		foreach($data["placedOrders"] as $key => $order){
			// Retrieving Tests Details.
            $data["placedOrders"][$key]->tests = DB::table("patient_tests as pt")
                                                    ->select("t.name as test")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into Comma Separated String.
			$allTests = [];

			foreach($data["placedOrders"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["placedOrders"][$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
			
			// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
			$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
        }

		// Page Name
		$data["title"] = "Pathology Dashboard Panel";

		// Retrieving All Active Collectors.
        $data["collectors"] = SampleCollector::where(["status" => "active"])->get();
		
		// dd($data);

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Dashboard";

        return view("pathology/dashboard", $data);
    }
	
	#==================================================#
	#--------------- COLLECTOR FUNCTION ---------------#
	#==================================================#

	public function collectors(){
        // Retrieving All Collectors.
        $data["collectors"] = DB::table("sample_collectors as sc")
									->select("sc.*", "c.name as country", "s.name as state", "ct.name as city")
									->join("countries as c", "c.id", "=", "sc.country")
									->join("states as s", "s.id", "=", "sc.state")
									->join("cities as ct", "ct.id", "=", "sc.city")
									->latest("sc.created_at")
									->get();

		// Page Name
		$data["title"] = "Pathology Collectors Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Collector";
		
		// dd($data);
		
		// View Pages Load
		return view ("pathology/collectors", $data);
	}

	public function addCollector(Request $request){
		// If Request Method Is Post.
		if ($request->isMethod("post")){
			// Current Date So That Valid DOB Validation Could Be Made.
			$now = date("d-m-Y", strtotime(now()));

			// Validation Rules.
			$rules = [
				"name" => "required",
				"contact" => "required|min:10|unique:sample_collectors",
				"email" => "required|email",
				"address" => "required",
				"country" => 'required|not_in:Select Country',
				"state" => 'required|not_in:Select State',
				"city" => 'required|not_in:Select City',
				"pincode" => "required|digits:6",				
				"dob" => "required|date|before_or_equal:$now",
				"gender" => "required",
				"vehicle_no" => "required",
			];

			// Validaton Custom Messages.
			$messages = [
				"name.required" => "Name Required",
				"contact.required" => "Contact Required",
				"contact.min" => "Invalid Contact",
				"contact.unique" => "Already A Registered Contact.",
				"email.required" => "Email Address Required",
				"email.email" => "Invalid Email Address",
				"address.required" => "Address Required",
				"country.required" => "Country Required",
				"state.required" => "State Required",
				"city.required" => "City Required",
				"pincode.required" => "Pincode Required",
				"pincode.digits" => "Invalid Pincode",
				"dob.required" => "DOB Required",
				"dob.date" => "Invalid Date",
				"dob.before_or_equal" => "Invalid Future Date",
				"gender.required" => "Gender Required",
				"vehicle_no.required" => "Vehicle No Required",
			];

			// Input Validation.
			$validator = Validator::make($request->all(), $rules, $messages);

			// On Validation Fail.
			if ($validator->fails()) {
				// Redirecting On Form.
				return redirect()
					->route("pathology-add-collector")
					->withErrors($validator, "addcollector")
					->withInput();
			}

			// On Uploading Collector Image.
			if ($request->hasFile("collector_image")) {
				// Retrieving Original File Name Of Uploaded File.
				$fileNameWithExtension = $request->file("collector_image")->getClientOriginalName();

				// Retrieving Extension Of Uploaded File.
				$fileExtension = $request->file("collector_image")->extension();

				// Moving Uploaded File On The Collector Image Folder.
				$fileNameToStore = $request->file("collector_image")->move(public_path("/uploads/collector/images"), $fileNameWithExtension);
			}

			// On Not Uploading Collector Image.
			else {
				$fileNameWithExtension = "01.jpg";
			}

			// On Validation Success. Creating New Sample Collector Model.
			$newSampleCollector = new SampleCollector();

			// Setting Request Values With Model's DB Columns.
			$newSampleCollector->name = trim($request->input("name"));
			$newSampleCollector->contact = trim($request->input("contact"));
			$newSampleCollector->email = trim($request->input("email"));
			$newSampleCollector->country = trim($request->input('country'));
			$newSampleCollector->state = trim($request->input('state'));
			$newSampleCollector->city = trim($request->input('city'));
			$newSampleCollector->address = trim($request->input("address"));
			$newSampleCollector->dob = trim($request->input("dob"));
			$newSampleCollector->pincode = trim($request->input("pincode"));
			$newSampleCollector->vehicle_no = trim($request->input("vehicle_no"));
			$newSampleCollector->gender = trim($request->input("gender"));
			$newSampleCollector->image = $fileNameWithExtension;
			$newSampleCollector->unique_code = bin2hex(random_bytes(3));

			// Saving Model
			$saveSampleCollector = $newSampleCollector->save();

			// On Saving Fail.
			if (!$saveSampleCollector) {
				$request->session()->flash("status", "danger");
				$request->session()->flash("message", "New Collector Details Not Saved. Try Again...");
			} else {
				$request->session()->flash("status", "success");
				$request->session()->flash("message", "New Collector Details Saved Successfully.");
			}

			// Redirecting On Form.
			return redirect()
				->route("pathology-add-collector")
				->withInput();
		}

		// Retrieving Countries.
		$data['countries'] = DB::table('countries')->orderBy('name', 'asc')->get();
		
		// Retrieving States.
		$data['states'] = DB::table('states')->orderBy('name', 'asc')->get();
		
		// Retrieving Cities.
		$data['cities'] = DB::table('cities')->orderBy('name', 'asc')->get();

		// Page Name
		$data["title"] = "Pathology Add Collector Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Collector";

		// View Pages Load
		return view("pathology/collector-add", $data);
	}

	public function editCollector(Request $request, $id){
		// If Request Method Is Post.
		if ($request->isMethod("post")) {
			// Current Date So That Valid DOB Validation Could Be Made.
			$now = date("d-m-Y", strtotime(now()));

			// Validation Rules.
			$rules = [
				"name" => "required",
				"contact" => "required|min:10|unique:sample_collectors,contact," . $id,
				"email" => "required|email",
				"address" => "required",
				"country" => 'required|not_in:Select Country',
				"state" => 'required|not_in:Select State',
				"city" => 'required|not_in:Select City',
				"pincode" => "required|digits:6",				
				"dob" => "required|date|before_or_equal:$now",
				"gender" => "required",
				"vehicle_no" => "required",
			];

			// Validaton Custom Messages.
			$messages = [
				"name.required" => "Name Required",
				"contact.required" => "Contact Required",
				"contact.min" => "Invalid Contact",
				"contact.unique" => "Already A Registered Contact.",
				"email.required" => "Email Address Required",
				"email.email" => "Invalid Email Address",
				"address.required" => "Address Required",
				"country.required" => "Country Required",
				"state.required" => "State Required",
				"city.required" => "City Required",
				"pincode.required" => "Pincode Required",
				"pincode.digits" => "Invalid Pincode",
				"dob.required" => "DOB Required",
				"dob.date" => "Invalid Date",
				"dob.before_or_equal" => "Invalid Future Date",
				"gender.required" => "Gender Required",
				"vehicle_no.required" => "Vehicle No Required",
			];

			// Input Validation.
			$validator = Validator::make($request->all(), $rules, $messages);

			// On Validation Fail.
			if ($validator->fails()) {
				// Redirecting On Form.
				return redirect()
					->route("pathology-edit-collector", ["id" => $id])
					->withErrors($validator, "editcollector")
					->withInput();
			}

			// On Validation Success. Creating New Sample Collector Model.
			$editSampleCollector = SampleCollector::find($id);

			// On Uploading Collector Image.
			if ($request->hasFile("collector_image")) {
				// Retrieving Original File Name Of Uploaded File.
				$fileNameWithExtension = $request->file("collector_image")->getClientOriginalName();

				// Retrieving Extension Of Uploaded File.
				$fileExtension = $request->file("collector_image")->extension();

				// Moving Uploaded File On The Collector Image Folder.
				$fileNameToStore = $request->file("collector_image")->move(public_path("/uploads/collector/images"), $fileNameWithExtension);

				// Setting Request Values With Model's DB Columns.
				$editSampleCollector->image = $fileNameWithExtension;
			}

			// Setting Request Values With Model's DB Columns.
			$editSampleCollector->name = trim($request->input("name"));
			$editSampleCollector->contact = trim($request->input("contact"));
			$editSampleCollector->email = trim($request->input("email"));
			$editSampleCollector->country = trim($request->input('country'));
			$editSampleCollector->state = trim($request->input('state'));
			$editSampleCollector->city = trim($request->input('city'));
			$editSampleCollector->address = trim($request->input("address"));
			$editSampleCollector->dob = trim($request->input("dob"));
			$editSampleCollector->gender = trim($request->input("gender"));
			$editSampleCollector->status = trim($request->input("status"));

			// Saving Model
			$updateSampleCollector = $editSampleCollector->save();

			// On Saving Fail.
			if (!$updateSampleCollector) {
				$request->session()->flash("status", "danger");
				$request->session()->flash("message", "Collector Details Not Updated. Try Again...");
			} else {
				$request->session()->flash("status", "success");
				$request->session()->flash("message", "Collector Details Updated Successfully.");
			}

			// Redirecting On Form.
			return redirect()
				->route("pathology-edit-collector", ["id" => $id])
				->withInput();
		}

		// Retrieving Collector Details.
		$data["collector"] = SampleCollector::find($id);
		
		// Passing country, city, state data
		$data['countries'] = DB::table('countries')->orderBy('name', 'asc')->get();
		$data['states'] = DB::table('states')->where('country_id', $data['collector']->country)->orderBy('name', 'asc')->get();
		$data['cities'] = DB::table('cities')->where('state_id', $data['collector']->state)->orderBy('name', 'asc')->get();

		// On No Collector Details Found.
		if (!$data["collector"]) {
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "No Such Collector Details Found. Try Again...");

			// Redirecting On Collectors Page.
			return redirect()->route("pathology-collectors");
		}

		// Page Name
		$data["title"] = "Pathology Edit Collector Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Collector";

		// dd($data);

		// View Pages Load
		return view("pathology/collector-edit", $data);
	}
	
	public function getStates(Request $request){
		// Retrieving 
		$cid = $request->post('cid');

		$states = DB::table('states')->where('country_id', $cid)->orderBy('name', 'asc')->get();
		
		return response()->json([
			"status" => "success",
			"states" => $states,
			"cid"=>$cid
		]);
	}

	public function getCities(Request $request){
		$sid = $request->post('sid');
		$cities = DB::table('cities')->where('state_id', $sid)->orderBy('name', 'asc')->get();
		return response()->json([
			"status" => "success",
			"cities" => $cities
		]);
	}

	public function getCollectorById(Request $request){
		if($request->isMethod("post")){
			$data["collector"] = DB::table("sample_collectors as sc")
									->select("sc.*", "c.name as country", "s.name as state", "ci.name as city")
									->join("countries as c", "c.id", "=", "sc.country")
									->join("states as s", "s.id", "=", "sc.state")
									->join("cities as ci", "ci.id", "=", "sc.city")
									->where(["sc.id" => trim($request->input("collector_id"))])
									->get()
									->first();

			// Converting PHP MySql Date To Human Readable Date.
			$data["collector"]->created_at = date("d/M/Y h:i A", strtotime($data["collector"]->created_at));
			
			// Converting Status In ucwords Formate.
			$data["collector"]->status = ucwords($data["collector"]->status);
			
			return response()->json([
				"status"=>"success",
				"data"=>$data
			]);
		}
	}
	
	#==================================================#
	#---------------- PATIENT FUNCTION ----------------#
	#==================================================#

	public function patient(){
		// Retrieving Patient Details.
		$data["patients"] = Patient::latest("created_at")->get();

		// Retrieving Member Count According Their Patient ID.
		foreach($data["patients"] as $key => $patient){
			$data["patients"][$key]["member_count"] = DB::table("members")
															->select(DB::raw("count(*) as member_count"))
															->where(["patient_id" => $patient->id])
															->first()
															->member_count;
		}

		// dd($data["patients"]);

		// Page Name
		$data["title"] = "Pathology Patient Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Patient";

		// View Pages Load
		return view ("pathology/patient", $data);
	}

	public function patientMembers(Request $request, $id){
		// Retrieving Patient Details.
		$data["members"] = Member::where(["patient_id" => $id])
								->latest("created_at")
								->get();
		
		// On No Members Found.
		if(!count($data["members"])){
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "No Member Details Found. Try Again...");

			// Returning Patient Page.
			return redirect()->route("pathology-patient");
		}

		// Page Name
		$data["title"] = "Pathology Patient Members Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Patient";

		// View Pages Load
		return view ("pathology/patient-member", $data);
	}

	public function patientOrders($patientId){
		// Retrieving Patient Details.
		$data["patientOrders"] = DB::table("patient_payments as pp")
                    				->select("pp.*", "p.id as patient_id", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email",
											"p.unique_code as patient_unique_code", "p.image as patient_image",
											"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
											"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date", "s.slot_time")
									->where([
										"pp.patient_id" => $patientId,
										"pp.member_id" => 0
									])
									->join("patients as p", "p.id", "=", "pp.patient_id")
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
									->latest("pp.created_at")
									->get()
									->toArray();
		
		// Retrieving Test Details For Each Order.
		foreach($data["patientOrders"] as $key => $order){
			// Retrieving Tests Details.
            $data["patientOrders"][$key]->tests = DB::table("patient_tests as pt")
                                                    ->select("t.name as test")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into Comma Separated String.
			$allTests = [];

			foreach($data["patientOrders"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["patientOrders"][$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
			
			// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
			$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
		
			// Retrieving Slot Time According To SlotTime Value From The CONSTANTS SLOT_TIMINGS Array.
			$order->slot_time = config("constants.SLOT_TIMINGS.$order->slot_time");
        }
		
		// Page Name
		$data["title"] = "Pathology Patient Orders Panel";
		
		// Activating The Current Selected Left Sidebar
		$data["active"] = "Patient";
		
		// dd($data);
		
		// View Pages Load
		return view ("pathology/show-patient-orders", $data);
	}

	public function patientMemberOrders($patientId, $memberId){
		// Retrieving Patient Details.
		$data["patientMemberOrders"] = DB::table("patient_payments as pp")
                    				->select("pp.*", "m.id as member_id", "m.patient_id as patient_id", "m.name as member_name", "m.contact as member_contact",
											"m.email as member_email", "m.unique_code as member_unique_code", "m.image as member_image",
											"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
											"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date", "s.slot_time")
									->where([
										"pp.patient_id" => $patientId,
										"pp.member_id" => $memberId
									])
									// ->join("patients as p", "p.id", "=", "pp.patient_id")
									->join("members as m", "m.id", "=", "pp.member_id")
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
									->latest("pp.created_at")
									->get()
									->toArray();
		
		// Retrieving Test Details For Each Order.
		foreach($data["patientMemberOrders"] as $key => $order){
			// Retrieving Tests Details.
            $data["patientMemberOrders"][$key]->tests = DB::table("patient_tests as pt")
                                                    ->select("t.name as test")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into Comma Separated String.
			$allTests = [];

			foreach($data["patientMemberOrders"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["patientMemberOrders"][$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
			
			// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
			$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
		
			// Retrieving Slot Time According To SlotTime Value From The CONSTANTS SLOT_TIMINGS Array.
			$order->slot_time = config("constants.SLOT_TIMINGS.$order->slot_time");
        }
		
		// Page Name
		$data["title"] = "Pathology Patient Member Orders Panel";
		
		// Activating The Current Selected Left Sidebar
		$data["active"] = "Patient";
		
		// dd($data);
		
		// View Pages Load
		return view ("pathology/show-patient-member-orders", $data);
	}

	public function getPatientMemberById(Request $request){
		if(!$request->isMethod("post")){
			// Creating Failed Response Data.
			$response = [
				"status" => "fail",
				"message" => "Inavalid Request Method.",
				"data" => null
			];
	
			// Returning View Page Response
			return response()->json($response);
		}

		// Retrieving Order Details.
		$data["order"] = PatientPayment::firstWhere(["order_number" => trim($request->input("order_number"))]);

		// On No Order Found.
		if(!$data["order"]){
			// Creating Success Response Data.
			$response = [
				"status" => "fail",
				"message" => "No Order Found.",
				"data" => null
			];

			// Returning View Page Response
			return response()->json($response);
		}

		// Retrieving Patient Details.
		$data["patient"] = Patient::find($data["order"]->patient_id);

		// Retrieving Member Details
		$data["member"] = Member::find($data["order"]->member_id);

		// Retrieving Address Details
		$data["address"] = Address::find($data["order"]->address_id);

		// Creating Success Response Data.
		$response = [
			"status" => "success",
			"message" => "Data Found.",
			"data" => $data
		];

		// Returning View Page Response
        return response()->json($response);
    }
	
	#==================================================#
	#----------------- ORDER FUNCTION -----------------#
	#==================================================#

	public function orderReceived(Request $request){
		// Counting Of Today's Total Placed Orders.
		$data["totalPlacedOrders"] = DB::table("patient_payments as pp")
												->select(DB::raw("count(*) as total_placed"))
												->whereDate("s.slot_date", Carbon::now())
												->join("slots as s", "s.order_number", "=", "pp.order_number")
												->first()
												->total_placed;
			
		// Counting Of Today's Total In Processing Orders.
		$data["totalInProcessingOrders"] = DB::table("patient_payments as pp")
												->select(DB::raw("count(*) as total_in_processing"))
												->whereDate("s.slot_date", Carbon::now())
												->where(function($query){
													$query->where("pp.order_status", config("constants.ORDER_STATUS.PLACED"))
													->orWhere("pp.order_status", config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED"))
													->orWhere("pp.order_status", config("constants.ORDER_STATUS.SAMPLE COLLECTED"))
													->orWhere("pp.order_status", config("constants.ORDER_STATUS.RESCHEDULED"));
												})
												->join("slots as s", "s.order_number", "=", "pp.order_number")
												->first()
												->total_in_processing;
												
												// Counting Of Today's Total In Lab Orders.
												$data["totalInLabOrders"] = DB::table("patient_payments as pp")
												->select(DB::raw("count(*) as total_in_lab"))
												->whereDate("s.slot_date", Carbon::now())
												->where("pp.order_status", config("constants.ORDER_STATUS.SAMPLE SUBMITTED"))
												->join("slots as s", "s.order_number", "=", "pp.order_number")
												->first()
												->total_in_lab;
												
												// Counting Of Today's Total Completed Orders.
												$data["totalCompletedOrders"] = DB::table("patient_payments as pp")
												->select(DB::raw("count(*) as total_completed"))
												->whereDate("s.slot_date", Carbon::now())
												->where(function($query){
													$query->where("pp.order_status", config("constants.ORDER_STATUS.REPORT ISSUED"))
														->orWhere("pp.order_status", config("constants.ORDER_STATUS.CANCELLED"))
														->orWhere("pp.order_status", config("constants.ORDER_STATUS.REFUNDED"));
												})
												->join("slots as s", "s.order_number", "=", "pp.order_number")
												->first()
												->total_completed;

		// Creating Default Empty Array For Placed Orders.
		$data["placedOrders"] = [];
		
		// Creating Default Empty Array For Collector Assigned Orders.
		$data["collectorAssignedOrders"] = [];
		
		// Creating Default Empty Array For Sample Collected Orders.
		$data["sampleCollectedOrders"] = [];
		
		// Creating Default Empty Array For Sample Submitted Orders.
		$data["sampleSubmittedOrders"] = [];
		
		// Creating Default Empty Array For Report Issued Orders.
		$data["reportIssuedOrders"] = [];
		
		// Creating Default Empty Array For Rescheduled Orders.
		$data["rescheduledOrders"] = [];
		
		// Creating Default Empty Array For Cancelled Orders.
		$data["cancelledOrders"] = [];

		// Retrieving Received Order For Today Slots With Patient Details With Their Members Details.
        $todayOrders = DB::table("patient_payments as pp")
                    ->select("pp.*", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email",
							"p.unique_code as patient_unique_code", "p.image as patient_image", 
							"m.name as member_name", "m.contact as member_contact", "m.email as member_email",
							"m.unique_code as member_unique_code", "m.image as member_image", "sc.id as sample_collector_id",
							"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
							"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date", "s.slot_time")
                    ->whereDate("s.slot_date", Carbon::now())
                    ->join("patients as p", "p.id", "=", "pp.patient_id")
                    ->leftJoin("members as m", "m.id", "=", "pp.member_id")
                    ->join("slots as s", "s.order_number", "=", "pp.order_number")
                    ->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
					->latest("pp.created_at")
					->get();
		
		// Retrieving Tests Details For Each Order And Storing Each Order In Their Category.
		foreach($todayOrders as $key => $order){
			// Retrieving Tests Details.
            $todayOrders[$key]->tests = DB::table("patient_tests as pt")
                                                    ->select("t.name as test")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into String.
			$allTests = [];

			foreach($todayOrders[$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$todayOrders[$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
		
			// Retrieving Slot Time According To SlotTime Value From The CONSTANTS SLOT_TIMINGS Array.
			$order->slot_time = config("constants.SLOT_TIMINGS.$order->slot_time");
			
			// Separting Each Order With Their Category.
			if($order->order_status === config("constants.ORDER_STATUS.PLACED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["placedOrders"][] = $order;
			} else if($order->order_status === config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["collectorAssignedOrders"][] = $order;
			} else if($order->order_status === config("constants.ORDER_STATUS.SAMPLE COLLECTED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["sampleCollectedOrders"][] = $order;
			} else if($order->order_status === config("constants.ORDER_STATUS.SAMPLE SUBMITTED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["sampleSubmittedOrders"][] = $order;
			} else if($order->order_status === config("constants.ORDER_STATUS.REPORT ISSUED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["reportIssuedOrders"][] = $order;
			} else if($order->order_status === config("constants.ORDER_STATUS.CANCELLED")){
				// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
				$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));

				$data["cancelledOrders"][] = $order;
			}
        }

		// Retrieving All Today's Rescheduled Order.
		$data["rescheduledOrders"] = DB::table("patient_payments as pp")
					->select("pp.*", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email",
							"p.unique_code as patient_unique_code", "p.image as patient_image", 
							"m.name as member_name", "m.contact as member_contact", "m.email as member_email",
							"m.unique_code as member_unique_code", "m.image as member_image", "sc.id as sample_collector_id",
							"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
							"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date", "s.slot_time")
					->whereDate("pp.status_with_time->rescheduled", Carbon::now())
					->where("pp.order_status", config("constants.ORDER_STATUS.RESCHEDULED"))
					->join("patients as p", "p.id", "=", "pp.patient_id")
					->leftJoin("members as m", "m.id", "=", "pp.member_id")
					->join("slots as s", "s.order_number", "=", "pp.order_number")
					->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
					->latest("pp.created_at")
					->get()
					->toArray();
		
		// Retrieving Tests Details For Each Order And Storing Each Order In Rescheulded Order Category.
		foreach($data["rescheduledOrders"] as $key => $order){
			// Retrieving Tests Details.
			$data["rescheduledOrders"][$key]->tests = DB::table("patient_tests as pt")
													->select("t.name as test")
													->where(["pt.order_number" => $order->order_number])
													->join("tests as t", "t.id", "=", "pt.test_id")
													->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into String.
			$allTests = [];

			foreach($data["rescheduledOrders"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["rescheduledOrders"][$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
		
			// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
			$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
		
			// Retrieving Slot Time According To SlotTime Value From The CONSTANTS SLOT_TIMINGS Array.
			$order->slot_time = config("constants.SLOT_TIMINGS.$order->slot_time");
		}
		
		// Retrieving All Active Collectors.
        $data["collectors"] = SampleCollector::where(["status" => "active"])->get();

		// dd($data);

		// Page Name
		$data["title"] = "Pathology Order Received Panel";
		
		// Activating The Current Selected Left Sidebar
		$data["active"] = "Order Received";

		// View Pages Load
		return view ("pathology/order-received", $data);
    }

	public function allOrders(Request $request){
		// Retrieving Received Order For Today Slots With Patient Details With Their Members Details.
        $query = DB::table("patient_payments as pp")
                    ->select("pp.*", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email",
							"p.unique_code as patient_unique_code", "p.image as patient_image", 
							"m.name as member_name", "m.contact as member_contact", "m.email as member_email",
							"m.unique_code as member_unique_code", "m.image as member_image",
							"sc.name as sample_collector_name", "sc.contact as sample_collector_contact", "sc.email as sample_collector_email",
							"sc.unique_code as sample_collector_unique_code", "sc.image as sample_collector_image", "s.slot_date", "s.slot_time")
                    ->join("patients as p", "p.id", "=", "pp.patient_id")
                    ->leftJoin("members as m", "m.id", "=", "pp.member_id")
                    ->join("slots as s", "s.order_number", "=", "pp.order_number")
                    ->leftJoin("sample_collectors as sc", "sc.id", "=", "pp.sample_collector_id")
					->latest("pp.created_at");
					
		// If Search Parameter Provided.
		if($request->has("search-orders") && trim($request->input("search-orders")) != "all"){
			$data["allOrders"] = $query->where(["order_status" => trim($request->input("search-orders"))])
										->get();
		}
		
		// If Search Parameter Not Provided.
		else{
			$data["allOrders"] = $query->get();
		}
		
		// Retrieving Patient, Member, Address And Slot Details For Each Order.
		foreach($data["allOrders"] as $key => $order){
			// Retrieving Tests Details.
            $data["allOrders"][$key]->tests = DB::table("patient_tests as pt")
                                                    ->select("t.name as test")
                                                    ->where(["pt.order_number" => $order->order_number])
                                                    ->join("tests as t", "t.id", "=", "pt.test_id")
                                                    ->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into String.
			$allTests = [];

			foreach($data["allOrders"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["allOrders"][$key]->tests = implode(", ", $allTests);

			// Converting Json StatusWithTime To Array.
			$order->status_with_time = json_decode($order->status_with_time, true);
			
			// Retrieving Order Status Key According To OrderStatus Value From The CONSTANTS ORDER_STATUS Array.
			$order->order_status = array_search($order->order_status, config("constants.ORDER_STATUS"));
		
			// Retrieving Slot Time According To SlotTime Value From The CONSTANTS SLOT_TIMINGS Array.
			$order->slot_time = config("constants.SLOT_TIMINGS.$order->slot_time");
        }
		
		// Retrieving All Active Collectors.
        $data["collectors"] = SampleCollector::where(["status" => "active"])->get();

		// Page Name
		$data["title"] = "Pathology All Orders Panel";
		
		// Activating The Current Selected Left Sidebar
		$data["active"] = "All Orders";

		// dd($data);
		
        // View Pages Load
		return view ("pathology/all-orders", $data);
    }

	public function getOrderById(Request $request){
		if(!$request->isMethod("post")){
			// Creating Failed Response Data.
			$response = [
				"status" => "fail",
				"message" => "Inavalid Request Method.",
				"data" => null
			];
	
			// Returning View Page Response
			return response()->json($response);
		}

		// Retrieving Payment Details.
		$payment = DB::table("patient_payments as pp")
								->select("pp.order_number", "pp.created_at", "pp.sample_collector_id", "p.name as patient_name", "p.unique_code as patient_unique_code",
										"p.image as patient_image", "m.name as member_name", "m.unique_code as member_unique_code", "m.image as member_image")
								->join("patients as p", "p.id", "pp.patient_id")
								->leftJoin("members as m", "m.id", "pp.member_id")
								->where(["order_number" => trim($request->input("order_number"))])
								->first();

		// Converting MySql Date Into Human Readable Date.
		$payment->created_at = date("d/M/Y h:i A", strtotime($payment->created_at));

		// Creating Success Response Data.
		$response = [
			"status" => "success",
			"message" => "Data Found.",
			"data" => $payment
		];

		// Returning View Page Response
        return response()->json($response);
    }

    public function assignSampleCollector(Request $request){
        // Validation Rules.
        $rules = [
            "order_number" => "required",
            "sample_collector_id" => "required"
        ];

        // Validaton Custom Messages.
        $messages = [
            "order_number.required" => "Order ID Required",
            "sample_collector_id.required" => "Sample Collector ID Required"
        ];

        // Input Validation.
        $validator = Validator::make($request->all(), $rules, $messages);

        // On Validation Fail
        if($validator->fails()){
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "Please Fill Out All Fields. Try Again...");

			// Returning On Previous Page.
			return redirect(url()->previous());
        }

        // On Validation Success. Retrieving Sample Collector.
        $sampleCollector = SampleCollector::find(trim($request->input("sample_collector_id")));

        // On Sample Collector Found.
        if(!$sampleCollector){
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "No Sample Collector Details Found. Try Again...");

			// Returning On Previous Page.
			return redirect(url()->previous());
        }

        // Retrieving Order Details.
        $order = PatientPayment::firstWhere(["order_number" => trim($request->input("order_number"))]);

        // On No Order Details Found.
        if(!$order){
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "No Order Details Found. Try Again...");

			// Returning On Previous Page.
			return redirect(url()->previous());
        }

        // Assinging Sample Collector To The Order.
        $order->sample_collector_id = trim($request->input("sample_collector_id"));
        $order->order_status = config("constants.ORDER_STATUS.TECHNICIAN ASSIGNED");
        $order->forceFill([
			"status_with_time->confirmed" => date("Y-m-d H:i:s"),
			"status_with_time->technician_assigned" => date("Y-m-d H:i:s")
		]);
        $assignSampleCollector = $order->save();

        // On Fail.
        if(!$assignSampleCollector){
			$request->session()->flash("status", "danger");
			$request->session()->flash("message", "Sample Collector Not Assigned. Try Again...");

			// Returning On Previous Page.
			return redirect(url()->previous());
        }

        // On Success.
		$request->session()->flash("status", "success");
		$request->session()->flash("message", "Sample Collector Assigned Successfully.");

		// Returning On Previous Page.
		return redirect(url()->previous());
    }
	
	#==================================================#
	#--------------- PAYMENT FUNCTION -----------------#
	#==================================================#

	public function payment(){
		// Sum Of Total Earning.
		$data["totalEarning"] = PatientPayment::select(DB::raw("sum(payable) as total_earning"))
												->where("order_status", config("constants.ORDER_STATUS.REPORT ISSUED"))
												->first()
												->total_earning;

		// Sum Of Today Earning.
		$data["todayEarning"] = DB::table("patient_payments as pp")
									->select(DB::raw("sum(payable) as today_earning"))
									->where(["order_status" => config("constants.ORDER_STATUS.REPORT ISSUED")])
									->whereDate("slot_date", Carbon::now())
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->first()
									->today_earning;

		// Sum Of Last Week Earning.
		$data["lastWeekEarning"] = DB::table("patient_payments as pp")
									->select(DB::raw("sum(payable) as last_week_earning"))
									->where(["order_status" => config("constants.ORDER_STATUS.REPORT ISSUED")])
									->whereDate("slot_date", Carbon::now()->subDays(7))
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->first()
									->last_week_earning;

		// Sum Of Last Month Earning.
		$data["lastMonthEarning"] = DB::table("patient_payments as pp")
									->select(DB::raw("sum(payable) as last_month_earning"))
									->where(["order_status" => config("constants.ORDER_STATUS.REPORT ISSUED")])
									->whereDate("slot_date", Carbon::now()->subMonth())
									->join("slots as s", "s.order_number", "=", "pp.order_number")
									->first()
									->last_month_earning;


		// Retrieving Payment Details.
		$data["payments"] = DB::table("patient_payments as pp")
								->select("pp.*", "p.name as patient_name", "p.contact as patient_contact", "p.email as patient_email", "p.unique_code as patient_unique_code", "p.image as patient_image",
										"m.name as member_name", "m.contact as member_contact", "m.email as member_email", "m.unique_code as member_unique_code", "m.image as member_image")
								->join("patients as p", "p.id", "pp.patient_id")
								->leftJoin("members as m", "m.id", "pp.member_id")
								->latest("pp.created_at")
								->get();
		
		// Retrieving Tests Details For Each Order.
		foreach($data["payments"] as $key => $order){
			// Retrieving Tests Details.
			$data["payments"][$key]->tests = DB::table("patient_tests as pt")
													->select("t.name as test")
													->where(["pt.order_number" => $order->order_number])
													->join("tests as t", "t.id", "=", "pt.test_id")
													->get()
													->toArray();

			// Creating Temporaty Test Variable For Stroing Current Order's All Tests
			// So That We Could Convert This Into String.
			$allTests = [];

			foreach($data["payments"][$key]->tests as $test){
				$allTests[] = $test->test;
			}

			// Converting allTests Array Into String.
			$data["payments"][$key]->tests = implode(", ", $allTests);
		}

		// dd($data);

		// Page Name
		$data["title"] = "Pathology Payment Panel";

		// Activating The Current Selected Left Sidebar
		$data["active"] = "Payment";

		// View Pages Load
		return view ("pathology/payment", $data);
	}
}