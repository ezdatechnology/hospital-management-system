<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Doctor;

class BaseController extends Controller{
    #==============================================#
    #---------- AUTHENTICATION FUNCTION -----------#
    #==============================================#
    
    public function index(Request $request){
        // If Form Method Is Post.
        if($request->isMethod("post")){
            // Validation Rules.
            $rules = [
                "email" => "required|email",
                "password" => "required",
            ];
    
            // Validaton Custom Messages.
            $messages = [
                "email.required" => "Email Address Required",
                "email.email" => "Invalid Email Address",
                "password.required" => "Password Required",
            ];
            
            // Input Validation.
            $validator = Validator::make($request->all(), $rules, $messages);
            
            // On validation fail
            if($validator->fails()){
                return redirect()->route("login")->withErrors($validator, "login")->withInput();
            }

            // Retrieving Doctor Model According To Credentials.
            $loginDetails = Doctor::firstWhere([
                                        "official_email" => trim($request->input("email")),
                                        "password" => md5(trim($request->input("password")))
                                    ]);
                                    
            // On Login Details Not Found.
            if(!$loginDetails){
                // Error Status Message.
                $request->session()->flash("status", "danger");
                $request->session()->flash("message", "Invalid Credenitals.");

                // If Invalid Credentials Redirecting Back To Login Page.
                return redirect()->route("login")->withInput();
            }
            
            // Creating Login Session Data.
            $request->session()->put("login", $loginDetails);
            
            // Success Status Message.
            $request->session()->flash("status", "success");
            $request->session()->flash("message", "Successfully Login.");
            
            // If Login Details Role Is Admin.
            if($loginDetails["role"] === "admin"){
                // Redirecting To Admin Dashboard.
                return redirect()->route("admin-dashboard");
            }

            // If Login Details Role Is Doctor.          
            else if($loginDetails["role"] === "doctor"){                
                // Redirecting To Doctor Dashboard.
                return redirect()->route("dashboard");
            }

            // If Login Details Role Is Path.          
            else if($loginDetails["role"] === "path"){                
                // Redirecting To Pathology Dashboard.
                return redirect()->route("pathology-dashboard");
            }
        }

        // Page Name
        $data["title"] = "Pathology Login Panel";

        // Loading Login View
        return view("login", $data);
    }

    #==============================================#
    #------------- LOGOUT API FUNCTION ------------#
    #==============================================#

    public function logout(Request $request){
        // Removing Session Data For Logging Out The User.
        $sessionData = $request->session()->pull("login");
        
        // On Logout Fail, Redirecting Back To Previous Dashboard.
        if($request->session()->has("login")){
            // Error Status Message.
            $request->session()->flash("status", "danger");
            $request->session()->flash("message", "Colud Not Log Out. Try Again...");
            
            // If Doctor Role Is Admin.
            if($sessionData["role"] === "admin"){
                // Redirecting To Admin Dashboard.
                return redirect()->route("admin-dashboard");
            }

            // If Doctor Role Is Employee.          
            else if($sessionData["role"] === "doctor"){
                // Redirecting To Employee Dashboard.
                return redirect()->route("doctor-dashboard");
            }
        }
        
        // Success Status Message.
        $request->session()->flash("status", "success");
        $request->session()->flash("message", "Loguot Successfully.");

        // On Successful Logout, Redirecting Back To Login Page.
        return redirect()->route("login");
    }
}