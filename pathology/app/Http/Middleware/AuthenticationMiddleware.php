<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticationMiddleware{
    public function handle(Request $request, Closure $next){
        // Validating If Authenticated Login.
        if(!$request->session()->has("login")){
            return redirect()->route("/");
        }

        // Proceed To Next Request If Admin Login OR Doctor Login Or Pathology Login.
        if(($request->session()->get("login")["role"] === "admin") || ($request->session()->get("login")["role"] === "doctor") || ($request->session()->get("login")["role"] === "path")){
            return $next($request);
        }
    }
}
