<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->unsignedBigInteger("country_id")->references("id")->on("countries");
            $table->string('contact')->unique();
            $table->string("email")->nullable();
            $table->string("address");
            $table->string("country");
            $table->string("state");
            $table->string("city");
            $table->string("pincode")->nullable();
            $table->string("vehicle_no")->nullable();
            $table->date("dob")->default("1996-11-06");
            $table->enum("gender", ["male", "female", "other"])->default("male");
            $table->string("image")->default("06.jpg");
            $table->string("password")->default("password");
            $table->string("unique_code")->unique();
            $table->enum("otp_verified", ["yes", "no"])->default("no");
            $table->enum("status", ["active", "inactive"])->default("active");
            $table->string('session_id')->nullable();
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
