<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('contact')->unique();
            $table->string('email')->unique();
            $table->string('official_email')->unique();
            $table->string('password');
            $table->date('dob');
            $table->text('address');
            $table->enum('role', ["admin", "doctor", "path"])->default("doctor");
            $table->string('speciality')->default("physician");
            $table->string('image')->default("dummy-user.jpg");
            $table->string('unique_code')->unique();
            $table->enum('status', ["active", "inactive"])->default("active");
            $table->string('created_by')->default("admin");
            $table->string('updated_by')->default("admin");
            $table->timestamp("created_at")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("updated_at")->default(DB::raw("CURRENT_TIMESTAMP"));            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
